#include <iostream>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <limits>

using namespace std; 


double d(double x1, double y1, double x2, double y2)
{
    double x = x1 - x2; //calculating number to square in next step
    double y = y1 - y2;
    double dist;

    dist = pow(x, 2) + pow(y, 2);       //calculating Euclidean distance
    dist = sqrt(dist);                  

    return dist;
}

void process_island(int index, int n) {
    // Island Data
    // 0: x
    // 1: y
    // 2: pop
    // 3: internet_bool
    // 4: closest island
    // 5: dist to closest island
    // 6: total dist

    double graph[n][7];

    for(int i = 0; i < n; i++) {
        scanf("%lf %lf %lf\n", &graph[i][0], &graph[i][1], &graph[i][2]);
    }

    graph[0][3] = 1;
    graph[0][4] = 0;
    graph[0][5] = 0;
    graph[0][6] = 0;

    int connected = 1;

    for(int i = 1; i < n; i++){
        graph[i][3] = 0;
        graph[i][4] = -1;
        graph[i][5] = -1;
        graph[i][6] = -1;
    }

    while(connected < n) {
        double dist = std::numeric_limits<double>::max();
        int closest_new_index;
        int closest_old_index;
        for(int i = 0; i < n; i++) {
            if(graph[i][3] == 0) {
                for(int j = 0; j < n; j++) {
                    if(graph[j][3] == 1){
                        double temp_dist = d(graph[i][0], graph[i][1], graph[j][0], graph[j][1]);
                        if (temp_dist < dist) {
                            dist = temp_dist;
                            closest_new_index = i;
                            closest_old_index = j;
                        }
                    }
                }
            }
        }
        connected++;
        //printf("Connecting %i to %i\n", closest_new_index, closest_old_index );
        graph[closest_new_index][3] = 1;
        graph[closest_new_index][4] = closest_old_index;
        graph[closest_new_index][5] = dist;
        if (dist < graph[closest_old_index][6]) {
            graph[closest_new_index][6] = graph[closest_old_index][6];
        }
        else {
            graph[closest_new_index][6] = dist;
        }
        

        // Print graph
        /*
        printf("%-8s %-8s %-8s %-8s %-8s %-8s %-8s %-8s\n", "Ind", "X", "Y", "Pop", "Conn", "Pair", "Dist", "Dist_Total");
        for(int i = 0; i < n; i++) {
            printf("%-8.0lf %-8.2lf %-8.2lf %-8.2lf %-8.2lf %-8.0lf %-8.2lf %-8.2lf\n", (double)i, graph[i][0], graph[i][1], graph[i][2], graph[i][3], graph[i][4], graph[i][5], graph[i][6]);
        }
        printf("\n");*/
    }

    double num = 0;
    double den = 0;

    for(int i = 0; i<n; i++) {
        num += graph[i][2] * graph[i][6];
        den += graph[i][2];
        //printf("NUM: %lf DEN: %lf\n",num,den );
    }

    // Print graph
    /*
    printf("%-8s %-8s %-8s %-8s %-8s %-8s %-8s %-8s\n", "Ind", "X", "Y", "Pop", "Conn", "Pair", "Dist", "Dist_Total");
    for(int i = 0; i < n; i++) {
        printf("%-8.0lf %-8.2lf %-8.2lf %-8.2lf %-8.2lf %-8.0lf %-8.2lf %-8.2lf\n", (double)i, graph[i][0], graph[i][1], graph[i][2], graph[i][3], graph[i][4], graph[i][5], graph[i][6]);
    }
    printf("\n");*/
    printf("Island Group: %d Average %.2lf\n", index, num/den);

    return;
}


int main() {
    int n;
    int index = 1;
    scanf("%d", &n);
    while(n != 0) {
        process_island(index++, n);
        scanf("%d", &n);
    }


}

