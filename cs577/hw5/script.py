from math import floor
import itertools

def main():

    sections = [ [30, 20], [40, 70], [10,30]]
    sections = [ [5, 10], [40, 70], [3,30], [30, 20], [90, 5], [100,200]]
    sections = [[4,4],[1,1],[2,2],[3,3],[5,5]]
    k = 2
    v = 10

 
    time = 0.0

    for index in range(0, len(sections)):
        time += (float(sections[index][0]) / float(sections[index][1]))
    print time

    print sections
    sections.sort(key=lambda x: (float(x[0])/float(x[1]) - float(x[0])/(float(x[1])+v)), reverse=True)
    print sections


    for index2 in range(0, k):
        normal_time = float(sections[index2][0])/float(sections[index2][1])
        speeding_time = float(sections[index2][0])/(float(sections[index2][1])+v)
        diff_time = normal_time - speeding_time
        print diff_time
        time -= diff_time

    print("The greed algorithm produces time: {:.4f}".format(time))
    print("The optim algorithm produces time: {:.4f}".format(print_optimal(sections,k,v)))

    
    

def print_optimal(sections, k, v):
    min_time = float("inf")


    perms = list(itertools.permutations(sections))

    for perm in perms:
            time = 0.0
            for z in range(0, len(perm)):
                
                if z < k :
                    time += (float(perm[z][0]) / (float(perm[z][1]) + v))
                else:
                    time += (float(perm[z][0]) / float(perm[z][1]))

            if time < min_time:
                min_time = time

    return min_time


if __name__ == "__main__":

    main()
