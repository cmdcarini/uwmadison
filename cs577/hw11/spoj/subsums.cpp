#include <bits/stdc++.h>

using namespace std;

void subset(int s[],vector<int> &v,int n,int start) {
    int i, j, sum, two_power = pow(2, n);

    for(i = 0; i < two_power; i++) {
            sum = 0;
        for(j = 0; j < n; j++) {
            if(i & (1 << j)) {
                sum = sum + s[j+start];
            }
        }
        v.push_back(sum);
    }
}

int main() {
    int n, a, b, i, j, upper, lower; 
    int s[40];
    vector<int> left,right;
    long long int result = 0;
    scanf("%d%d%d", &n, &a, &b);

    for(i = 0; i < n ;i++) {
        scanf("%d", &s[i]);
    }

    subset(s, left, n/2, 0);
    subset(s, right, (n&1)?n/2+1:n/2, n/2);

    sort(right.begin(), right.end());

    for(i = 0; i < left.size(); i++) {
        lower = lower_bound(right.begin(), right.end(), a-left[i]) - right.begin();
        upper = upper_bound(right.begin(), right.end(), b-left[i]) - right.begin();
        result = result + (upper - lower);
    }
    printf("%lld\n",result);
}
