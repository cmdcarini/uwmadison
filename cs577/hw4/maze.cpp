#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

int main(){
	// Read in vars
	int N,E,T,M;
	scanf("%d%d%d%d",&N,&E,&T,&M);

	// Initialize Adjacency Matrix
	long graph[N][N];
	memset(graph,100100,N * N * sizeof(graph[0][0]));

	// Read in graph connections
	while(M > 0){
		int a,b,c;
		scanf("%d %d%d",&a,&b,&c);
		// Off
		graph[a-1][b-1]=c;
		M--;
	}

	// Set self-connections to zero
	for(int i=0;i<N;i++){
		graph[i][i]=0;
	}

	// Compute
	for(int k=0;k<N;k++){
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				if(graph[i][j]>graph[i][k]+graph[k][j]){
					graph[i][j]=graph[i][k]+graph[k][j];
				}
			}
		}
	}

	// Determain Count
	int count=0;
	for(int i=0;i<N;i++){
		if(graph[i][E-1]<=T)
			count++;
	}
	printf("%d\n",count);
	return 0;
}