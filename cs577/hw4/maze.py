import sys


class Graph():

	def __init__(self):
		self.num_cells = None
		self.exit_cell = None
		self.timer = None
		self.M = None
		self.graph = None
		self.input_parse()

	def input_parse(self):

		self.num_cells = int(input())
		self.exit_cell = int(input())
		self.timer = int(input())
		self.M = int(input())

		self.graph = [[0 for column in range(self.num_cells)] for row in range(self.num_cells)] 

		for x in range(0,self.M):
			line = input()
			line_split = line.split()


			a = int(line_split[0]) - 1
			b = int(line_split[1]) - 1
			c = int(line_split[2])

			self.graph[a][b] = c

	def compute(self):  
		result = 0
		distance = [99999999999] * self.num_cells
		distance[self.exit_cell-1] = 0
		checked = [False] * self.num_cells
		min_index = 0
		for nodes in range(self.num_cells):
			min = 99999999999
			for curr in range(self.num_cells):
				if distance[curr] < min and checked[curr] == False:
					min = distance[curr]
					min_index = curr
			
			checked[min_index] = True
			for vert in range(self.num_cells):
				if self.graph[min_index][vert] > 0 and checked[vert] == False and distance[vert] > (distance[min_index] + self.graph[min_index][vert]):
					distance[vert] = distance[min_index] + self.graph[min_index][vert]
		for nodes in range(self.num_cells): 
			if(distance[nodes] <= self.timer): 
				result += 1
		
		print(result)


	def print_graph(self):
		print(self.graph)



g = Graph()
g.compute()
# g.print_graph()
