


def main():

    #lectures = [ [1.0, 3.1], [2.2, 4],  [1.7,5], [4,5]]
    lectures = [ [1.0, 3], [1, 4], [1,4], [3,6], [3,6], [4,6]]

    lectures.sort(key=lambda x: x[1])
    print_array(lectures)
    
    array_i = 0

    count = 0

    while (len(lectures) > 0):

        start_i = lectures[array_i][0]
        end_i = lectures[array_i][1]

        while (array_i < len(lectures) and lectures[array_i][0] < end_i):
            if lectures[array_i][0] > start_i:
                start_i = lectures[array_i][0]
            array_i += 1

        print("Time to dump!\n{}, {}\n".format(start_i,end_i))
        count += 1
        lectures = lectures[array_i:]
        array_i = 0

        

    print("Count: {}".format(count))


def print_array(lectures):
    for item in lectures:
        i = 0
        density = 0.5

        while i <= item[1]:
            if (i == item[0]):
                print "(",
            elif (i == item[1]):
                print "]",
            elif (i < item[0]):
                print " ",
            else:
                print "-",

            i += density
        print(" ")

if __name__ == "__main__":

    main()