# for i <- 2 to N
#     j <- i
#     while j > 1 and a[j] < a[j - 1]
#         swap a[j] and a[j - 1]
#         j <- j - 1


# Read in T
T = int(input())

# Iterate over T
for t in range(0, T):
	# Read in N
	N = int(input())
	# Read in the array
	a = input().split(" ")
	
	counter = 0

	# Convert the array into type Int
	a = list(map(int, a))

	for i in range(0, N):
		j = i
		
		while j > 0 and (a[j] < a[j - 1]):
			a[j], a[j-1] = a[j-1],a[j]
			counter = counter + 1
			j = j - 1

	print(counter)


