msk = None
n = None
m = None
home = None
dist = [[10000000 for col in range(101)] for row in range(101)]
dp = [[-1 for col in range(1 << 12)] for row in range(101)]
src = [0] * 20
dest = [0] * 20

def main():
    global msk, n, m, home, dist, dp, src, dest
    home = 0
    t = int(input())
    for num in range(t):
        dist = [[10000000 for col in range(101)] for row in range(101)]
        for test in range(101):
            dist[test][test] = 0
        dp = [[-1 for col in range(1 << 12)] for row in range(101)]
        src = [0] * 20
        dest = [0] * 20
        n, m, home = (int(i) for i in input().split())
        for i in range(m):
            x, y, c = (int(z) for z in input().split())
            dist[y-1][x-1] = c
            dist[x-1][y-1] = dist[y-1][x-1]
        for k in range(n):
            for i in range(n):
                for j in range(n):
                    if(dist[i][k] + dist[k][j] < dist[i][j]):
                        dist[i][j] = dist[i][k] + dist[k][j]
        msk = 0
        k = 0
        q = int(input())
        for i in range(q): 
            x, y, c = (int(z) for z in input().split())
            msk = msk + c
            while(c > 0):
                src[k] = x - 1
                dest[k] = y - 1
                k += 1
                c -= 1
        msk = k
        ans = courier(0, home - 1)
        print(ans)
        return ans
        # return 0

def courier(mask, prev):
    global msk, n, m, home, dist, dp, src, dest
    ans = 10000000
    cost = 0
    f = 0
    # Check for cleared array
    if (dp[mask][prev] != -1):
        return dp[mask][prev]

    for i in range(msk):
        if((mask & (1 << i)) == 0):
            f = 1
            cost = courier(mask | (1 << i), dest[i]) + dist[prev][src[i]] + dist[src[i]][dest[i]]
            if(cost < ans):
                ans = cost
    if (f == 0):
        dp[mask][prev] = dist[prev][home - 1]
        return dp[mask][prev]
    
    dp[mask][prev] = ans
    return dp[mask][prev]

if __name__ == "__main__":
    main()