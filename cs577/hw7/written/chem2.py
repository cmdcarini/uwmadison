e = {
	1: { 2: 10, 3: 5 },
	2: { 1: 10, 3: 42 },
	3: { 1: 5, 2: 42 }

}
# e = {
# 	1: { 2: 10, 3: 5, 4:20 },
# 	2: { 1: 10, 3: 42, 4:6 },
# 	3: { 1: 5, 2: 42, 4:50 },
# 	4: { 1: 20, 2: 6, 3:50 }

# }

n = None
k = None


def badness(i, j):
	global e
	cost = 0

	for x in range(i, j+1):
		for y in range(i, j+1):
			if x != y and x < y:
				cost += e[x][y]

	return cost

def main():
	global n, k, e
	n = 3
	k = 2

	for x in range(1, n+1):
		for y in range(1, n+1):
			None
			#print(x,y,badness(x,y))

	print(dp(1,1))


def dp(starting_value, index):
	global k, n
	print("Running DP on index ", index)

	if index > k:
		print("Returning with cost zero. Index: ", index)
		return 0
	elif starting_value > n:
		print("Empty bottle. Index: ", index)
		return 0


	min_bad = 10000

	if (index >= k):
		last_bad = badness(starting_value, n)
		print(starting_value, n, last_bad)
		min_bad = last_bad
	else:
		for j in range(starting_value, n+1):
			j_bad = badness(starting_value, j) + dp(j+1, index+1)
			print(starting_value, j, j_bad)
			if j_bad < min_bad:
				min_bad = j_bad


	print("Returning DP on index ", index, ". With value ", min_bad)
	return min_bad

if __name__ == '__main__':
	main()