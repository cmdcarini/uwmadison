
d = None
n = None
k = None
a = []
b = []

def f(index, start):
    if start < 0:
        return 0

    if index == n-1:
        if start == 0:
            return 1
        else:
            return 0

    ret = b[index][start]

    if ret != -1:
        return ret
   
    if (index in a):
        print("foo")
        return f(index + 1, start + 1)
    
    return f(index + 1,start + 1) + f(index + 1,start - 1)


def main():
    global d, n, k, a, b
    d = int(input())
    while d > 0:
        # Input n, and k
        n,k = input().split()
        # Multiple n * 2
        n = int(n)*2
        # Init data structures
        new = []
        for x in range (0, 50):
            for y in range (0, 50):
                new.append(-1)
            b.append(new)
            new = []

        # Mark location of opening brackets
        aa = input().split()
        for value in aa:
            a.append(int(value))

        print(f(0,0))
        d = d -1

    print(a)
    #print(b)
    return 0

main()