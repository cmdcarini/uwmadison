#include<cstring>
#include<iostream>

using namespace std;

int d;
int n;
int k;
int j;
int a[50];
int b[50][50];

int recursive(int index, int open) {
    // if open is less than 0
    if(open < 0) {
        return 0;
    }

    // if index is equal to n
    if(index == n) {
        return (open == 0);
    }

    int &ret = b[index][open];

    // if ret not equal to -1
    if(ret != -1) {
        return ret;
    }

    if(a[index] == 1) {
        return ret = recursive(index + 1, open + 1);
    }

    return ret = recursive(index + 1, open - 1) + 
        recursive(index + 1, open + 1);
}

int main() {
	// Input d
	cin >> d;

	// Loop over d times
	while(d > 0) {
		// Input n, and k
		cin >> n;
		cin >> k;
		// Init data structures
		memset(b, -1, sizeof b);
		memset(a, 0, sizeof a);
        n = n * 2;
        
		// Mark location of opening brackets
		for(int i = 0; i < k; i++) {
			cin >> j;
			a[j - 1] = 1;
		}
		cout << recursive(0, 0) << endl;
        d--;
	}
	return 0;
}