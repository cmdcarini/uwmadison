import math
import sys


def main():
	N = int(input())
	
	points = []
	closest_d = sys.maxsize
	closest_index = 0

	for line in range(0, N):
		line_raw = input()
		x_iter = int(line_raw.split(" ")[0])
		y_iter = int(line_raw.split(" ")[1])

		points.append((x_iter,y_iter))

	for index in range(0, N):
		for o_index in range(index+1, N):

			iter_dist = math.sqrt( ( points[o_index][0] - points[index][0] )**2 + (points[o_index][1] - points[index][1])**2 )
			#print("   "+str(index) + " " + str(o_index) + " " + str(iter_dist))
			
			if iter_dist < closest_d:
				closest_d = iter_dist
				closest_index = (index, o_index)


	print("{} {} {:0.6f}".format(closest_index[0], closest_index[1], closest_d))

if __name__ == "__main__":
    main()