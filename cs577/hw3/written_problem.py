
import sys

#A = [4,2,1,3]
#A = [1,2,3,4]


def func(array):

	if(len(array) == 1):
		return (array,0)
	
	mid = len(array)/2

	left = array[0:mid]
	right = array[mid:]
	l_sum = sum(left)
	r_sum = sum(right)

	if(l_sum > r_sum):
		temp = left
		left = right
		right = temp

	
	l_ret = func(left)
	r_ret = func(right)
	left = l_ret[0]
	right = r_ret[0]
	left_c = l_ret[1]
	right_c = r_ret[1]

	l = 0
	r = 0
	count = 0


	inv_count = 0
	for i in range(0, len(left)): 
		for j in range(0, len(right)): 
			if (left[i] > right[j]): 
				count += 1

	'''         
	while( l < len(left) and r < len(right)):
		if left[l] <= right[r]:
			l = l + 1
		else:
			count = count + len(left) - l
			r = r + 1'''

	for index in range(0, mid):
		left.append(right[index])

	print("{}:{} {} {}: {}".format(len(array), count, left_c, right_c, left))

	return (left, count + left_c + right_c)


def sum(array):
	s = 0
	for index in range(0, len(array)):
		s = s + array[index]

	return s

def getInvCount(arr, n): 
  
	inv_count = 0
	for i in range(0, n): 
		for j in range(i + 1, n): 

			if (arr[i] > arr[j]): 
				#print ("{}, {}".format(i,j))
				inv_count += 1
  
	return inv_count


line = raw_input()
A = [int(x) for x in line.split()]
ret = func(A)
print(ret)
print getInvCount(ret[0],len(A))

