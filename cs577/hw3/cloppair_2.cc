#include <iostream> 
#include <float.h> 
#include <stdlib.h> 
#include <math.h> 
#include <string>
#include <cfloat>
#include <limits.h>
#include <algorithm>

using namespace std;

struct Point
{
	long double x,y;
	int index;
};

double dist(Point p1,Point p2)
{
	return sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y));
}

bool compareX(Point a,Point b)
{
	return a.x < b.x;
}

bool compareY(Point a,Point b)
{
	return a.y < b.y;
}

int g_1,g_2;

double g_min=999999999;

double bruteforce(Point p[],int n)
{
	double d=9999999999.9;

	for( int i=0;i<n;i++)
	{
		for(int j=i+1;j<n;j++)
		{
			double dtemp=dist(p[i],p[j]);

			if(dtemp<d)
			{
				d=dtemp;

				if(d < g_min)
				{
					g_min=d;
					g_1= p[i].index;
					g_2= p[j].index;
				}
			}
		}
	}

	return d;
}

double closest(Point P[],int n)
{
	if (n <= 3)
		return bruteforce(P , n);

	// Find the Middle Point
	int mid=n/2;

	// Find the smallest pair on each side of the array
	double left=closest(P,mid);
	double right=closest(P+mid,n-mid);
	// Use the smaller of the two
	double d=min(left,right);

	Point strip[n];

	int stripsize=0;

	Point midpoint=P[mid];

	for(int i=0;i<n;i++)
	{
		if(abs(P[i].x - midpoint.x) < d)
			strip[stripsize++] = P[i];
	}

	sort(strip, strip+stripsize , compareY);

	for(int i=0;i<stripsize;i++)
	{
		for(int j=i+1;j<stripsize;j++)
		{
			double tempdist = dist(strip[i],strip[j]);

			if(tempdist>=d) break;

			d=tempdist;

			if(d<g_min)
			{
				g_min=d;
				g_1=strip[i].index;
				g_2=strip[j].index;
			}

		}
	}

	return d;
}

int main()
{
	int n;

	cin>>n;

	Point P[n];

	double a,b;

	for(int i=0;i<n;i++)
	{
		scanf("%lf%lf", &a,&b);
		Point iter_point;
		iter_point.x=a;
		iter_point.y=b;
		iter_point.index=i;
		P[i]=iter_point;
	}

	sort(P, P+n, compareX);

	double d = closest(P,n);

	if(g_1 > g_2)
		swap(g_1,g_2);

	printf("%d %d %.6lf\n",g_1, g_2, g_min);

	return 0;
}