// A divide and conquer program in C++ to find the smallest distance from a 
// given set of points. 

#include <iostream> 
#include <float.h> 
#include <stdlib.h> 
#include <math.h> 
#include <string>
#include <cfloat>
#include <limits.h>
using namespace std; 

long long g_min;
int g_1, g_2;

// A structure to represent a Point in 2D plane 
struct Point 
{ 
	long x, y;
	int index;
}; 


/* Following two functions are needed for library function qsort(). 
Refer: http://www.cplusplus.com/reference/clibrary/cstdlib/qsort/ */

// Needed to sort array of points according to X coordinate 
int compareX(const void* a, const void* b) 
{ 
	Point *p1 = (Point *)a, *p2 = (Point *)b; 
	return (p1->x - p2->x); 
} 
// Needed to sort array of points according to Y coordinate 
int compareY(const void* a, const void* b) 
{ 
	Point *p1 = (Point *)a, *p2 = (Point *)b; 
	return (p1->y - p2->y); 
} 

// A utility function to find the distance between two points 
long long dist(Point p1, Point p2) 
{ 
	return (p1.x - p2.x)*(p1.x - p2.x) + 
			(p1.y - p2.y)*(p1.y - p2.y); 
} 

// A Brute Force method to return the smallest distance between two points 
// in P[] of size n 
long long bruteForce(Point P[], int n) 
{ 
	long long min = LLONG_MAX; 
	for (int i = 0; i < n; ++i) 
		for (int j = i+1; j < n; ++j) 
			if (dist(P[i], P[j]) < min) {
				min = dist(P[i], P[j]);
				if (min < g_min) {
					g_min = min;
					g_1 = P[i].index;
					g_2 = P[j].index;
					//cout << "New Min (" << g_1 << "," << g_2 << "):" << g_min << "\n"; 
				}
				
			}
				
	return min; 
} 

// A utility function to find minimum of two float values 
long long min(long long x, long long y) 
{ 
	return (x < y)? x : y; 
} 


// A utility function to find the distance beween the closest points of 
// strip of given size. All points in strip[] are sorted accordint to 
// y coordinate. They all have an upper bound on minimum distance as d. 
// Note that this method seems to be a O(n^2) method, but it's a O(n) 
// method as the inner loop runs at most 6 times 
long long stripClosest(Point strip[], int size, long long d) 
{ 
	long long min = d; // Initialize the minimum distance as d 

	// Pick all points one by one and try the next points till the difference 
	// between y coordinates is smaller than d. 
	// This is a proven fact that this loop runs at most 6 times 
	for (int i = 0; i < size; ++i) 
		for (int j = i+1; j < size && (strip[j].y - strip[i].y) < min; ++j) 
			if (dist(strip[i],strip[j]) < min) 
				min = dist(strip[i], strip[j]); 

	return min; 
} 

// A recursive function to find the smallest distance. The array Px contains 
// all points sorted according to x coordinates and Py contains all points 
// sorted according to y coordinates 
long long closestUtil(Point Px[], Point Py[], int n) 
{ 
	// If there are 2 or 3 points, then use brute force 
	if (n <= 3) 
		return bruteForce(Px, n); 

	// Find the middle point 
	long mid = n/2; 
	Point midPoint = Px[mid]; 


	// Divide points in y sorted array around the vertical line. 
	// Assumption: All x coordinates are distinct. 
	Point Pyl[mid+1]; // y sorted points on left of vertical line 
	Point Pyr[n-mid-1]; // y sorted points on right of vertical line 
	long li = 0, ri = 0; // indexes of left and right subarrays 
	for (long i = 0; i < n; i++) 
	{ 
		if (Py[i].x <= midPoint.x) 
			Pyl[li++] = Py[i]; 
		else
			Pyr[ri++] = Py[i]; 
	} 

	// Consider the vertical line passing through the middle point 
	// calculate the smallest distance dl on left of middle point and 
	// dr on right side 
	long long dl = closestUtil(Px, Pyl, mid); 
	long long dr = closestUtil(Px + mid, Pyr, n-mid); 

	// Find the smaller of two distances 
	long long d = min(dl, dr); 

	// Build an array strip[] that contains points close (closer than d) 
	// to the line passing through the middle point 
	Point strip[n]; 
	long j = 0; 
	for (long i = 0; i < n; i++) 
		if (abs(Py[i].x - midPoint.x) < d) 
			strip[j] = Py[i], j++; 

	// Find the closest points in strip. Return the minimum of d and closest 
	// distance is strip[] 
	return min(d, stripClosest(strip, j, d) ); 
} 

// The main functin that finds the smallest distance 
// This method mainly uses closestUtil() 
long long closest(Point P[], long n) 
{ 
	Point Px[n]; 
	Point Py[n]; 
	for (long i = 0; i < n; i++) 
	{ 
		Px[i] = P[i]; 
		Py[i] = P[i]; 
	} 

	qsort(Px, n, sizeof(Point), compareX); 
	qsort(Py, n, sizeof(Point), compareY); 

	// Use recursive function closestUtil() to find the smallest distance 
	return closestUtil(Px, Py, n); 
} 

// Driver program to test above functions 
int main() 
{ 
	string N_str;
	getline(cin, N_str);

	int N = atol(N_str.c_str());

	Point P[N];

	for (int i = 0; i < N; i++)
	{
		string line;
		getline(cin, line);
		size_t pos = line.find(" ");
		string x = line.substr(0, pos);
		string y = line.substr(pos);

		Point iter_point = {atol(x.c_str()), atol(y.c_str()), i};

		P[i] = iter_point;

	}





	Point X[] = {{2, 3}, {12, 30}, {40, 50}, {5, 1}, {12, 10}, {3, 4}}; 
	int n = sizeof(P) / sizeof(P[0]); 

	g_min = LLONG_MAX;
	closest(P, n);

	cout << fixed;
	cout.precision(6);

	//cout << LLONG_MAX << "\n";
	//cout << g_min << "\n" ;
	if (g_min == LLONG_MAX) {
		//cout << g_1 << " " << g_2 << " " << 0 << "\n";
	}
	else {
		cout << g_1 << " " << g_2 << " " << sqrt(g_min) << "\n";
	}
	
	return 0; 
} 
