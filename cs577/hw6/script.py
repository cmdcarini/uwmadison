from math import floor
import itertools


'''
I just iterated though the input array
Keeping track of the smallest exchange rate and the profit
And if the profit was greater than than the starting dollar amount and the next 
rate in the array was smaller than the current rate, I updated the available 
funds to the value of profit
'''

def main(A, d, b, k):

    inv_usd = k     # Current number of usd
    inv_bdc = 0     # Current number of bdc

    OPT_P_BDC = 0   # Previous opt for selling all usd
    OPT_P_USD = 0   # Previous opt for selling all bdc
    OPT = k         # Current opt value

    # We need time O(n) and space O(1)
    for i in range(0, len(A)):
        days_exchange_rate = A[i]

        sell_usd = (inv_usd - d) / days_exchange_rate
        sell_bdc = (inv_bdc - b) * days_exchange_rate

        OPT_P_BDC = max(OPT_P_BDC, sell_usd)
        OPT_P_USD = max(OPT_P_USD, sell_bdc)

        OPT = max(OPT, OPT_P_USD, (OPT_P_BDC - b) * days_exchange_rate)
        print("{}: {} {} {}".format(i, OPT, OPT_P_USD, OPT_P_BDC))


    return OPT

def process_day_sellusd(A, d, b, k):
    # Are we at the first day in the array?
    if not A:
        return (k, 0)

    # Get the last day in the array
    today_exchange_rate = A[-1]

    # What should we have done yesterday?
    # Sold all Dollars
    ret_d = process_day_sellusd(A[:-1], d, b, k)
    # Sold all BDC
    ret_b = process_day_sellbdc(A[:-1], d, b, k)

    ret_b_today = (ret_b[0] - d) / today_exchange_rate
    
    print("{}Sellusd| {} sellusd:{} sellbdc:{} ret_b_today:{}"
        .format(" "*2*len(A), A, ret_d, ret_b, ret_b_today))

    return (0, max(ret_b_today, ret_d[1]))

def process_day_sellbdc(A, d, b, k):
    # Are we at the first day in the array?
    if not A:
        return (k, 0)

    # Get the last day in the array
    today_exchange_rate = A[-1]

    # What should we have done yesterday?
    # Sold all Dollars
    ret_d = process_day_sellusd(A[:-1], d, b, k)
    # Sold all BDC
    ret_b = process_day_sellbdc(A[:-1], d, b, k)

    
    ret_d_today = (ret_d[1] - b) * today_exchange_rate
    
    print("{}Sellbdc| {} sellusd:{} sellbdc:{} ret_d_today:{}"
        .format(" "*(2*len(A)), A, ret_d, ret_b, ret_d_today))

    return (max(ret_d_today, ret_b[0]), 0)




if __name__ == "__main__":

    A = [20, 50, 10, 60]
    d = 10
    b = 3
    k = 100
    #k = 200

    print(main(A, d, b, k))
    print(process_day_sellbdc(A, d, b, k))
