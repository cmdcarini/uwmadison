#include <stdio.h>
#include <iostream>
#include <string>
#include <cstring>

using namespace std;

int main() {
    int moves[1000001];
    int K, L, num_towers, max = 0, move_one, move_k, move_l;
    scanf("%d %d %d\n", &K, &L, &num_towers);
    int towers[num_towers];

    for(int i = 1; i <= num_towers; i++) {
        scanf("%d", &towers[i]);
        if(max < towers[i])
            max = towers[i];
    }

    moves[0] = 0;
    for(int i = 1; i <= max; i++) {
        move_one = moves[i-1];
        if(i - K >= 0) {
            move_k = moves[i-K];
        } else {
            move_k = 1;
        }

        if(i - L >= 0) {
            move_l = moves[i-L]; 
        } else {
            move_l = 1;
        }

        if(move_one == 1 && move_k == 1 && move_l == 1) {
            moves[i]=0;
        } else {
            moves[i]=1;
        }
    }
    string result = "";
    for(int i = 1; i <= num_towers; i++) {
        if(moves[towers[i]] == 1) {
            result += "A";
        } else {
            result += "B";
        }
    }

    printf("%s\n", result.c_str()); 
}