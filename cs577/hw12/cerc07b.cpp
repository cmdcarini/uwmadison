#include <stdio.h>
#include <cstring>
using namespace std;

bool a[20][20];
bool b[20][20];

int main() {
	int X, Y;
  	while(1==1) {
		scanf("%d %d", &Y, &X);
		if (X == 0 && Y == 0) {
			break;
		}
		char s[20];
		for (int i = 0; i < Y; i++) {
			scanf("%s", &s);
			for (int j = 0; j < X; j++)
				if (s[j] == 'X')
					a[j][i] = true;
				else
					a[j][i] = false;
		}
		int min = 1000;
		for (int i = 0; i < (1 << X); i++) {
			memcpy(b, a, sizeof(b));
			int flops = 0;
			for (int j = 0; j < X; j++)
				if ((1 << j) & i) {
					flops++;
					b[j][0] = !b[j][0];
					b[j + 1][0] = !b[j + 1][0];
					b[j][1] = !b[j][1];
					if (j > 0) {
						b[j - 1][0] = !b[j - 1][0];
					}
				}
			for (int j = 1; j < Y; j++)
				for (int k = 0; k < X; k++)
					if (b[k][j - 1]) {
						b[k][j] = !b[k][j];
						b[k + 1][j] = !b[k + 1][j];
						b[k][j + 1] = !b[k][j + 1];
						if (j > 0) {
							b[k][j - 1] = !b[k][j - 1];
						}
						if (k > 0) {
							b[k - 1][j] = !b[k - 1][j];
						}
						flops++;
					}
			bool B = true;
			for (int j = 0; j < X; j++) {
				if (b[j][Y - 1]) {
					B = false;
					break;
				}
			}
			if (B) {
				if (flops < min) {
					min = flops;
				}
			}
		}
		if (min < 1000) {
			printf("You have to tap %d tiles.\n", min);
		}
		else {
			printf("Damaged billboard.\n");
		}
	}
	return 0;
}