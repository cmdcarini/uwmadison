Input: 	Offers[ [u1,vj] ,..., [ui,vj] ,..., [un, vj]], an array of length m, where each array element corresponds to a job offer for student ui where i is the index assigned to each student (0<i<n-1), and vj is the index of the company extending the offer (0<j<n-1). ui,vj !∈ ℚ- 
		Favs[ f0, f1, ... , fi, ..., fn], an array of length n, where each array element corresponds to the favorite company for each applicant. fi would be equal to index of the favorite company for applicant number i. f0 !∈ ℚ- 


Output: OPT, where OPT is guaranteed number of students who will recieve their favorite offer. 


//	Edges will contain a list of edges
//	Each edge will contain the following properties
//      Index 0: u value of edge
//      Index 1: v value of edge
//		Index 2: f value of edge
Edges = []

Procedure main(Offers, Favs):
	// Convert parameters and setup problem
	for each offer in Offers do:
		Edges.add([offer[0],offer[1],∞])

	N = Edges[-1][0] + 1

	// Find optimal soln
	sum = 0
	loop = True
	while loop is True do:
		// Only loop again if a guranteed applicant is found. Default to false
		loop = False

		// Iterate over all of the applicants 
		for each applicant a in [0,N) do:
			edge = hasOneOption(a, Edges)
			if edge is not NULL do:
				// This edge is the only option for applicant a. Assign it a value 1
				edge[2] = 1

				// Now go and set any other edge with destination equal to the applicants assigned internship to value 0
				for each edge e in Edges do:
					if e[1] == edge[1]:
						e[2] = 0

				// Continue the loop as a applicant with a guaranteed assignemtn was found
				loop = True

				// Check if the Favorite company of the applicant is equal to the edge's destination
				if Favs[edge[0]] == edge[1] do:
					// If so add to sum
					sum++

				// Break out of the loop. Reevaluate if loop should run again
				break


Proedure hasOneOption(Applicant, Edges):
	r = NULL
	foundOne = FALSE

	for each edge e in Edges do:
		if




	return r
	

