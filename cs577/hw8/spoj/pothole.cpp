#include <cstring>
#include <iostream>
#include <queue> 

using namespace std;
#define inf 10000

int graph[205][205];
int residual_graph[205][205];

int parent[205];
bool visited[205];
int n;

bool bfs(int s,int t){

	// Create a visited array and mark all vertices as not visited 
	memset(visited,false,sizeof(visited));

	// Create a queue, enqueue source vertex and mark source vertex 
	// as visited 
	queue <int> q;
	q.push(s);
	visited[s]=true;
	parent[s]=-1;

	// Standard BFS Loop
	while(!q.empty())
	{
		int u = q.front();
		q.pop();

		for(int node =1;node<=t;node++){
			if(residual_graph[u][node]>0 && visited[node]==false){
				visited[node]=true;
				parent[node]=u;
				q.push(node);
			}
		}
		
	}

	// If we reached sink in BFS starting from source, then return 
	// true, else false 
	return (visited[t] == true); 
}

// Returns the maximum flow from s to t 
int fordfulkerson(int s,int t){

	parent[s]=-1; // This array is filled by BFS and to store path 

	int max_flow = 0; // There is no flow initially

	// Augment the flow while there is path from source to sink 
	while(bfs(s,t)){
		// Find minimum residual capacity of the edges along the 
		// path filled by BFS. Or we can say find the maximum flow 
		// through the path found. 
		int pathflow=inf;
		for(int u=t; u!=s; u=parent[u]){
			int v = parent[u];
			pathflow = min(pathflow, residual_graph[v][u]); 
		}
		
		// update residual capacities of the edges and reverse edges 
		// along the path 
		for(int u=t; u!=s; u=parent[u]){
			int v=parent[u]; 
			residual_graph[v][u]-=pathflow;
			residual_graph[u][v]+=pathflow;
		}

		// Add path flow to overall flow
		max_flow+=pathflow;
	}

	// Return the overall flow 
	return max_flow;
}

// Driver program to test above functions
int main(){
	// Read in the number of trials
	int t;
	scanf("%d",&t);

	// Iterate over the trials
	while(t--){

		// Reset all of the vars for a new trial
		memset(parent,0,sizeof(parent));
		memset(graph,0,sizeof(graph));
		memset(residual_graph,0,sizeof(residual_graph));
		
		// Scan the number of verticies 
		scanf("%d",&n);
		for(int i=1;i<=n-1;i++){
			// Scan the vertexes
			int x;
			scanf("%d",&x);
			// Update the graph with each value
			for(int j=1;j<=x;j++){
				int val;
				scanf("%d",&val);
				if(i==1 || val==n)
					graph[i][val]=1,residual_graph[i][val]=1;
				else
					graph[i][val]=inf,residual_graph[i][val]=inf;
				residual_graph[val][i]=0;
			}
		}

		cout<<fordfulkerson(1,n)<<endl;
	}
}