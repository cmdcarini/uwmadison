import subprocess

blacklist = open("file_blacklist.txt", "r")
for line in blacklist: 
    cmd = 'find . -name "%s"'%line.rstrip()
    out = subprocess.Popen(cmd,shell=True,stdin=subprocess.PIPE, 
                            stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    # Get standard out and error
    (stdout, stderr) = out.communicate()
    print(cmd)
    if stdout.decode().split() != []:
        for file_found in stdout.decode().split():
            if(file_found != ''):
                print(file_found)
        answer = ""
        while(answer != "y" and answer != "n"):
            answer = input("Would you like to delete this junk? ")
        
        if(answer == 'y'):
            cmd = 'find . -name "%s" -delete'%line.rstrip()
            print("RUNNING: ", cmd, "\n")
            out = subprocess.Popen(cmd,shell=True,stdin=subprocess.PIPE, 
                                    stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        else:
            print("You just made me do all that work for nothing?\n")
    else:
        print('')

blacklist = open("dir_blacklist.txt", "r")
for line in blacklist: 
    cmd = 'find . -type d -name "%s"'%line.rstrip()
    out = subprocess.Popen(cmd,shell=True,stdin=subprocess.PIPE, 
                            stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    # Get standard out and error
    (stdout, stderr) = out.communicate()
    print(cmd)
    if stdout.decode().split() != []:
        for file_found in stdout.decode().split():
            if(file_found != ''):
                print(file_found)
        answer = ""
        while(answer != "y" and answer != "n"):
            answer = input("Would you like to delete this junk? ")
        
        if(answer == 'y'):
            cmd = 'find . -type d -name "%s" -exec rm -rf {} +'%line.rstrip()
            print("RUNNING: ", cmd, "\n")
            out = subprocess.Popen(cmd,shell=True,stdin=subprocess.PIPE, 
                                    stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        else:
            print("You just made me do all that work for nothing?\n")
    else:
        print('')
