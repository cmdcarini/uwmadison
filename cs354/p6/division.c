////////////////////////////////////////////////////////////////////////////////
// Main File:        (name of file with main function)
// This File:        (name of this file)
// Other Files:      (name of all other files if any)
// Semester:         CS 354 Spring 2017
//
// Author:           Marco D Carini
// Email:            carini@cs.wisc.edu
// CS Login:         carini
//
////////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>

int count;

//handler for SIGFPE
void handler_SIGFPE(int sig){
	if(sig == SIGFPE){
		printf("Error: a division by 0 operation was attempted.\nTotal number of operations successfully completed: %d\nThe program will be terminated.\n", count);
                exit(0);
	}
}

//handler for SIGINT
void handler_SIGINT(int sig){
	if(sig == SIGINT){
		printf("\nTotal number of operations successfully completed: %d\nThe program will be terminated.\n",count);
                exit(0);
	}
}

//divides two inputs, invalid inputs are counted as zeros
//is able to handle SIGFPE, and SIGINT
int main(){
	//vars needed for input/divison/output
	char firstIn[10];
	char secIn[10];
	int firstInt = 0; 
	int secInt = 0;
	int div = 0;
	int rem = 0;
	count = 0;

	//SIGFPE, divide by 0 handler setup
	struct sigaction sa;
	memset (&sa, 0, sizeof(sa));
	sa.sa_handler = &handler_SIGFPE;
	sigaction(SIGFPE, &sa, NULL);
	
	//SIGINT, Ctrl-C handler setup
	struct sigaction sa1;
	memset(&sa1, 0, sizeof(sa1));
	sa1.sa_handler = &handler_SIGINT;	
	sigaction(SIGINT, &sa1, NULL);

	while(1){
		//get two inputs
		printf("Enter first integer: ");
		scanf("%s", firstIn);
		printf("Enter second integer: ");
		scanf("%s", secIn);

		//double check they are ints, otherwise
		//maintain == 0
		if(firstIn != NULL || atoi(firstIn) > 0)
			firstInt = atoi(firstIn);
		if(secIn != NULL || atoi(secIn) > 0)
			secInt = atoi(secIn);
		
		//divide the inputs, get remainder
		div = firstInt/secInt;
		count = count + 1;
		rem = firstInt % secInt;
	
		//output
		printf("%d / %d is %d with a remainder of %d\n", firstInt, secInt, div, rem);	
	}	
}
