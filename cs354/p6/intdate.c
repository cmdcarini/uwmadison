////////////////////////////////////////////////////////////////////////////////
// Main File:        n/a
// This File:        intdate.c
// Other Files:      sendsig.c, division.c
// Semester:         CS 354 Spring 2017
//
// Author:           Marco D Carini
// Email:            carini@cs.wisc.edu
// CS Login:         carini
//
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>

int count = 0; 

//handler for SIGALRM
//outputs PID, curr time, and sets a new alarm 
//of 3 seconds
void handler_SIGALRM(int sig){
	if(sig == SIGALRM){	
		time_t t = time(NULL);
        	struct tm *tm = localtime(&t);
        	char s[64];
        	strftime(s, sizeof(s), "%c", tm);
        	printf("PID: %d | Current time: %s\n",getpid(), s);
        	alarm(3);
	}
}

//handler for SIGUSR1
//counts num. of SIGUSR1 signals received
void handler_SIGUSR1(int sig){	
	if(sig == SIGUSR1){
		count++;
                printf("SIGUSR1 caught!\n");
	}
}

//handler for SIGINT
//when ^C is input, outputs that it was received, 
//then, proceeds to output num. of SIGUSR1 signals 
//received.
void handler_SIGINT(int sig){
	if(sig == SIGINT){
		printf("\nSIGINT recieved.\n");
                printf("SIGUSR1 was received %d times. Exiting now.\n", count);
                exit(0);
	}

}

//sets up sigactions, loops indefinitely, until SIGKILL, or SIGINT
int main(){
	//SIGALRM handler setup
	struct sigaction sa;
	memset (&sa, 0, sizeof(sa));
	sa.sa_handler = &handler_SIGALRM;
	sigaction(SIGALRM, &sa, NULL);
	
	//SIGUSR1 handler setup
	struct sigaction sa1;
	memset(&sa1, 0, sizeof(sa1));
	sa1.sa_handler = &handler_SIGUSR1;
	sigaction(SIGUSR1, &sa1, NULL);	
	
	//SIGINT handler setup
	struct sigaction sa2; 
	memset(&sa2, 0, sizeof(sa2));
	sa2.sa_handler = &handler_SIGINT;
	sigaction(SIGINT, &sa2, NULL);
	
	//init alarm(3)
	alarm(3);
	printf("Pid and time will be printed every 3 seconds\n");
	printf("Enter ^C to end the program\n");	
	
	//loops while 1==1
	while(1){
	}
}
