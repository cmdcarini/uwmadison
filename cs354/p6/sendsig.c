////////////////////////////////////////////////////////////////////////////////
// Main File:        n/a
// This File:        sendsig.c
// Other Files:      intdate.c, division.c
// Semester:         CS 354 Spring 2017
//
// Author:           Marco D Carini
// Email:            carini@cs.wisc.edu
// CS Login:         carini
//
////////////////////////////////////////////////////////////////////////////////

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>

//gets console input and sends either:
//-u: SIGUSR1 signal to specified PID
//-i: SIGINT signal to specified PID
int main(int argc, char *argv[]){
	//check that there were proper num of args
	if(argc == 0 || argc == 1 || argc == 2){
		printf("Usage: <signal type> <pid>\n");
		return 0;
	}
	
	//double check PID is an int and exists at all
	if(argv[2] == NULL || (atoi(argv[2]) < 0) || (atoi(argv[2]) == 0)){
		printf("Invalid PID\n");
		printf("Usage: <signal type> <pid>\n");
		return 0;
	}

	//double checks signal type is either -u or -i
	if(argv[1] == NULL || (strcmp("-u",argv[1]) != 0 && strcmp("-i",argv[1]) != 0)){
		printf("Invalid Signal Type\n");
		printf("Usage: <signal type> <pid>\n");
		return 0;
	}	

	//converts char* to int
	int pid = atoi(argv[2]);

	//if SIGUSR1, send SIGUSR1 sig to PID
	if(strcmp("-u", argv[1]) == 0){
		kill(pid, SIGUSR1);
	}

	//if SIGINT, send SIGINT sig to PID
	if(strcmp("-i",argv[1]) == 0){
		kill(pid, SIGINT);
	}
}
