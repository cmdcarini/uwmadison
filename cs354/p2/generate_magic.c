///////////////////////////////////////////////////////////////////////////////
// Main File:        generate_magic.c
// This File:        generate_magic.c
// Other Files:      verify_magic.c
// Semester:         CS 354 Spring 2017
//
// Author:           Marco Carini
// Email:            carini@cs.wisc.edu
// CS Login:         carini
//
////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

// Structure representing Square
// size: dimension(number of rows/columns) of the square
// array: 2D array of integers
typedef struct _Square {
	int size;
	int **array;
} Square;

int get_square_size();
Square * generate_magic(int size);
void write_to_file(Square * square, char *filename);

int main(int argc, char *argv[])
{
	// Check input arguments to get filename
	char *filename = *(argv + 1);

	// Get size from user
	int size = get_square_size();
	
	// Generate the magic square
	Square *result = generate_magic(size);

	// Write the square to the output file
	write_to_file(result, filename);
	for(int i = 0; i < size; i++){
		free(*((result->array) + i));
	}
	free(result->array);
	free(result);
	return 0;
}

/* get_square_size prompts the user for the magic square size
 * checks if it is an odd number >= 3 and returns the number
 */
int get_square_size()
{
	int size = 0;
	printf("Enter size of magic square, must be odd\n");
	scanf("%d", &size);
        if((size < 3) || (size%2 == 0)){
                printf("Size must be an odd number >= 3.\n");
                exit(1);
        }

	return size;

}

/* generate_magic constructs a magic square of size n
 * using the Siamese algorithm and returns the Square struct
 */
Square * generate_magic(int n)
{
	int size = n;
	Square *result = malloc(sizeof(Square));
	result->size = size;
	result->array = malloc(sizeof(int*)*size);

	for(int i = 0; i < size; i++){
		*((result->array) + i) = malloc(sizeof(int)*size);
	}
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			*(*((result->array) + i) + j) = 0;

		}
	}			
	int row = 0;
	int col = 0;
	col = size / 2;
	*(*((result->array) + row) + col) = 1;

	for(int i = 2; i < (size*size + 1); i++){
		int rowf = row;
		int colf = col;

		if(row == 0){
			row = size - 1;
		}
		else{
			row--;
		}
		if (col == size - 1){
			col = 0;
		}
		else{
			col++;
		}
		
		while(*(*((result->array) + row) + col) != 0){
			if(row == size-1){
				row = 0;
			}
			else{
				row = rowf + 1;
			}
			col = colf;
		}	
		*(*((result->array) + row) + col) = i;
	}
	
	return result;
}

/* write_to_file opens up a new file(or overwrites the existing file)
 * and writes out the square in the format expected by verify_magic.c
 */
void write_to_file(Square * square, char *filename)
{
	//opens files for editing, quits if error.
	FILE *fp;
	fp = fopen(filename, "w+");
	if(fp == NULL || fp == 0){
		printf("Cannot open file for reading.\n");
		exit(1);	
	}

	//various variables
	int **array = square->array;
	int size = square->size;
	int col2rem = 0;
	fprintf(fp, "%d\n", size);

	//writes square to file
	for(int i = 0; i < size; i++){
		for(int j = 0; j < size-1; j++){
			fprintf(fp, "%d,", *(*(array+ i)+j));
			col2rem = j;
		}
		fprintf(fp, "%d\n", *(*(array + i) + (col2rem + 1)));
	}
	fclose(fp);
}
