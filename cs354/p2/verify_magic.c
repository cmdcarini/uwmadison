////////////////////////////////////////////////////////////////////////////////
// Main File:        verify_magic.c
// This File:        verify_magic.c
// Other Files:      generate_magic.c, magic-3.txt
// Semester:         CS 354 Spring 2017
//
// Author:           Marco Carini
// Email:            carini@cs.wisc.edu
// CS Login:         carini
//
////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Structure representing Square
// size: dimension(number of rows/columns) of the square
// array: 2D array of integers
typedef struct _Square {
	int size;
	int **array;
} Square;

Square * construct_square(char *filename);
int verify_magic(Square * square);

int main(int argc, char *argv[])
{
	// Check input arguments to get filename
	char *filename = *(argv + 1);

	// Construct square
	Square *resultSquare = construct_square(filename);

	// Verify if it's a magic square and print true or false
	int result = verify_magic(resultSquare);
	if(result == 1){
		printf("true\n");
	}
	else{
		printf("false\n");
	}

	for(int i = 0; i < (resultSquare->size); i++){
		free(*((resultSquare->array) + i));
	}
	free(resultSquare->array);
	free(resultSquare);	
	return 0;
}

/* construct_square reads the input file to initialize a square struct
 * from the contents of the file and returns the square.
 * The format of the file is defined in the assignment specifications
 */
Square * construct_square(char *filename)
{
	int ssize = 0;
	int tmp = 0;
	
	// Open and read the file
	FILE * opened_file;
	opened_file = fopen(filename, "r");
	if(opened_file == 0){
		printf("Cannot open file for reading.\n");
		exit(1);
	}

	// Read the first line to get the square size
	fscanf(opened_file, "%d", &ssize);
	
	// Initialize a new Square struct of that size
	Square *result = malloc(sizeof(Square));
        result->size = 0;
	result->size = ssize;
        result->array = malloc(sizeof(int*)*(ssize));
	for(int i = 0; i < ssize; i++){
		*((result->array) + i) = malloc(sizeof(int)*ssize);
	}
	
	// Read the rest of the file to fill up the square
	for(int i = 0; i < result->size; i++){
		for(int j = 0; j < result->size; j++){
			fscanf(opened_file, "%d,", &tmp);
			*(*((result->array) + i) + j) = tmp;
		}
	}
	
	fclose(opened_file);	
	return result;
}

/* verify_magic verifies if the square is a magic square
 * 
 * returns 1(true) or 0(false)
 */
int verify_magic(Square * square)
{
	
	int **array = square->array;
	int size = square->size;
	int *sumTable = malloc(sizeof(int) * size);
	int isMagic = 0;
	int cols = 0; 
	int rows = 0 ;
	int diag1 = 0;
	int diag2 = 0;
	//check all the rows
	for(int i = 0; i < size; i++){
		*(sumTable +i) = 0;
	}
	for(int i = 0; i < size; i++){
		for(int j = 0; j < size; j++){
			*(sumTable + i) = *(sumTable + i) + *(*(array + i) + j);
		}
	}
	
	for(int i = 0; i < (size-1); i++){
		if((*(sumTable + i)) == (*(sumTable+(i+1)))){
			rows = 1;
		} 
		else{
			rows = 0;
			break; 
		}
	}	
	
	for(int i = 0; i < size; i++){
		*(sumTable + i) = 0;
	}
	
	for(int i = 0; i < size; i++){
		for(int j = 0; j < size; j++){
			*(sumTable + i) = *(sumTable + i) + *(*(array + j) + i);
		}
	}
	
	for(int i = 0; i < (size -1); i++){
		if((*(sumTable + i)) == (*(sumTable +(i+ 1)))){
			cols = 1;
		}
		else{
			cols = 0;
			break;
		}
	}
	
	for(int i = 0; i < size; i++){
                *(sumTable + i) = 0;
        }
	
	for(int i = 0; i < size; i++){
		for(int j = 0; j < size; j++){
			*(sumTable +i) = *(sumTable + i) + *(*(array + j) + j);
		}
	}

		
	for(int i = 0; i < (size-1); i++){
		if((*(sumTable + i)) == (*(sumTable +(i+ 1)))){
			diag1 = 1;
		}
		
		else{
			diag1 = 0;
			break;
		}

	}	
	
	for(int i = 0; i < size; i++){
                *(sumTable + i) = 0;
        }
	
	for(int i = size -1; i >=0 ; i--){
		for(int j = 0; j < size; j++){
			*(sumTable + j) = *(sumTable + j) + *(*(array + i) + j);
		}

	
	}
	for(int i = 0; i < (size-1); i++){
		if((*(sumTable +i)) == (*(sumTable +(i+ 1)))){
			diag2 = 1;
		}
		else{
			diag2 = 0;
			break;
		}
	}
	
	if((cols == rows) && (cols == diag1) && (cols == diag2)){
		isMagic = 1;
	}
	free(sumTable);
	return isMagic;
}
