////////////////////////////////////////////////////////////////////////////////
// Main File:        mem.c
// This File:        mem.c
// Other Files:      mem.h
// Semester:         CS 354 Spring 2017
//
// Author:           Marco Carini
// Email:            marco.carini@wisc.edu
// CS Login:         carini
//
////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include "mem.h"

/*
 * This structure serves as the header for each allocated and free block
 * It also serves as the footer for each free block
 * The blocks are ordered in the increasing order of addresses 
 */
typedef struct block_tag{

  int size_status;
  int help;
 /*
  * Size of the block is always a multiple of 4
  * => last two bits are always zero - can be used to store other information
  *
  * LSB -> Least Significant Bit (Last Bit)
  * SLB -> Second Last Bit 
  * LSB = 0 => free block
  * LSB = 1 => allocated/busy block
  * SLB = 0 => previous block is free
  * SLB = 1 => previous block is allocated/busy
  * 
  * When used as the footer the last two bits should be zero
  */

 /*
  * Examples:
  * 
  * For a busy block with a payload of 24 bytes (i.e. 24 bytes data + an additional 4 bytes for header)
  * Header:
  * If the previous block is allocated, size_status should be set to 31
  * If the previous block is free, size_status should be set to 29
  * 
  * For a free block of size 28 bytes (including 4 bytes for header + 4 bytes for footer)
  * Header:
  * If the previous block is allocated, size_status should be set to 30
  * If the previous block is free, size_status should be set to 28
  * Footer:
  * size_status should be 28
  * 
  */

} block_tag;

/* Global variable - This will always point to the first block
 * i.e. the block with the lowest address */
block_tag *first_block = NULL;

/* Global variable - Total available memory */
int total_mem_size = 0;

/*
  * This function gets the next block from parameter
  * currPos, and returns the next block if found.
  */


block_tag* next(block_tag* currPos){
	
	block_tag *next = NULL;
	block_tag *tmp = (void*)first_block + (total_mem_size) + 2;

	//if currPos is NULL, there is no next block
	if(currPos == NULL){
		return NULL;
	}
	
	next = (void*)currPos + (currPos->size_status -(currPos-> size_status % 4));
	
	//if next is NULL, currPos was the last block in the chain
	if(next == NULL){
                return NULL;
        }
	//if currPoss was invalid, there is no next
	if(currPos->size_status <= 0 || (currPos->size_status > total_mem_size)){
		return NULL;
	}
	
	//if next is invalid, there is no next.
	if(next >= tmp){
		return NULL;		
	}
	return next;
		
}


/* 
 * Function for allocating 'size' bytes
 * Returns address of the payload in the allocated block on success 
 * Returns NULL on failure 
 * Here is what this function should accomplish 
 * - If size is less than equal to 0 - Return NULL
 * - Round up size to a multiple of 4 
 * - Traverse the list of blocks and allocate the best free block which can accommodate the requested size 
 * - Also, when allocating a block - split it into two blocks when possible 
 * Tips: Be careful with pointer arithmetic 
 */
void* Mem_Alloc(int size){

	//If size <= 0 return NULL
	if(size <= 0){
		return NULL;
	}
	
	//if size is invalid, can't alloc anything
	if(size > total_mem_size){
		return NULL;
	}

	//round up to multiple of 4
	int rem = size % 4;
	if(rem != 0){
		size = size + 4 - rem;
	}
	
	block_tag *currBest = NULL;
	block_tag *currPos = first_block;
	block_tag *prevBest = NULL;
	block_tag *prevPos = NULL;		
	
	while(currPos != NULL){
		if((currPos->size_status & 1) == 0){
			if(currPos->size_status >= size + sizeof(block_tag)){
				//initial condition
				if(currBest == NULL){
					currBest = currPos;
					prevBest = prevPos;
				}
				
				//best case
				else if(currPos->size_status == size + sizeof(block_tag)){
					currBest = currPos;
					prevBest = prevPos;
					break;
	

				}
				//finds a new better case
				else if((currPos -> size_status - size) < (currBest -> size_status -size)){
					currBest = currPos;
					prevBest = prevPos;
				}
			}		
		}

		prevPos = currPos;
		currPos = next(currPos);
	
	}


	//allocation step
	if(currBest != NULL){
		if(currBest -> size_status -size + sizeof(block_tag) >= sizeof(block_tag) + 4){
			block_tag *nb = (void*)currBest + sizeof(block_tag) + size;
			nb->size_status = (currBest->size_status-(currBest->size_status %4)) - sizeof(block_tag) -size;
			currBest -> size_status = size + sizeof(block_tag);
		}
	
		if(prevBest == NULL || ((prevBest->size_status & 1) != 0)){
			currBest -> size_status += 2;
		}
		currBest -> size_status += 1;
		return (void*)currBest + sizeof(block_tag);
	}
	else{
		return NULL;
	}
	
}

/* 
 * Function for freeing up a previously allocated block 
 * Argument - ptr: Address of the payload of the allocated block to be freed up 
 * Returns 0 on success 
 * Returns -1 on failure 
 * Here is what this function should accomplish 
 * - Return -1 if ptr is NULL
 * - Return -1 if ptr is not within the range of memory allocated by Mem_Init()
 * - Return -1 if ptr is not 4 byte aligned
 * - Mark the block as free 
 * - Coalesce if one or both of the immediate neighbours are free 
 */
int Mem_Free(void *ptr){
	
	//if ptr is null, there's nothing to free
	if(ptr == NULL){
		return -1;
	}
	
	//int to determine, if there is anything to free in the first place
	int isValid = 0;
	block_tag *tmp = first_block;
	ptr = (void*)ptr - sizeof(block_tag);
	while(tmp != NULL){
		if((void*)tmp == ptr){
			isValid = 1;
			break;
		}
		tmp = next(tmp);
	}	
	
	//provided there is stuff to free
	if(isValid){
		block_tag *currPos =(block_tag*)ptr;
		currPos -> size_status = currPos -> size_status - 1;
		block_tag *nextPos = next(currPos);
		block_tag *prevPos = first_block;
		if(currPos == first_block){
			prevPos = NULL;
		}
		else{
			//loop to place prevPos right before currPos
			while(next(prevPos) != currPos){
				prevPos = next(prevPos);
			}
	
		}

		int left = 0;
		int right = 0;
		
		//nothing to do here
		if(prevPos == NULL && nextPos == NULL){
			return 0;
		}
		
		//if there is not a prev block, but is a next block
		else if(prevPos == NULL && nextPos != NULL){
			//right was allocated
			if((nextPos->size_status & 1) != 0){
				return 0;
			} 
			//right was free! coalesce it
			else if((nextPos->size_status & 1) == 0){
				right = 1;
			}
		
		}
		
		//if there is a not a next block, but is a prev
		else if(prevPos != NULL && nextPos == NULL){
			//left was allocated
			if((prevPos->size_status & 1) != 0){
				return 0;

			}
		//	left is free! coalesce it
			else if((prevPos->size_status & 1) == 0){
				left = 1;

			}
		
		}
		//do nothing
		else if (((prevPos->size_status & 1) != 0) && ((nextPos->size_status & 1) != 0)) {
			return 0;
		//left is free, coalesce it
		} else if (((prevPos->size_status & 1) == 0) && ((nextPos->size_status & 1) != 0)) {
			left = 1;
		//right is free, coalesce it
		} else if (((prevPos->size_status & 1) != 0) && ((nextPos->size_status & 1) == 0)) {
			right = 1;
		//both are free, coalesce them!
		} else if (((prevPos->size_status & 1) == 0) && ((nextPos->size_status & 1) == 0)) {
			left = 1;
			right = 1;
		}

		int currSize = (currPos->size_status) - (currPos->size_status%4);
		int prevSize = 0;
		if(prevPos != NULL){
			prevSize = (prevPos ->size_status) - (prevPos->size_status%4);
		}
		int nextSize = (nextPos -> size_status) - (nextPos->size_status%4);
		
		//if both need coalescing
		if (left && right) {
			int size = 0;
			if((prevPos->size_status & 2) == 2){
                                size = 2;
                        }

			prevPos->size_status = prevSize + currSize + nextSize + size;
			block_tag *nextFoot2 = (void*)currPos + currSize - sizeof(block_tag);
			if ((void *)nextFoot2 == (void *)first_block + total_mem_size - sizeof(block_tag)) {
                        	return 0;
               		 }
			nextFoot2->size_status = (prevPos->size_status) - (prevPos->size_status%4);
			
		//if only left needs coalescing
		} else if (left && !right) {
			int size = 0;
			if((prevPos->size_status & 2) == 2){
				size = 2;
			}
			prevPos->size_status = prevSize + currSize + size;
			block_tag *currFoot = (void*)currPos + currSize - sizeof(block_tag);
                        currFoot -> size_status = (prevPos-> size_status) - (prevPos->size_status%4);
                }
		//if only right needs coalescing
		 else if (!left && right) {
			int size = 0;
			if((currPos->size_status & 2) == 2){
				size = 2;
			}
			currPos->size_status = currSize + nextSize + size;
			block_tag *nextFoot = (void*)nextPos + nextSize - sizeof(block_tag);
			if ((void *)nextFoot == (void *)first_block + total_mem_size - sizeof(block_tag)) {
                       		return 0;
                         }

			nextFoot->size_status = (currPos->size_status) -(currPos->size_status%4);
		}

		return 0;
	} else {
		return -1;
	}
	

}

/*
 * Function used to initialize the memory allocator
 * Not intended to be called more than once by a program
 * Argument - sizeOfRegion: Specifies the size of the chunk which needs to be allocated
 * Returns 0 on success and -1 on failure 
 */
int Mem_Init(int sizeOfRegion){
  int pagesize;
  int padsize;
  int fd;
  int alloc_size;
  void* space_ptr;
  static int allocated_once = 0;
  
  if(0 != allocated_once){
    fprintf(stderr,"Error:mem.c: Mem_Init has allocated space during a previous call\n");
    return -1;
  }
  if(sizeOfRegion <= 0){
    fprintf(stderr,"Error:mem.c: Requested block size is not positive\n");
    return -1;
  }

  // Get the pagesize
  pagesize = getpagesize();

  // Calculate padsize as the padding required to round up sizeOfRegion to a multiple of pagesize
  padsize = sizeOfRegion % pagesize;
  padsize = (pagesize - padsize) % pagesize;

  alloc_size = sizeOfRegion + padsize;

  // Using mmap to allocate memory
  fd = open("/dev/zero", O_RDWR);
  if(-1 == fd){
    fprintf(stderr,"Error:mem.c: Cannot open /dev/zero\n");
    return -1;
  }
  space_ptr = mmap(NULL, alloc_size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
  if (MAP_FAILED == space_ptr){
    fprintf(stderr,"Error:mem.c: mmap cannot allocate space\n");
    allocated_once = 0;
    return -1;
  }
  
  allocated_once = 1;
  
  // Intialising total available memory size
  total_mem_size = alloc_size;

  // To begin with there is only one big free block
  first_block = (block_tag*) space_ptr;
  
  // Setting up the header
  first_block->size_status = alloc_size;
  // Marking the previous block as busy
  first_block->size_status += 2;

  // Setting up the footer
  block_tag *footer = (block_tag*)((char*)first_block + alloc_size - 4);
  footer->size_status = alloc_size;
  
  return 0;
}

/* 
 * Function to be used for debugging 
 * Prints out a list of all the blocks along with the following information for each block 
 * No.      : serial number of the block 
 * Status   : free/busy 
 * Prev     : status of previous block free/busy
 * t_Begin  : address of the first byte in the block (this is where the header starts) 
 * t_End    : address of the last byte in the block 
 * t_Size   : size of the block (as stored in the block header)(including the header/footer)
 */ 
void Mem_Dump() {
  int counter;
  char status[5];
  char p_status[5];
  char *t_begin = NULL;
  char *t_end = NULL;
  int t_size;

  block_tag *current = first_block;
  counter = 1;

  int busy_size = 0;
  int free_size = 0;
  int is_busy = -1;

  fprintf(stdout,"************************************Block list***********************************\n");
  fprintf(stdout,"No.\tStatus\tPrev\tt_Begin\t\tt_End\t\tt_Size\n");
  fprintf(stdout,"---------------------------------------------------------------------------------\n");
  
  while(current < (block_tag*)((char*)first_block + total_mem_size)){

    t_begin = (char*)current;
    
    t_size = current->size_status;
    
    if(t_size & 1){
      // LSB = 1 => busy block
      strcpy(status,"Busy");
      is_busy = 1;
      t_size = t_size - 1;
    }
    else{
      strcpy(status,"Free");
      is_busy = 0;
    }

    if(t_size & 2){
      strcpy(p_status,"Busy");
      t_size = t_size - 2;
    }
    else strcpy(p_status,"Free");

    if (is_busy) busy_size += t_size;
    else free_size += t_size;

    t_end = t_begin + t_size - 1;
    
    fprintf(stdout,"%d\t%s\t%s\t0x%08lx\t0x%08lx\t%d\n",counter,status,p_status,
                    (unsigned long int)t_begin,(unsigned long int)t_end,t_size);
    
    current = (block_tag*)((char*)current + t_size);
    counter = counter + 1;
  }
  fprintf(stdout,"---------------------------------------------------------------------------------\n");
  fprintf(stdout,"*********************************************************************************\n");

  fprintf(stdout,"Total busy size = %d\n",busy_size);
  fprintf(stdout,"Total free size = %d\n",free_size);
  fprintf(stdout,"Total size = %d\n",busy_size+free_size);
  fprintf(stdout,"*********************************************************************************\n");
  fflush(stdout);
  return;
}
