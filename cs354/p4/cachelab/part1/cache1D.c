////////////////////////////////////////////////////////////////////////////////
// Main File:        csim.c
// This File:        cache1D.c
// Other Files:      cache2Drows.c, cache2Dcols.c, p4questions.txt, csim.c
// Semester:         CS 354 Spring 2017
//
// Author:           Marco Carini
// Email:            carini@cs.wisc.edu
// CS Login:         carini
//
//////////////////////////// 80 columns wide ///////////////////////////////////

//creates int array of size 100000
int arr[100000];

//iterates over the array and sets each value to its index
int main(){
    for(int i = 0; i < 100000; i++){
        arr[i] = i;
    }
}