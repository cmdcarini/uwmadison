////////////////////////////////////////////////////////////////////////////////
// Main File:        csim.c
// This File:        cache1D.c
// Other Files:      cache2Drows.c, cache2Dcols.c, p4questions.txt, csim.c
// Semester:         CS 354 Spring 2017
//
// Author:           Marco Carini
// Email:            carini@cs.wisc.edu
// CS Login:         carini
//
//////////////////////////// 80 columns wide ///////////////////////////////////

//creates 2D int array of size 3000 x 500
int arr2D[3000][500];

//steps through 2D int array in row-wise order
int main(){

    for(int row = 0; row < 3000; row++){
        for(int col = 0; col < 500; col++){
            arr2D[row][col] = row + col;
        }
    }

}
