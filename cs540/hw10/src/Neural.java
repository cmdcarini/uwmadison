import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Scanner;



public class Neural {
	public static void main(String args[]) throws FileNotFoundException {
		int flag = Integer.valueOf(args[0]);
		
		if(flag == 100) {
			double [] weights = {Double.valueOf(args[1]), Double.valueOf(args[2]), Double.valueOf(args[3]),
					Double.valueOf(args[4]), Double.valueOf(args[5]), Double.valueOf(args[6]),
					Double.valueOf(args[7]), Double.valueOf(args[8]), Double.valueOf(args[9])};
			double x1 = Double.valueOf(args[10]);
			double x2 = Double.valueOf(args[11]);
			
			double [] u = new double[3];
			double [] v = new double[3];
			
			u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
			v[0] = Math.max(u[0], 0);
			
			u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
			v[1] = Math.max(u[1], 0);
			
			u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
			v[2] = 1/(1+Math.exp(-(u[2])));
			
			System.out.printf("%.5f %.5f %.5f %.5f %.5f %.5f", u[0], v[0], u[1], v[1], u[2], v[2]);
			
		}

		if(flag == 200) {
			double [] weights = {Double.valueOf(args[1]), Double.valueOf(args[2]), Double.valueOf(args[3]),
					Double.valueOf(args[4]), Double.valueOf(args[5]), Double.valueOf(args[6]),
					Double.valueOf(args[7]), Double.valueOf(args[8]), Double.valueOf(args[9])};
			double x1 = Double.valueOf(args[10]);
			double x2 = Double.valueOf(args[11]);
			double y = Double.valueOf(args[12]);
			
			double [] u = new double[3];
			double [] v = new double[3];
			
			u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
			v[0] = Math.max(u[0], 0);
			
			u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
			v[1] = Math.max(u[1], 0);
			
			u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
			v[2] = 1/(1+Math.exp(-(u[2])));
			
			double e = 0.5*Math.pow((v[2] - y), 2);
			double partiale = v[2] - y;
			double partialu = partiale*(v[2]*(1-v[2]));
			
			System.out.printf("%.5f %.5f %.5f", e, partiale, partialu);				
		}
		if(flag == 300) {
			double [] weights = {Double.valueOf(args[1]), Double.valueOf(args[2]), Double.valueOf(args[3]),
					Double.valueOf(args[4]), Double.valueOf(args[5]), Double.valueOf(args[6]),
					Double.valueOf(args[7]), Double.valueOf(args[8]), Double.valueOf(args[9])};
			double x1 = Double.valueOf(args[10]);
			double x2 = Double.valueOf(args[11]);
			double y = Double.valueOf(args[12]);
			
			double [] u = new double[3];
			double [] v = new double[3];
			
			u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
			v[0] = Math.max(u[0], 0);
			
			u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
			v[1] = Math.max(u[1], 0);
			
			u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
			v[2] = 1/(1+Math.exp(-(u[2])));
			
			double ec = 0.5*Math.pow((v[2] - y), 2);
			double partialec = v[2] - y;
			double partialuc = partialec*(v[2]*(1-v[2]));
			
			double partialea = weights[7]*partialuc;
			double partialeb = weights[8]*partialuc;
			
			int factor = (u[0] >= 0) ? 1 : 0;		
			double partialeau = partialea*(factor);
			
			factor = (u[1] >= 0) ? 1 : 0;
			double partialebu = partialeb*factor;
			
			System.out.printf("%.5f %.5f %.5f %.5f", partialea, partialeau, partialeb, partialebu);	
			
		}
		if(flag == 400) {
			double [] weights = {Double.valueOf(args[1]), Double.valueOf(args[2]), Double.valueOf(args[3]),
					Double.valueOf(args[4]), Double.valueOf(args[5]), Double.valueOf(args[6]),
					Double.valueOf(args[7]), Double.valueOf(args[8]), Double.valueOf(args[9])};
			double x1 = Double.valueOf(args[10]);
			double x2 = Double.valueOf(args[11]);
			double y = Double.valueOf(args[12]);
			
			double [] u = new double[3];
			double [] v = new double[3];
			
			u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
			//System.out.println(u[0]);
			v[0] = Math.max(u[0], 0);
			
			u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
			v[1] = Math.max(u[1], 0);
			
			u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
			v[2] = 1/(1+Math.exp(-(u[2])));
			
			double ec = 0.5*Math.pow((v[2] - y), 2);
			double partialec = v[2] - y;
			double partialuc = partialec*(v[2]*(1-v[2]));
			
			double partialea = weights[7]*partialuc;
			double partialeb = weights[8]*partialuc;
			
			int factor = (u[0] >= 0) ? 1 : 0;		
			double partialeau = partialea*(factor);
			
			factor = (u[1] >= 0) ? 1 : 0;
			double partialebu = partialeb*factor;
			double partialw1 = 1*partialeau;
			double partialw2 = x1*partialeau;
			double partialw3 = x2*partialeau;
			double partialw4 = 1*partialebu;
			double partialw5 = x1*partialebu;
			double partialw6 = x2*partialebu;
			double partialw7 = 1*partialuc;
			double partialw8 = v[0]*partialuc;
			double partialw9 = v[1]*partialuc;
			
			System.out.printf("%.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f\n", partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9);
		}
		if(flag == 500) {
			double [] weights = {Double.valueOf(args[1]), Double.valueOf(args[2]), Double.valueOf(args[3]),
					Double.valueOf(args[4]), Double.valueOf(args[5]), Double.valueOf(args[6]),
					Double.valueOf(args[7]), Double.valueOf(args[8]), Double.valueOf(args[9])};
			double x1 = Double.valueOf(args[10]);
			double x2 = Double.valueOf(args[11]);
			double y = Double.valueOf(args[12]);
			double mu = Double.valueOf(args[13]);
			double [] u = new double[3];
			double [] v = new double[3];
			
			u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
			//System.out.println(u[0]);
			v[0] = Math.max(u[0], 0);
			
			u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
			v[1] = Math.max(u[1], 0);
			
			u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
			v[2] = 1/(1+Math.exp(-(u[2])));
			
			double ec = 0.5*Math.pow((v[2] - y), 2);
			double partialec = v[2] - y;
			double partialuc = partialec*(v[2]*(1-v[2]));
			
			double partialea = weights[7]*partialuc;
			double partialeb = weights[8]*partialuc;
			
			int factor = (u[0] >= 0) ? 1 : 0;		
			double partialeau = partialea*(factor);
			
			factor = (u[1] >= 0) ? 1 : 0;
			double partialebu = partialeb*factor;
			double partialw1 = 1*partialeau;
			double partialw2 = x1*partialeau;
			double partialw3 = x2*partialeau;
			double partialw4 = 1*partialebu;
			double partialw5 = x1*partialebu;
			double partialw6 = x2*partialebu;
			double partialw7 = 1*partialuc;
			double partialw8 = v[0]*partialuc;
			double partialw9 = v[1]*partialuc;
			
			double partialw[] = {partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9};
			for(int i = 0; i <9; i++) {
				System.out.printf("%.5f ", weights[i]);
			}
			System.out.printf("\n");
			
			System.out.printf("%.5f\n", ec);
			
			
			//System.out.printf("%.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f\n", partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9);
			for(int i = 0; i < 9; i++) {
				weights[i] = weights[i] - mu*partialw[i];
				System.out.printf("%.5f ", weights[i]);
			}
			System.out.printf("\n");
			u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
			//System.out.println(u[0]);
			v[0] = Math.max(u[0], 0);
			
			u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
			v[1] = Math.max(u[1], 0);
			
			u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
			v[2] = 1/(1+Math.exp(-(u[2])));
			
			ec = 0.5*Math.pow((v[2] - y), 2);
			System.out.printf("%.5f\n", ec);
			
		}
		if(flag == 600) {
			double [] weights = {Double.valueOf(args[1]), Double.valueOf(args[2]), Double.valueOf(args[3]),
					Double.valueOf(args[4]), Double.valueOf(args[5]), Double.valueOf(args[6]),
					Double.valueOf(args[7]), Double.valueOf(args[8]), Double.valueOf(args[9])};
			//URL url = insertionSort.class.getResource("hw2_midterm_A_train.txt");
			File fp = new File("./hw2_midterm_A_train.txt");
			Scanner scnr = new Scanner(fp);
			double x1 = scnr.nextDouble();
			double x2 = scnr.nextDouble();
			double y = scnr.nextDouble();
			double mu = Double.valueOf(args[10]);
			double [] u = new double[3];
			double [] v = new double[3];
			
			u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
			//System.out.println(u[0]);
			v[0] = Math.max(u[0], 0);
			
			u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
			v[1] = Math.max(u[1], 0);
			
			u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
			v[2] = 1/(1+Math.exp(-(u[2])));
			
			double ec = 0.5*Math.pow((v[2] - y), 2);
			double partialec = v[2] - y;
			double partialuc = partialec*(v[2]*(1-v[2]));
			
			double partialea = weights[7]*partialuc;
			double partialeb = weights[8]*partialuc;
			
			int factor = (u[0] >= 0) ? 1 : 0;		
			double partialeau = partialea*(factor);
			
			factor = (u[1] >= 0) ? 1 : 0;
			double partialebu = partialeb*factor;
			double partialw1 = 1*partialeau;
			double partialw2 = x1*partialeau;
			double partialw3 = x2*partialeau;
			double partialw4 = 1*partialebu;
			double partialw5 = x1*partialebu;
			double partialw6 = x2*partialebu;
			double partialw7 = 1*partialuc;
			double partialw8 = v[0]*partialuc;
			double partialw9 = v[1]*partialuc;
			
			double partialw[] = {partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9};
//			for(int i = 0; i <9; i++) {
//				System.out.printf("%.5f ", weights[i]);
//			}
//			System.out.printf("\n");
//			
//			System.out.printf("%.5f\n", ec);
			//double partialw[] = {partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9};
			System.out.printf("%.5f %.5f %.5f\n", x1, x2, y);
			//System.out.printf("%.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f\n", partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9);
			for(int i = 0; i < 9; i++) {
				weights[i] = weights[i] - mu*partialw[i];
				System.out.printf("%.5f ", weights[i]);
			}
			System.out.printf("\n");
			u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
			//System.out.println(u[0]);
			v[0] = Math.max(u[0], 0);
			
			u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
			v[1] = Math.max(u[1], 0);
			
			u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
			v[2] = 1/(1+Math.exp(-(u[2])));
			//Double ec = 0;
			ec = 0;
//			ec = 0.5*Math.pow((v[2] - y), 2);
//			ec *= 25;
//			System.out.printf("%.5f\n", ec);
			File eval = new File("./hw2_midterm_A_eval.txt");
			Scanner evalscnr = new Scanner(eval);
			Double evalx1 = evalscnr.nextDouble();
			Double evalx2 = evalscnr.nextDouble();
			Double evaly = evalscnr.nextDouble();
			Double []evalu= new Double[3];
			Double []evalv = new Double[3];
			for(int i = 0; i < 24; i++) {
				evalu[0] = weights[0]*1 + weights[1]*evalx1 + weights[2]*evalx2;
				//System.out.println(u[0]);
				evalv[0] = Math.max(evalu[0], 0);
				
				evalu[1] = weights[3]*1 + weights[4]*evalx1 + weights[5]*evalx2;
				evalv[1] = Math.max(evalu[1], 0);
				
				evalu[2] = weights[6]*1 + weights[7]*evalv[0] + weights[8]*evalv[1];
				evalv[2] = 1/(1+Math.exp(-(evalu[2])));
				ec += 0.5*(Math.pow((evalv[2] - evaly), 2));
				evalx1 = evalscnr.nextDouble();
				evalx2 = evalscnr.nextDouble();
				evaly = evalscnr.nextDouble();
			}
			evalu[0] = weights[0]*1 + weights[1]*evalx1 + weights[2]*evalx2;
			//System.out.println(u[0]);
			evalv[0] = Math.max(evalu[0], 0);
			
			evalu[1] = weights[3]*1 + weights[4]*evalx1 + weights[5]*evalx2;
			evalv[1] = Math.max(evalu[1], 0);
			
			evalu[2] = weights[6]*1 + weights[7]*evalv[0] + weights[8]*evalv[1];
			evalv[2] = 1/(1+Math.exp(-(evalu[2])));
			ec += 0.5*(Math.pow((evalv[2] - evaly), 2));
			System.out.printf("%.5f\n", ec);
			for(int j = 0; j < 66; j++) {
				x1 = scnr.nextDouble();
				x2 = scnr.nextDouble();
				y = scnr.nextDouble();
				u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
				//System.out.println(u[0]);
				v[0] = Math.max(u[0], 0);
				
				u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
				v[1] = Math.max(u[1], 0);
				
				u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
				v[2] = 1/(1+Math.exp(-(u[2])));
				
				ec = 0.5*Math.pow((v[2] - y), 2);
				
				
				partialec = v[2] - y;
				partialuc = partialec*(v[2]*(1-v[2]));

				partialea = weights[7]*partialuc;
				partialeb = weights[8]*partialuc;
				
				factor = (u[0] >= 0) ? 1 : 0;		
				partialeau = partialea*(factor);
				
				factor = (u[1] >= 0) ? 1 : 0;
				partialebu = partialeb*factor;
				partialw[0] = 1*partialeau;
				partialw[1] = x1*partialeau;
				partialw[2] = x2*partialeau;
				partialw[3] = 1*partialebu;
				partialw[4] = x1*partialebu;
				partialw[5] = x2*partialebu;
				partialw[6] = 1*partialuc;
				partialw[7] = v[0]*partialuc;
				partialw[8] = v[1]*partialuc;
				
				//double partialw[] = {partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9};
				System.out.printf("%.5f %.5f %.5f\n", x1, x2, y);
				//System.out.printf("%.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f\n", partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9);
				for(int i = 0; i < 9; i++) {
					weights[i] = weights[i] - mu*partialw[i];
					System.out.printf("%.5f ", weights[i]);
				}
				System.out.printf("\n");
				evalscnr = new Scanner(eval);
				ec = 0;
				evalu= new Double[3];
				evalv = new Double[3];
				for(int i = 0; i < 25; i++) {
					evalx1 = evalscnr.nextDouble();
					evalx2 = evalscnr.nextDouble();
					evaly = evalscnr.nextDouble();
					evalu[0] = weights[0]*1 + weights[1]*evalx1 + weights[2]*evalx2;
					//System.out.println(u[0]);
					evalv[0] = Math.max(evalu[0], 0);
					
					evalu[1] = weights[3]*1 + weights[4]*evalx1 + weights[5]*evalx2;
					evalv[1] = Math.max(evalu[1], 0);
					
					evalu[2] = weights[6]*1 + weights[7]*evalv[0] + weights[8]*evalv[1];
					evalv[2] = 1/(1+Math.exp(-(evalu[2])));
					ec += 0.5*(Math.pow((evalv[2] - evaly), 2));
				}
				System.out.printf("%.5f\n", ec);
			}
		}
		if(flag == 700) {
			double [] weights = {Double.valueOf(args[1]), Double.valueOf(args[2]), Double.valueOf(args[3]),
					Double.valueOf(args[4]), Double.valueOf(args[5]), Double.valueOf(args[6]),
					Double.valueOf(args[7]), Double.valueOf(args[8]), Double.valueOf(args[9])};
			//URL url = insertionSort.class.getResource("hw2_midterm_A_train.txt");
			File fp = new File("./hw2_midterm_A_train.txt");
			Scanner scnr = new Scanner(fp);
			double x1 = scnr.nextDouble();
			double x2 = scnr.nextDouble();
			double y = scnr.nextDouble();
			double mu = Double.valueOf(args[10]);
			double [] u = new double[3];
			double [] v = new double[3];
			int T = Integer.valueOf(args[11]);
			u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
			//System.out.println(u[0]);
			v[0] = Math.max(u[0], 0);
			
			u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
			v[1] = Math.max(u[1], 0);
			
			u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
			v[2] = 1/(1+Math.exp(-(u[2])));
			
			double ec = 0.5*Math.pow((v[2] - y), 2);
			double partialec = v[2] - y;
			double partialuc = partialec*(v[2]*(1-v[2]));
			
			double partialea = weights[7]*partialuc;
			double partialeb = weights[8]*partialuc;
			
			int factor = (u[0] >= 0) ? 1 : 0;		
			double partialeau = partialea*(factor);
			
			factor = (u[1] >= 0) ? 1 : 0;
			double partialebu = partialeb*factor;
			double partialw1 = 1*partialeau;
			double partialw2 = x1*partialeau;
			double partialw3 = x2*partialeau;
			double partialw4 = 1*partialebu;
			double partialw5 = x1*partialebu;
			double partialw6 = x2*partialebu;
			double partialw7 = 1*partialuc;
			double partialw8 = v[0]*partialuc;
			double partialw9 = v[1]*partialuc;
			
			double partialw[] = {partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9};
//			for(int i = 0; i <9; i++) {
//				System.out.printf("%.5f ", weights[i]);
//			}
//			System.out.printf("\n");
//			
//			System.out.printf("%.5f\n", ec);
			//double partialw[] = {partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9};
			//System.out.printf("%.5f %.5f %.5f\n", x1, x2, y);
			//System.out.printf("%.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f\n", partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9);
			for(int i = 0; i < 9; i++) {
				weights[i] = weights[i] - mu*partialw[i];
				///System.out.printf("%.5f ", weights[i]);
			}
			//System.out.printf("\n");
			u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
			//System.out.println(u[0]);
			v[0] = Math.max(u[0], 0);
			
			u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
			v[1] = Math.max(u[1], 0);
			
			u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
			v[2] = 1/(1+Math.exp(-(u[2])));
			//Double ec = 0;
			ec = 0;
//			ec = 0.5*Math.pow((v[2] - y), 2);
//			ec *= 25;
//			System.out.printf("%.5f\n", ec);
			File eval = new File("./hw2_midterm_A_eval.txt");
			Scanner evalscnr = new Scanner(eval);
			Double evalx1 = evalscnr.nextDouble();
			Double evalx2 = evalscnr.nextDouble();
			Double evaly = evalscnr.nextDouble();
			Double []evalu= new Double[3];
			Double []evalv = new Double[3];
			for(int i = 0; i < 24; i++) {
				evalu[0] = weights[0]*1 + weights[1]*evalx1 + weights[2]*evalx2;
				//System.out.println(u[0]);
				evalv[0] = Math.max(evalu[0], 0);
				
				evalu[1] = weights[3]*1 + weights[4]*evalx1 + weights[5]*evalx2;
				evalv[1] = Math.max(evalu[1], 0);
				
				evalu[2] = weights[6]*1 + weights[7]*evalv[0] + weights[8]*evalv[1];
				evalv[2] = 1/(1+Math.exp(-(evalu[2])));
				ec += 0.5*(Math.pow((evalv[2] - evaly), 2));
				evalx1 = evalscnr.nextDouble();
				evalx2 = evalscnr.nextDouble();
				evaly = evalscnr.nextDouble();
			}
			evalu[0] = weights[0]*1 + weights[1]*evalx1 + weights[2]*evalx2;
			//System.out.println(u[0]);
			evalv[0] = Math.max(evalu[0], 0);
			
			evalu[1] = weights[3]*1 + weights[4]*evalx1 + weights[5]*evalx2;
			evalv[1] = Math.max(evalu[1], 0);
			
			evalu[2] = weights[6]*1 + weights[7]*evalv[0] + weights[8]*evalv[1];
			evalv[2] = 1/(1+Math.exp(-(evalu[2])));
			ec += 0.5*(Math.pow((evalv[2] - evaly), 2));
			//System.out.printf("%.5f\n", ec);
			int bounds = 66;
			for(int epoch = 0; epoch < T; epoch++) {
				if(epoch > 0)  bounds = 67;
			for(int j = 0; j < bounds; j++) {
				x1 = scnr.nextDouble();
				x2 = scnr.nextDouble();
				y = scnr.nextDouble();
				u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
				//System.out.println(u[0]);
				v[0] = Math.max(u[0], 0);
				
				u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
				v[1] = Math.max(u[1], 0);
				
				u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
				v[2] = 1/(1+Math.exp(-(u[2])));
				
				ec = 0.5*Math.pow((v[2] - y), 2);
				
				
				partialec = v[2] - y;
				partialuc = partialec*(v[2]*(1-v[2]));

				partialea = weights[7]*partialuc;
				partialeb = weights[8]*partialuc;
				
				factor = (u[0] >= 0) ? 1 : 0;		
				partialeau = partialea*(factor);
				
				factor = (u[1] >= 0) ? 1 : 0;
				partialebu = partialeb*factor;
				partialw[0] = 1*partialeau;
				partialw[1] = x1*partialeau;
				partialw[2] = x2*partialeau;
				partialw[3] = 1*partialebu;
				partialw[4] = x1*partialebu;
				partialw[5] = x2*partialebu;
				partialw[6] = 1*partialuc;
				partialw[7] = v[0]*partialuc;
				partialw[8] = v[1]*partialuc;
				
				//double partialw[] = {partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9};
				//System.out.printf("%.5f %.5f %.5f\n", x1, x2, y);
				//System.out.printf("%.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f\n", partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9);
				for(int i = 0; i < 9; i++) {
					weights[i] = weights[i] - mu*partialw[i];
					//System.out.printf("%.5f ", weights[i]);
				}
				//System.out.printf("\n");
				
			}
			for(int i = 0; i < 9; i++) {
				//weights[i] = weights[i] - mu*partialw[i];
				System.out.printf("%.5f ", weights[i]);
			}
			System.out.printf("\n");

			evalscnr = new Scanner(eval);
			ec = 0;
			evalu= new Double[3];
			evalv = new Double[3];
			for(int i = 0; i < 25; i++) {
				evalx1 = evalscnr.nextDouble();
				evalx2 = evalscnr.nextDouble();
				evaly = evalscnr.nextDouble();
				evalu[0] = weights[0]*1 + weights[1]*evalx1 + weights[2]*evalx2;
				//System.out.println(u[0]);
				evalv[0] = Math.max(evalu[0], 0);
				
				evalu[1] = weights[3]*1 + weights[4]*evalx1 + weights[5]*evalx2;
				evalv[1] = Math.max(evalu[1], 0);
				
				evalu[2] = weights[6]*1 + weights[7]*evalv[0] + weights[8]*evalv[1];
				evalv[2] = 1/(1+Math.exp(-(evalu[2])));
				ec += 0.5*(Math.pow((evalv[2] - evaly), 2));
			}
			System.out.printf("%.5f\n", ec);
			scnr = new Scanner(fp);
			evalscnr = new Scanner(eval);
		}
		}
		if(flag == 800) {
			double [] weights = {Double.valueOf(args[1]), Double.valueOf(args[2]), Double.valueOf(args[3]),
					Double.valueOf(args[4]), Double.valueOf(args[5]), Double.valueOf(args[6]),
					Double.valueOf(args[7]), Double.valueOf(args[8]), Double.valueOf(args[9])};
			//URL url = insertionSort.class.getResource("hw2_midterm_A_train.txt");
			File fp = new File("./hw2_midterm_A_train.txt");
			Scanner scnr = new Scanner(fp);
			double x1 = scnr.nextDouble();
			double x2 = scnr.nextDouble();
			double y = scnr.nextDouble();
			double mu = Double.valueOf(args[10]);
			double [] u = new double[3];
			double [] v = new double[3];
			int T = Integer.valueOf(args[11]);
			double prevec = 0;
			u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
			//System.out.println(u[0]);
			v[0] = Math.max(u[0], 0);
			
			u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
			v[1] = Math.max(u[1], 0);
			
			u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
			v[2] = 1/(1+Math.exp(-(u[2])));
			
			double ec = 0.5*Math.pow((v[2] - y), 2);
			double partialec = v[2] - y;
			double partialuc = partialec*(v[2]*(1-v[2]));
			
			double partialea = weights[7]*partialuc;
			double partialeb = weights[8]*partialuc;
			
			int factor = (u[0] >= 0) ? 1 : 0;		
			double partialeau = partialea*(factor);
			
			factor = (u[1] >= 0) ? 1 : 0;
			double partialebu = partialeb*factor;
			double partialw1 = 1*partialeau;
			double partialw2 = x1*partialeau;
			double partialw3 = x2*partialeau;
			double partialw4 = 1*partialebu;
			double partialw5 = x1*partialebu;
			double partialw6 = x2*partialebu;
			double partialw7 = 1*partialuc;
			double partialw8 = v[0]*partialuc;
			double partialw9 = v[1]*partialuc;
			
			double partialw[] = {partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9};
//			for(int i = 0; i <9; i++) {
//				System.out.printf("%.5f ", weights[i]);
//			}
//			System.out.printf("\n");
//			
//			System.out.printf("%.5f\n", ec);
			//double partialw[] = {partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9};
			//System.out.printf("%.5f %.5f %.5f\n", x1, x2, y);
			//System.out.printf("%.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f\n", partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9);
			for(int i = 0; i < 9; i++) {
				weights[i] = weights[i] - mu*partialw[i];
				///System.out.printf("%.5f ", weights[i]);
			}
			//System.out.printf("\n");
			u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
			//System.out.println(u[0]);
			v[0] = Math.max(u[0], 0);
			
			u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
			v[1] = Math.max(u[1], 0);
			
			u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
			v[2] = 1/(1+Math.exp(-(u[2])));
			//Double ec = 0;
			ec = 0;
//			ec = 0.5*Math.pow((v[2] - y), 2);
//			ec *= 25;
//			System.out.printf("%.5f\n", ec);
			File eval = new File("./hw2_midterm_A_eval.txt");
			Scanner evalscnr = new Scanner(eval);
			Double evalx1 = evalscnr.nextDouble();
			Double evalx2 = evalscnr.nextDouble();
			Double evaly = evalscnr.nextDouble();
			Double []evalu= new Double[3];
			Double []evalv = new Double[3];
			for(int i = 0; i < 24; i++) {
				evalu[0] = weights[0]*1 + weights[1]*evalx1 + weights[2]*evalx2;
				//System.out.println(u[0]);
				evalv[0] = Math.max(evalu[0], 0);
				
				evalu[1] = weights[3]*1 + weights[4]*evalx1 + weights[5]*evalx2;
				evalv[1] = Math.max(evalu[1], 0);
				
				evalu[2] = weights[6]*1 + weights[7]*evalv[0] + weights[8]*evalv[1];
				evalv[2] = 1/(1+Math.exp(-(evalu[2])));
				ec += 0.5*(Math.pow((evalv[2] - evaly), 2));
				evalx1 = evalscnr.nextDouble();
				evalx2 = evalscnr.nextDouble();
				evaly = evalscnr.nextDouble();
			}
			evalu[0] = weights[0]*1 + weights[1]*evalx1 + weights[2]*evalx2;
			//System.out.println(u[0]);
			evalv[0] = Math.max(evalu[0], 0);
			
			evalu[1] = weights[3]*1 + weights[4]*evalx1 + weights[5]*evalx2;
			evalv[1] = Math.max(evalu[1], 0);
			
			evalu[2] = weights[6]*1 + weights[7]*evalv[0] + weights[8]*evalv[1];
			evalv[2] = 1/(1+Math.exp(-(evalu[2])));
			ec += 0.5*(Math.pow((evalv[2] - evaly), 2));
			//System.out.printf("%.5f\n", ec);
			int bounds = 66;
			for(int epoch = 0; epoch < T; epoch++) {
				if(epoch > 0)  bounds = 67;
			for(int j = 0; j < bounds; j++) {
				x1 = scnr.nextDouble();
				x2 = scnr.nextDouble();
				y = scnr.nextDouble();
				u[0] = weights[0]*1 + weights[1]*x1 + weights[2]*x2;
				//System.out.println(u[0]);
				v[0] = Math.max(u[0], 0);
				
				u[1] = weights[3]*1 + weights[4]*x1 + weights[5]*x2;
				v[1] = Math.max(u[1], 0);
				
				u[2] = weights[6]*1 + weights[7]*v[0] + weights[8]*v[1];
				v[2] = 1/(1+Math.exp(-(u[2])));
				
				ec = 0.5*Math.pow((v[2] - y), 2);
				
				
				partialec = v[2] - y;
				partialuc = partialec*(v[2]*(1-v[2]));

				partialea = weights[7]*partialuc;
				partialeb = weights[8]*partialuc;
				
				factor = (u[0] >= 0) ? 1 : 0;		
				partialeau = partialea*(factor);
				
				factor = (u[1] >= 0) ? 1 : 0;
				partialebu = partialeb*factor;
				partialw[0] = 1*partialeau;
				partialw[1] = x1*partialeau;
				partialw[2] = x2*partialeau;
				partialw[3] = 1*partialebu;
				partialw[4] = x1*partialebu;
				partialw[5] = x2*partialebu;
				partialw[6] = 1*partialuc;
				partialw[7] = v[0]*partialuc;
				partialw[8] = v[1]*partialuc;
				
				//double partialw[] = {partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9};
				//System.out.printf("%.5f %.5f %.5f\n", x1, x2, y);
				//System.out.printf("%.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f\n", partialw1, partialw2, partialw3, partialw4, partialw5, partialw6, partialw7, partialw8, partialw9);
				for(int i = 0; i < 9; i++) {
					weights[i] = weights[i] - mu*partialw[i];
					//System.out.printf("%.5f ", weights[i]);
				}
				//System.out.printf("\n");
				
			}
//			for(int i = 0; i < 9; i++) {
//				//weights[i] = weights[i] - mu*partialw[i];
//				System.out.printf("%.5f ", weights[i]);
//			}
//			System.out.printf("\n");

			evalscnr = new Scanner(eval);
			ec = 0;
			evalu= new Double[3];
			evalv = new Double[3];
			for(int i = 0; i < 25; i++) {
				evalx1 = evalscnr.nextDouble();
				evalx2 = evalscnr.nextDouble();
				evaly = evalscnr.nextDouble();
				evalu[0] = weights[0]*1 + weights[1]*evalx1 + weights[2]*evalx2;
				//System.out.println(u[0]);
				evalv[0] = Math.max(evalu[0], 0);
				
				evalu[1] = weights[3]*1 + weights[4]*evalx1 + weights[5]*evalx2;
				evalv[1] = Math.max(evalu[1], 0);
				
				evalu[2] = weights[6]*1 + weights[7]*evalv[0] + weights[8]*evalv[1];
				evalv[2] = 1/(1+Math.exp(-(evalu[2])));
				ec += 0.5*(Math.pow((evalv[2] - evaly), 2));
			}
			
			scnr = new Scanner(fp);
			evalscnr = new Scanner(eval);
			if(prevec - ec < 0 && prevec != 0) { 
				System.out.println(epoch+1);
				for(int i = 0; i < 9; i++) {
					//weights[i] = weights[i] - mu*partialw[i];
					System.out.printf("%.5f ", weights[i]);
				}
				System.out.printf("\n");
				System.out.printf("%.5f\n", ec);
				Scanner testscnr = new Scanner(new File("./hw2_midterm_A_test.txt"));
				Double testx1 = evalscnr.nextDouble();
				Double testx2 = evalscnr.nextDouble();
				Double testy = evalscnr.nextDouble();
				Double []testu= new Double[3];
				Double []testv = new Double[3];
				Double testec = 0.0;
				Double count = 0.0;
				Double actual = 0.0;
				for(int i = 0; i < 25; i++) {
					testx1 = testscnr.nextDouble();
					testx2 = testscnr.nextDouble();
					testy = testscnr.nextDouble();
					testu[0] = weights[0]*1 + weights[1]*testx1 + weights[2]*testx2;
					//System.out.println(u[0]);
					testv[0] = Math.max(testu[0], 0);
					
					testu[1] = weights[3]*1 + weights[4]*testx1 + weights[5]*testx2;
					testv[1] = Math.max(testu[1], 0);
					
					testu[2] = weights[6]*1 + weights[7]*testv[0] + weights[8]*testv[1];
					testv[2] = 1/(1+Math.exp(-(testu[2])));
					if(testv[2] > 0.5 && testy == 1 ||testv[2] < 0.5 && testy==0) {
						count++;
					}
//					if(testy == 1) {
//						actual++;
//					}
					//System.out.println(testv[2]);
					//testec += 0.5*(Math.pow((testv[2] - testy), 2));
				}
				Double result = count/25;
				System.out.printf("%.5f\n",result);
				testscnr.close();
				scnr.close();
				evalscnr.close();
				return;
			
			}
			else prevec = ec;
		}
		}
		
		
	}
}
