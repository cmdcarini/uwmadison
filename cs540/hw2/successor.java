import java.util.*;

public class successor {
    public static class JugState {
        int[] Capacity = new int[]{0,0,0};
        int[] Content = new int[]{0,0,0};
        public JugState(JugState copyFrom)
        {
            this.Capacity[0] = copyFrom.Capacity[0];
            this.Capacity[1] = copyFrom.Capacity[1];
            this.Capacity[2] = copyFrom.Capacity[2];
            this.Content[0] = copyFrom.Content[0];
            this.Content[1] = copyFrom.Content[1];
            this.Content[2] = copyFrom.Content[2];
        }
        public JugState()
        {
        }
        public JugState(int A,int B, int C)
        {
            this.Capacity[0] = A;
            this.Capacity[1] = B;
            this.Capacity[2] = C;
        }
        public JugState(int A,int B, int C, int a, int b, int c)
        {
            this.Capacity[0] = A;
            this.Capacity[1] = B;
            this.Capacity[2] = C;
            this.Content[0] = a;
            this.Content[1] = b;
            this.Content[2] = c;
        }
        
        public boolean isEqual(JugState j) {
        	
        		
        		return false;
        	
        }
        public void printContent()
        {
            System.out.println(this.Content[0] + " " + this.Content[1] + " " + this.Content[2]);
        }
        public boolean hasState(ArrayList<int []> stateList, int state[]) {
        		boolean result = false;
        		int curr[];
        		for(int i = 0; i < stateList.size(); i++) {
        			curr = stateList.get(i);
        			if(curr[0] == state[0] && curr[1] == state[1] && curr[2] == state[2]) 
        				result = true;		
        		}
        		return result; 		
        }
        public ArrayList<JugState> getNextStates(){
            ArrayList<JugState> successors = new ArrayList<>();
            ArrayList<int []> states = new ArrayList<>();
            // TODO add all successors to the list
            //adds the basic case of emptying the jug
            if(this.Content[0] != 0 && !hasState(states, new int[] {0, this.Content[1], this.Content[2]})) {
            		successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], 0, this.Content[1], this.Content[2]));
            		states.add(new int[]{0, this.Content[1], this.Content[2]});
            }
            if(this.Content[1] != 0 && !hasState(states, new int[] {this.Content[0], 0, this.Content[2]})) {
        			successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Content[0], 0, this.Content[2]));
        			states.add(new int[]{this.Content[0], 0, this.Content[2]});
            }
            if(this.Content[2] != 0 && !hasState(states, new int[]{this.Content[0], this.Content[1], 0})) {
        			successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Content[0], this.Content[1], 0));
        			states.add(new int[]{this.Content[0], this.Content[1], 0});
            }

            //fills the jug if not full already
            if(this.Content[0] != this.Capacity[0]) {
            		if(!hasState(states, new int[] {this.Capacity[0], this.Content[1], this.Content[2]})) {
            			successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Capacity[0], this.Content[1], this.Content[2]));
            			states.add(new int[] {this.Capacity[0], this.Content[1], this.Content[2]});
            		}
            		if(this.Content[0] + this.Content[1] > this.Capacity[0] && !hasState(states, new int[] {this.Capacity[0], this.Content[1] - (this.Capacity[0]-this.Content[0]), this.Content[2]})) {
                		successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Capacity[0], this.Content[1] - (this.Capacity[0]-this.Content[0]), this.Content[2]));
                		states.add(new int[] {this.Capacity[0], this.Content[1] - (this.Capacity[0]-this.Content[0]), this.Content[2]});
            		} else if(!hasState(states, new int [] {this.Content[0] + this.Content[1], 0, this.Content[2]}  )){
                		successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Content[0] + this.Content[1], 0, this.Content[2]));
                		states.add(new int [] {this.Content[0] + this.Content[1], 0, this.Content[2]});
            		}
            		
            		if(this.Content[0] + this.Content[2] > this.Capacity[0] && !hasState(states, new int [] {this.Capacity[0], this.Content[1], this.Content[2] - (this.Capacity[0]-this.Content[0]) } )) {
                		successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Capacity[0], this.Content[1], this.Content[2] - (this.Capacity[0]-this.Content[0])));
                		states.add(new int [] {this.Capacity[0], this.Content[1], this.Content[2] - (this.Capacity[0]-this.Content[0]) });
            		} else if(!hasState(states, new int [] {this.Content[0] + this.Content[2], this.Content[1], 0 } )){
                		successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Content[0] + this.Content[2], this.Content[1], 0));
                		states.add(new int [] {this.Content[0] + this.Content[2], this.Content[1], 0 });
            		}
            		
            }
            
            if(this.Content[1] != this.Capacity[1]) {
            		if(!hasState(states, new int [] {this.Content[0], this.Capacity[1], this.Content[2]} )) {
            			successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Content[0], this.Capacity[1], this.Content[2]));
            			states.add(new int [] {this.Content[0], this.Capacity[1], this.Content[2]});
            		}
        			if(this.Content[1] + this.Content[0] > this.Capacity[1] && !hasState(states, new int [] {this.Content[0] - (this.Capacity[1] - this.Content[1]), this.Capacity[1], this.Content[2] } )) {
                		successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Content[0] - (this.Capacity[1] - this.Content[1]), this.Capacity[1], this.Content[2]));
                		states.add(new int [] { this.Content[0] - (this.Capacity[1] - this.Content[1]), this.Capacity[1], this.Content[2]});
            		} else if(!hasState(states, new int [] {0, this.Content[1] + this.Content[0], this.Content[2] } )){
                		successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], 0, this.Content[1] + this.Content[0], this.Content[2]));
                		states.add(new int [] {0, this.Content[1] + this.Content[0], this.Content[2]});
            		}
        			
        			if(this.Content[1] + this.Content[2] > this.Capacity[1] && !hasState(states, new int [] {this.Content[0], this.Capacity[1], this.Content[2] - (this.Capacity[1] - this.Content[1]) } )) {
                		successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Content[0], this.Capacity[1], this.Content[2] - (this.Capacity[1] - this.Content[1])));
                		states.add(new int [] {this.Content[0], this.Capacity[1], this.Content[2] - (this.Capacity[1] - this.Content[1])});
        			} else if(!hasState(states, new int [] {this.Content[0], this.Content[1] + this.Content[2], 0} )){
                		successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Content[0], this.Content[1] + this.Content[2], 0));
                		states.add(new int [] {this.Content[0], this.Content[1] + this.Content[2], 0});
        			}
            
            }
            
            if(this.Content[2] != this.Capacity[2]) {
            		if(!hasState(states, new int [] {this.Content[0], this.Content[1], this.Capacity[2]})) {
            			successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Content[0], this.Content[1], this.Capacity[2]));
            			states.add(new int [] { this.Content[0], this.Content[1], this.Capacity[2]});
            		}
            		if(this.Content[2] + this.Content[0] > this.Capacity[2] && !hasState(states, new int [] {this.Content[0] - (this.Capacity[2]-this.Content[2]), this.Content[1], this.Capacity[2] } )) {
                		successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Content[0] - (this.Capacity[2]-this.Content[2]), this.Content[1], this.Capacity[2]));
                		states.add(new int [] {this.Content[0] - (this.Capacity[2]-this.Content[2]), this.Content[1], this.Capacity[2]});
            		} else if (!hasState(states, new int [] {0, this.Content[1], this.Content[2] + this.Content[0] } )){
                		successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], 0, this.Content[1], this.Content[2] + this.Content[0]));
                		states.add(new int [] {0, this.Content[1], this.Content[2] + this.Content[0]});
            		}
        			
        			if(this.Content[2] + this.Content[1] > this.Capacity[2] && !hasState(states, new int [] {this.Content[0], this.Content[1] - (this.Capacity[2]-this.Content[2]), this.Capacity[2] } )) {
                		successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Content[0], this.Content[1] - (this.Capacity[2]-this.Content[2]), this.Capacity[2]));
                		states.add(new int [] {this.Content[0], this.Content[1] - (this.Capacity[2]-this.Content[2]), this.Capacity[2]});
            		} else if(!hasState(states, new int [] {this.Content[0], 0, this.Content[2] + this.Content[1]})){
                		successors.add(new JugState(this.Capacity[0], this.Capacity[1], this.Capacity[2], this.Content[0], 0, this.Content[2] + this.Content[1]));
                		states.add(new int [] {this.Content[0], 0, this.Content[2] + this.Content[1]});
            		}            
            }
            return successors;
        }
    }

    public static void main(String[] args) {
        if( args.length != 6 )
        {
            System.out.println("Usage: java successor [A] [B] [C] [a] [b] [c]");
            return;
        }

        // parse command line arguments
        JugState a = new JugState();
        a.Capacity[0] = Integer.parseInt(args[0]);
        a.Capacity[1] = Integer.parseInt(args[1]);
        a.Capacity[2] = Integer.parseInt(args[2]);
        a.Content[0] = Integer.parseInt(args[3]);
        a.Content[1] = Integer.parseInt(args[4]);
        a.Content[2] = Integer.parseInt(args[5]);

        // Implement this function
        ArrayList<JugState> asist = a.getNextStates();

        // Print out generated successors
        for(int i=0;i< asist.size(); i++)
        {
            asist.get(i).printContent();
        }

        return;
    }
}

