public class Ice {
	static public void main(String args[]) {
		int flag = Integer.valueOf(args[0]);
		int [][] dataset = {{1855, 118},
				{1856, 151},
				{1857, 121},
				{1858, 96},
				{1859, 110},
				{1860, 117},
				{1861, 132},
				{1862, 104},
				{1863, 125},
				{1864, 118},
				{1865, 125},
				{1866, 123},
				{1867, 110},
				{1868, 127},
				{1869, 131},
				{1870, 99},
				{1871, 126},
				{1872, 144},
				{1873, 136},
				{1874, 126},
				{1875, 91},
				{1876, 130},
				{1877, 62},
				{1878, 112},
				{1879, 99},
				{1880, 161},
				{1881, 78},
				{1882, 124},
				{1883, 119},
				{1884, 124},
				{1885, 128},
				{1886, 131},
				{1887, 113},
				{1888, 88},
				{1889, 75},
				{1890, 111},
				{1891, 97},
				{1892, 112},
				{1893, 101},
				{1894, 101},
				{1895, 91},
				{1896, 110},
				{1897, 100},
				{1898, 130},
				{1899, 111},
				{1900, 107},
				{1901, 105},
				{1902, 89},
				{1903, 126},
				{1904, 108},
				{1905, 97},
				{1906, 94},
				{1907, 83},
				{1908, 106},
				{1909, 98},
				{1910, 101},
				{1911, 108},
				{1912, 99},
				{1913, 88},
				{1914, 115},
				{1915, 102},
				{1916, 116},
				{1917, 115},
				{1918, 82},
				{1919, 110},
				{1920, 81},
				{1921, 96},
				{1922, 125},
				{1923, 104},
				{1924, 105},
				{1925, 124},
				{1926, 103},
				{1927, 106},
				{1928, 96},
				{1929, 107},
				{1930, 98},
				{1931, 65},
				{1932, 115},
				{1933, 91},
				{1934, 94},
				{1935, 101},
				{1936, 121},
				{1937, 105},
				{1938, 97},
				{1939, 105},
				{1940, 96},
				{1941, 82},
				{1942, 116},
				{1943, 114},
				{1944, 92},
				{1945, 98},
				{1946, 101},
				{1947, 104},
				{1948, 96},
				{1949, 109},
				{1950, 122},
				{1951, 114},
				{1952, 81},
				{1953, 85},
				{1954, 92},
				{1955, 114},
				{1956, 111},
				{1957, 95},
				{1958, 126},
				{1959, 105},
				{1960, 108},
				{1961, 117},
				{1962, 112},
				{1963, 113},
				{1964, 120},
				{1965, 65},
				{1966, 98},
				{1967, 91},
				{1968, 108},
				{1969, 113},
				{1970, 110},
				{1971, 105},
				{1972, 97},
				{1973, 105},
				{1974, 107},
				{1975, 88},
				{1976, 115},
				{1977, 123},
				{1978, 118},
				{1979, 99},
				{1980, 93},
				{1981, 96},
				{1982, 54},
				{1983, 111},
				{1984, 85},
				{1985, 107},
				{1986, 89},
				{1987, 87},
				{1988, 97},
				{1989, 93},
				{1990, 88},
				{1991, 99},
				{1992, 108},
				{1993, 94},
				{1994, 74},
				{1995, 119},
				{1996, 102},
				{1997, 47},
				{1998, 82},
				{1999, 53},
				{2000, 115},
				{2001, 21},
				{2002, 89},
				{2003, 80},
				{2004, 101},
				{2005, 95},
				{2006, 66},
				{2007, 106},
				{2008, 97},
				{2009, 87},
				{2010, 109},
				{2011, 57},
				{2012, 87},
				{2013, 117},
				{2014, 91},
				{2015, 62},
				{2016, 65}};

		if(flag == 100) {
			for(int i = 0; i < 162; i++)
				System.out.println(dataset[i][0] + " " + dataset[i][1]);
		}
		
		if(flag == 200) {
			System.out.println("162");
			Double mean = 0.0;
			for(int i = 0; i < 162; i++) {
				mean += dataset[i][1];
			}
			mean = mean/162;
			System.out.printf("%.2f\n", mean);
			Double std = 0.0; 
			for(int i = 0; i < 162; i++) {
				std += Math.pow((dataset[i][1] - mean),2);
			}
			std = std/161;
			std = Math.sqrt(std);
			System.out.printf("%.2f\n", std);
		}
		
		if(flag == 300) {
			Double beta0 = Double.valueOf(args[1]);
			Double beta1 = Double.valueOf(args[2]);
			Double mse = 0.0;
			for(int i = 0; i < 162; i++) {
				mse += Math.pow((beta0 + beta1*dataset[i][0] - dataset[i][1]), 2);
			}
			mse = mse/162;
			System.out.printf("%.2f\n", mse);
		}
		
		if(flag == 400) {
			Double beta0 = Double.valueOf(args[1]);
			Double beta1 = Double.valueOf(args[2]);
			Double partialb0 = 0.0;
			Double partialb1 = 0.0;
			
			for(int i = 0; i < 162; i++) {
				partialb0 += (beta0 + beta1*dataset[i][0] - dataset[i][1]);
				partialb1 += ((beta0 + beta1*dataset[i][0] - dataset[i][1])*dataset[i][0]);
			}
			partialb0 = partialb0*(2.0/162.0);
			partialb1 = partialb1*(2.0/162.0);
			System.out.printf("%.2f\n", partialb0);
			System.out.printf("%.2f\n", partialb1);
		}
		
		if(flag == 500) {
			Double beta0t = 0.0;
			Double beta1t = 0.0;
			Double partialb0 = 0.0;
			Double partialb1 = 0.0;
			Double mu = Double.valueOf(args[1]);
			Double t = Double.valueOf(args[2]);
			Double mse = 0.0;
			for(int i = 1; i < t+1; i++) {
				for(int j = 0; j < 162; j++) {
					partialb0 += (beta0t + beta1t*dataset[j][0] - dataset[j][1]);
					partialb1 += ((beta0t + beta1t*dataset[j][0] - dataset[j][1])*dataset[j][0]);
				}
				partialb0 = partialb0*(2.0/162.0);
				partialb1 = partialb1*(2.0/162.0);
				beta0t = beta0t - mu*partialb0;
				beta1t = beta1t - mu*partialb1;
				for(int j = 0; j < 162; j++) {
					mse += Math.pow((beta0t + beta1t*dataset[j][0] - dataset[j][1]), 2);
				}
				mse = mse/162;
				System.out.printf("%d %.2f %.2f %.2f\n", i, beta0t, beta1t, mse);
				mse = 0.0;
				partialb0 = 0.0;
				partialb1 = 0.0;
			}
		}
		
		if(flag == 600) {
			Double xbar = 0.0;
			Double ybar = 0.0;
			Double beta1 = 0.0;
			Double beta0 = 0.0;
			
			for(int i = 0; i < 162; i++) {
				xbar += dataset[i][0];
				ybar += dataset[i][1];
			}
			
			xbar = xbar/162;
			ybar = ybar/162;
			Double num1 = 0.0;
			Double num2 = 0.0;
			
			for(int i = 0; i < 162; i++) {
				num1 += (dataset[i][0]-xbar)*(dataset[i][1]-ybar);
				num2 += Math.pow(dataset[i][0]-xbar,2);
			}
			beta1 = num1/num2;
			beta0 = ybar -beta1*xbar;
			Double mse = 0.0;
			for(int j = 0; j < 162; j++) {
				mse += Math.pow((beta0 + beta1*dataset[j][0] - dataset[j][1]), 2);
			}
			mse = mse/162;
			System.out.printf("%.2f %.2f %.2f\n", beta0, beta1, mse);
		}
		
		if(flag == 700) {
			Double xbar = 0.0;
			Double ybar = 0.0;
			Double beta1 = 0.0;
			Double beta0 = 0.0;
			Double year = Double.valueOf(args[1]);
			for(int i = 0; i < 162; i++) {
				xbar += dataset[i][0];
				ybar += dataset[i][1];
			}
			
			xbar = xbar/162;
			ybar = ybar/162;
			Double num1 = 0.0;
			Double num2 = 0.0;
			
			for(int i = 0; i < 162; i++) {
				num1 += (dataset[i][0]-xbar)*(dataset[i][1]-ybar);
				num2 += Math.pow(dataset[i][0]-xbar,2);
			}
			beta1 = num1/num2;
			beta0 = ybar -beta1*xbar;
			Double mse = 0.0;
			for(int j = 0; j < 162; j++) {
				mse += Math.pow((beta0 + beta1*dataset[j][0] - dataset[j][1]), 2);
			}
			mse = mse/162;
			System.out.printf("%.2f", beta0 + beta1*year);
		}
		
		if(flag == 800) {
			Double beta0t = 0.0;
			Double beta1t = 0.0;
			Double partialb0 = 0.0;
			Double partialb1 = 0.0;
			Double mu = Double.valueOf(args[1]);
			Double t = Double.valueOf(args[2]);
			Double mse = 0.0;
			Double xbar = 0.0;
			for(int i = 0; i < 162; i++) {
				xbar += dataset[i][0];
			}
			xbar = xbar/162;
			Double std = 0.0;
			
			for(int i = 0; i < 162; i++) {
				std += Math.pow(dataset[i][0]-xbar, 2);
			}
			std = std/161;
			std = Math.sqrt(std);
			for(int i = 1; i < t+1; i++) {
				for(int j = 0; j < 162; j++) {
					partialb0 += (beta0t + beta1t*((dataset[j][0]-xbar)/std) - dataset[j][1]);
					partialb1 += ((beta0t + beta1t*((dataset[j][0]-xbar)/std) - dataset[j][1])*((dataset[j][0]-xbar)/std));
				}
				partialb0 = partialb0*(2.0/162.0);
				partialb1 = partialb1*(2.0/162.0);
				beta0t = beta0t - mu*partialb0;
				beta1t = beta1t - mu*partialb1;
				for(int j = 0; j < 162; j++) {
					mse += Math.pow((beta0t + beta1t*((dataset[j][0]-xbar)/std) - dataset[j][1]), 2);
				}
				mse = mse/162;
				System.out.printf("%d %.2f %.2f %.2f\n", i, beta0t, beta1t, mse);
				mse = 0.0;
				partialb0 = 0.0;
				partialb1 = 0.0;
			}
		}
		
		if(flag == 900) {
			Double beta0t = 0.0;
			Double beta1t = 0.0;
			Double partialb0 = 0.0;
			Double partialb1 = 0.0;
			Double mu = Double.valueOf(args[1]);
			Double t = Double.valueOf(args[2]);
			Double mse = 0.0;
			Double xbar = 0.0;
			for(int i = 0; i < 162; i++) {
				xbar += dataset[i][0];
			}
			xbar = xbar/162;
			Double std = 0.0;
			
			for(int i = 0; i < 162; i++) {
				std += Math.pow(dataset[i][0]-xbar, 2);
			}
			std = std/161;
			std = Math.sqrt(std);
			int j = 0;
			for(int i = 1; i < t+1; i++) {
				j = (int)(Math.random()*161);
				partialb0 = 2*(beta0t + beta1t*((dataset[j][0]-xbar)/std) - dataset[j][1]);
				partialb1 = 2*((beta0t + beta1t*((dataset[j][0]-xbar)/std) - dataset[j][1])*((dataset[j][0]-xbar)/std));
				beta0t = beta0t - mu*partialb0;
				beta1t = beta1t - mu*partialb1;
				for(j = 0; j < 162; j++)
					mse += Math.pow((beta0t + beta1t*((dataset[j][0]-xbar)/std) - dataset[j][1]), 2);
				mse = mse/162;
				System.out.printf("%d %.2f %.2f %.2f\n", i, beta0t, beta1t, mse);
				mse = 0.0;
				partialb0 = 0.0;
				partialb1 = 0.0;
			}
		}
	}
}
