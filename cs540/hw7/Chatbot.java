
import java.util.*;
import java.io.*;
class triple{
	ArrayList<Double> nums = new ArrayList<Double>();
	public triple(Double i, Double l, Double r) {
		this.nums.add(i);
		this.nums.add(l);
		this.nums.add(r);
	}
	
	public Double getI() {
		return nums.get(0);
	}
	
	public Double getL() {
		return nums.get(1);
	}
	
	public Double getR() {
		return nums.get(2);
	}
}
public class Chatbot{
    private static String filename = "./WARC201709_wid.txt";
    private static ArrayList<Integer> readCorpus(){
        ArrayList<Integer> corpus = new ArrayList<Integer>();
        try{
            File f = new File(filename);
            Scanner sc = new Scanner(f);
            while(sc.hasNext()){
                if(sc.hasNextInt()){
                    int i = sc.nextInt();
                    corpus.add(i);
                }
                else{
                    sc.next();
                }
            }
        }
        catch(FileNotFoundException ex){
            System.out.println("File Not Found.");
        }
        return corpus;
    }
    static public void main(String[] args){
        ArrayList<Integer> corpus = readCorpus();
		int flag = Integer.valueOf(args[0]);
        
        if(flag == 100){
			int w = Integer.valueOf(args[1]);
            int count = 0;
            //TODO count occurence of w
            for(Integer i : corpus) {
            		if(i == w)
            			count ++;
            }
            
            System.out.println(count);
            System.out.println(String.format("%.7f",count/(double)corpus.size()));
        }
        else if(flag == 200){
        		ArrayList<triple> triples = new ArrayList<triple>();
        		ArrayList<Integer> count = new ArrayList<Integer>();
        		//ArrayList<Integer> lp = new ArrayList<Integer>();
        		//ArrayList<Integer> rp = new ArrayList<Integer>();
        		ArrayList<Double> p = new ArrayList<Double>();
        		triple item;
        		Double lInd = 0.00;
        		Double rInd = 0.00;
        		int sum;
        		int currCount = 0;
            int n1 = Integer.valueOf(args[1]);
            int n2 = Integer.valueOf(args[2]);
            //TODO generate
            double r = n1/n2;
            for(int i = 0; i < corpus.size(); i++) {
            		System.out.println("here");
            		currCount = 0;
            		for(Integer j : corpus) {
            			if(i == j)
            				currCount++;
            			count.add(i, currCount);
            		}
            }
//            for(int i = 0; i < count.size(); i++) {
//            		
//            }
            for(int i = 0 ; i < count.size(); i++) {
            		System.out.println("No here");
            		p.add(count.get(i)/(double)corpus.size());
            		if(i == 0)
            			lInd = 0.00;
            		else
            			lInd = p.subList(0, i-1).stream().mapToDouble(Double::doubleValue).sum();
            		rInd = p.subList(0, i).stream().mapToDouble(Double::doubleValue).sum();
            		item = new triple((double)i, lInd, rInd);
            		triples.add(item);
            }
            for(triple trip : triples) {
            		System.out.println("Got here : " + trip.getI() + trip.getL() + trip.getR());
            		if(r > trip.getL() && r < trip.getR()) {
            			System.out.println(trip.getI());
            			System.out.println(trip.getL());
            			System.out.println(trip.getR());
            			break;
            		}
            }
        }
        else if(flag == 300){
            int h = Integer.valueOf(args[1]);
            int w = Integer.valueOf(args[2]);
            int count = 0;
            ArrayList<Integer> words_after_h = new ArrayList<Integer>();
            //TODO

            //output 
            System.out.println(count);
            System.out.println(words_after_h.size());
            System.out.println(String.format("%.7f",count/(double)words_after_h.size()));
        }
        else if(flag == 400){
            int n1 = Integer.valueOf(args[1]);
            int n2 = Integer.valueOf(args[2]);
            int h = Integer.valueOf(args[3]);
            //TODO
            
        }
        else if(flag == 500){
            int h1 = Integer.valueOf(args[1]);
            int h2 = Integer.valueOf(args[2]);
            int w = Integer.valueOf(args[3]);
            int count = 0;
            ArrayList<Integer> words_after_h1h2 = new ArrayList<Integer>();
            //TODO

            //output 
            System.out.println(count);
            System.out.println(words_after_h1h2.size());
            if(words_after_h1h2.size() == 0)
                System.out.println("undefined");
            else
                System.out.println(String.format("%.7f",count/(double)words_after_h1h2.size()));
        }
        else if(flag == 600){
            int n1 = Integer.valueOf(args[1]);
            int n2 = Integer.valueOf(args[2]);
            int h1 = Integer.valueOf(args[3]);
            int h2 = Integer.valueOf(args[4]);
            //TODO
        }
        else if(flag == 700){
            int seed = Integer.valueOf(args[1]);
            int t = Integer.valueOf(args[2]);
            int h1=0,h2=0;

            Random rng = new Random();
            if (seed != -1) rng.setSeed(seed);

            if(t == 0){
                // TODO Generate first word using r
                double r = rng.nextDouble();
                System.out.println(h1);
                if(h1 == 9 || h1 == 10 || h1 == 12){
                    return;
                }

                // TODO Generate second word using r
                r = rng.nextDouble();
                System.out.println(h2);
            }
            else if(t == 1){
                h1 = Integer.valueOf(args[3]);
                // TODO Generate second word using r
                double r = rng.nextDouble();
                System.out.println(h2);
            }
            else if(t == 2){
                h1 = Integer.valueOf(args[3]);
                h2 = Integer.valueOf(args[4]);
            }

            while(h2 != 9 && h2 != 10 && h2 != 12){
                double r = rng.nextDouble();
                int w  = 0;
                // TODO Generate new word using h1,h2
                System.out.println(w);
                h1 = h2;
                h2 = w;
            }
        }

        return;
    }
}
