import java.util.*;

class State {
    char[] board;

    public State(char[] arr) {
        this.board = Arrays.copyOf(arr, arr.length);
    }

    public int getScore() {
    		int score1 = 0;
    		int score2 = 0;
	    	for(int i = 0; i < this.board.length; i++) {
	    		if(this.board[i] == '1') score1+=1;
	    		if(this.board[i] == '2') score2+=1;
	    	}
	    	if(score1 > score2) return 1;
	    	if(score1 < score2) return -1;
	    	return 0;
    }
    
    public boolean isTerminal() {
        return this.getSuccessors('1').length == 0 && this.getSuccessors('2').length == 0;
    }
    public char[][] tryUpRightDiag(char [][] board, char player, char opponent, int row, int col, int Orow, int Ocol) {
    		if(row-1>=0 && col-1>=0 && board[row-1][col-1] == opponent){ 
			row = row-1; col = col-1;
			
			while(row>0 && col>0 && board[row][col] == opponent) {
				row--;col--;
		}
			
			if(row>=0 && col>=0 && board[row][col] == player) {
				while(row!=Orow-1 && col!=Ocol-1)
					board[++row][++col]=player;
		}
		} 
    		return board;
    }
    
    public char[][] tryRight(char[][] board, char player, char opponent, int row, int col, int Orow) {
		if(row-1>=0 && board[row-1][col] == opponent){
			row = row-1;
			while(row>0 && board[row][col] == opponent) row--;
			if(row>=0 && board[row][col] == player) {while(row!=Orow-1)board[++row][col]=player;}
		} 
		return board;
    }
    
    public char[][] tryLowRightDiag(char[][] board, char player, char opponent, int row, int col, int Orow, int Ocol) {
		if(row-1>=0 && col+1<=3 && board[row-1][col+1] == opponent){
			row = row-1; col = col+1;
			while(row>0 && col<3 && board[row][col] == opponent){row--;col++;}
			if(row>=0 && col<=3 && board[row][col] == player) {while(row!=Orow-1 && col!=Ocol+1)board[++row][--col] = player;}
		}  
		return board;
    }
    
    public char[][] tryDown(char[][] board, char player, char opponent, int row, int col, int Ocol){
		if(col-1>=0 && board[row][col-1] == opponent){
			col = col-1;
			while(col>0 && board[row][col] == opponent)col--;
			if(col>=0 && board[row][col] == player) {while(col!=Ocol-1)board[row][++col] = player;}
		}
		
		return board;
    }
    
    public char[][] tryUp(char[][] board, char player, char opponent, int row, int col, int Ocol){
    		if(col+1<=3 && board[row][col+1] == opponent){
			col=col+1;
			while(col<3 && board[row][col] == opponent)col++;
			if(col<=3 && board[row][col] == player) {while(col!=Ocol+1)board[row][--col] = player;}
		}
		
		return board;
    }
    
    public char[][] tryLowLeftDiag(char[][] board, char player, char opponent, int row, int col, int Orow, int Ocol){
    		if(row+1<=3 && col-1>=0 && board[row+1][col-1] == opponent){ 
			row=row+1;col=col-1;
			while(row<3 && col>0 && board[row][col] == opponent){row++;col--;}
			if(row<=3 && col>=0 && board[row][col] == player) {while(row!=Orow+1 && col!=Ocol-1)board[--row][++col] = player;}
		}
	
    		return board;
    }
    public char[][] tryLeft(char[][] board, char player, char opponent, int row, int col, int Orow){
    		if(row+1 <= 3 && board[row+1][col] == opponent){ 
    			row=row+1;
    			while(row<3 && board[row][col] == opponent) row++;
    			if(row<=3 && board[row][col] == player) {while(row!=Orow+1)board[--row][col] = player;}
    		}
    		return board;
    }
    
    public char[][] tryUpLeftDiag(char[][] board, char player, char opponent, int row, int col, int Orow, int Ocol){
		if(row+1 <= 3 && col+1 <=3 && board[row+1][col+1] == opponent){
			row=row+1;col=col+1;
			while(row<3 && col<3 && board[row][col] == opponent){row++;col++;}
			if(row<=3 && col<=3 && board[row][col] == player)while(row!=Orow+1 && col!=Ocol+1)board[--row][--col] = player;}
		
		return board;
    }
    
    public State move(char player, int row, int col) {
    		State newState;
    		char[][] board = to2D(this.board.clone());
    		board[row][col] = player;
    		char opponent;
    		if(player =='1') opponent = '2';
    		else opponent = '1';

    		board = tryUpRightDiag(board, player, opponent, row, col, row, col);
    		board = tryRight(board, player, opponent, row, col, row);
    		board = tryLowRightDiag(board, player, opponent, row, col, row, col);
    		board = tryDown(board, player, opponent, row, col, col);
    		board = tryUp(board, player, opponent, row, col, col);
    		board = tryLowLeftDiag(board, player, opponent, row, col, row, col);
    		board = tryLeft(board, player, opponent, row, col, row);
    		board = tryUpLeftDiag(board, player, opponent, row, col, row, col);
   		
    		char[] spots = to1D(board);
    		newState = new State(spots);

    		return newState;
    }
    
    public char[] to1D(char[][] board) {
    		char[] result = new char[16];
    		int index = 0; 
    		for(int i = 0; i < 4; i++) {
    			for(int j = 0; j < 4; j++) {
    				result[index] = board[i][j];
    				index++;
    			}
    		}
    		return result;
    }
    
    public char[][] to2D(char[] board){
    		char[][] result = new char[4][4];
    		int curr = 0;
    		for(int i = 0; i < 4; i++) {
    			for(int j = 0; j < 4;j++) {
    				result[i][j] = this.board[curr];
    				curr++;
    			}
    		} 
    		return result;
    }
    
    public boolean exists(ArrayList<Integer> row, ArrayList<Integer> col, int rowVal, int colVal) {
    		boolean result = false;
    		for(int i = 0 ; i < row.size(); i++) {
    			if((row.get(i) == rowVal) && (col.get(i) == colVal)) result = true;			
    		}
    		return result;
    }
   
    public State[] getSuccessors(char player) {

        // TO DO: get all successors and return them in proper order
    		char[][] board = to2D(this.board);
    		char opponent;
    		if(player == '1') opponent = '2';
    		else opponent = '1';
    		
    		ArrayList<State> successors = new ArrayList<State>();
    		ArrayList<Integer> row = new ArrayList<Integer>();
    		ArrayList<Integer> col = new ArrayList<Integer>();

	    	for(int rownum=0;rownum<4;++rownum){
	            for(int colnum=0;colnum<4;++colnum){
	                if(board[rownum][colnum] == opponent){
	                    int OROW = rownum, OCOL = colnum; 
	                    
	                    //find upper left diag
	                    if(rownum-1>=0 && colnum-1>=0 && board[rownum-1][colnum-1] == '0'){ 
	                        rownum = rownum+1; 
	                        colnum = colnum+1;
	                        while(rownum<3 && colnum<3 && board[rownum][colnum] == opponent){
	                        		rownum++;colnum++;
	                        }
	                        if(rownum<=3 && colnum<=3 && board[rownum][colnum] == player) {
	                        		if(!exists(row,col,OROW-1,OCOL-1)) {
	                        			row.add(OROW-1);
	                        			col.add(OCOL-1);
	                        		}
	                        }
	                    } 
	                    rownum=OROW;colnum=OCOL;
	                    
	                    //find left
	                    if(rownum-1>=0 && board[rownum-1][colnum] == '0'){
	                        rownum = rownum+1;
	                        while(rownum<3 && board[rownum][colnum] == opponent) rownum++;
	                        if(rownum<=3 && board[rownum][colnum] == player) {
	                        		if(!exists(row,col,OROW-1,OCOL)) {
	                        			row.add(OROW-1);
	                        			col.add(OCOL);
	                        		}
	                        }
	                    } 
	                    rownum=OROW;
	                    
	                    //find lower left diag
	                    if(rownum-1>=0 && colnum+1<=3 && board[rownum-1][colnum+1] == '0'){
	                        rownum = rownum+1; 
	                        colnum = colnum-1;
	                        while(rownum<3 && colnum>0 && board[rownum][colnum] == opponent) {
	                        		rownum++;
	                        		colnum--;
	                        }
	                        if(rownum<=3 && colnum>=0 && board[rownum][colnum] == player) {
	                        		if(!exists(row,col,OROW-1,OCOL+1)) {
	                        			row.add(OROW-1);
	                        			col.add(OCOL+1);
	                        		}       
	                        }
	                    }  
	                    rownum=OROW;colnum=OCOL;
	                    
	                    //find up
	                    if(colnum-1>=0 && board[rownum][colnum-1] == '0'){
	                        colnum = colnum+1;
	                        while(colnum<3 && board[rownum][colnum] == opponent) colnum++;
	                        if(colnum<=3 && board[rownum][colnum] == player) {
	                        		if(!exists(row,col,OROW,OCOL-1)) {
	                        			row.add(OROW);
	                        			col.add(OCOL-1);
	                        		}
	                        }
	                    }
	                    colnum=OCOL;
	                    
	                    //find down
	                    if(colnum+1<=3 && board[rownum][colnum+1] == '0') {
	                        colnum=colnum-1;
	                        while(colnum>0 && board[rownum][colnum] == opponent) colnum--;
	                        if(colnum>=0 && board[rownum][colnum] == player) {
	                        		if(!exists(row,col,OROW,OCOL+1)) {
	                        			row.add(OROW);
	                        			col.add(OCOL+1);
	                        		}
	                        }
	                    }
	                    colnum=OCOL;
	                    
	                    //find upper right
	                    if(rownum+1<=3 && colnum-1>=0 && board[rownum+1][colnum-1] == '0') {
	                        rownum=rownum-1;
	                        colnum=colnum+1;
	                        while(rownum>0 && colnum<3 && board[rownum][colnum] == opponent) {
	                        		rownum--;
	                        		colnum++;
	                        }
	                        if(rownum>=0 && colnum<=3 && board[rownum][colnum] == player) {
	                        		if(!exists(row,col,OROW+1,OCOL-1)) {
	                        			row.add(OROW+1);
	                        			col.add(OCOL-1);
	                        		}
	                        }
	                    }
	                    rownum=OROW;colnum=OCOL;
	                    
	                    //find right
	                    if(rownum+1 <= 3 && board[rownum+1][colnum] == '0'){
	                        rownum=rownum-1;
	                        while(rownum>0 && board[rownum][colnum] == opponent) rownum--;
	                        if(rownum>=0 && board[rownum][colnum] == player) {
	                        		if(!exists(row,col,OROW+1,OCOL)) {
	                        			row.add(OROW+1);
	                        			col.add(OCOL);
	                        		}
	                        }
	                    }
	                    rownum=OROW;
	                    
	                    //find lower right diag
	                    if(rownum+1 <= 3 && colnum+1 <=3 && board[rownum+1][colnum+1] == '0'){
	                        rownum=rownum-1;colnum=colnum-1;
	                        while(rownum>0 && colnum>0 && board[rownum][colnum] == opponent) {
	                        		rownum--;
	                        		colnum--;
	                        }
	                        if(rownum>=0 && colnum>=0 && board[rownum][colnum] == player) {
	                        		if(!exists(row,col,OROW+1,OCOL+1)) {	
	                        			row.add(OROW+1);
	                        			col.add(OCOL+1);
	                        		}
	                        }
	                    }
	                    rownum=OROW;colnum=OCOL;
	                    }
	                } 
        } 
	    	
    		for(int i = 0; i < row.size(); i++) {
    			successors.add(move(player, row.get(i), col.get(i)));
    		}
  		
    		State[] succsArr = new State[successors.size()];
    		succsArr = successors.toArray(succsArr);
        return succsArr;
    }
 
    public void printState(int option, char player) {
    		State[] states = this.getSuccessors(player);
    		if(option != 1 && option != 2 && option != 3 && option != 4 && option != 5 && option != 6) {
    			System.out.println("Invalid Flag Value");
    			return;
    		}

    		if(option == 1 && states.length == 0 && !isTerminal()) System.out.println(this.board);
    		
    		if(option == 1 && states.length != 0)
    			for(State s: states)
    				System.out.println(s.getBoard());
    		
    		if(option == 2) {
    			if(isTerminal()) {
    				int count = 0;
    				for(int i = 0; i < this.board.length; i++) {
    					if(this.board[i] == player) count ++;
    				    else count --;
    				}
    				if(count > 0) System.out.println(1);
    				else if(count < 0) System.out.println(-1);
    				else if(count == 0) System.out.println(0);		
    			} else System.out.println("non-terminal");
    		}
    		
    		if(option == 3) {
    			int result = Minimax.run(this,player);
    			System.out.println(result);
    			System.out.println(Minimax.count);
    		}
    		
    		if(option == 4) {
    			if(this.getSuccessors(player).length == 0 && !isTerminal())
    				System.out.println(this.getBoard());
    			else if(!isTerminal())
    				System.out.println(this.getSuccessors(player)[0].getBoard());
    		}
    		
    		if(option == 5) {
    			int result = Minimax.run_with_pruning(this, player);
    			System.out.println(result);
    			System.out.println(Minimax.count);
    		}
    		
    		if(option == 6) {
    			if(this.getSuccessors(player).length == 0 && !isTerminal())
    				System.out.println(this.getBoard());
    			else if(!isTerminal())
    				System.out.println(this.getSuccessors(player)[0].getBoard());
    		}
    		
    }

    public String getBoard() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 16; i++) 
            builder.append(this.board[i]);
        return builder.toString().trim();
    }

    public boolean equals(State src) {
        for (int i = 0; i < 16; i++) if (this.board[i] != src.board[i]) return false;
        return true;
    }
}

class Minimax {
	public static int count = 0;
	public static int printMove = 0;
	private static int max_value(State curr_state) {
		count++;
		int alpha = -999999;
		
		if(curr_state.isTerminal()) return curr_state.getScore();
		
		if(curr_state.getSuccessors('1').length == 0 && !curr_state.isTerminal()) 
			alpha = Math.max(alpha,  min_value(curr_state));

		for(State s: curr_state.getSuccessors('1')) alpha = Math.max(alpha, min_value(s));
		
		return alpha;
	}
	
	private static int min_value(State curr_state) {
		count ++;
		int beta = 999999;		
		if(curr_state.isTerminal()) return curr_state.getScore();
		
		if(curr_state.getSuccessors('2').length == 0 && !curr_state.isTerminal()) 
			beta = Math.min(beta,  max_value(curr_state));

		for(State s: curr_state.getSuccessors('2')) beta = Math.min(beta, max_value(s));

		return beta;
	}
	
	private static int max_value_with_pruning(State curr_state, int alpha, int beta) {
		count++;
		int max = -999999;
		
		if(curr_state.isTerminal()) return curr_state.getScore();
		if(curr_state.getSuccessors('1').length == 0 && !curr_state.isTerminal()) 
			alpha = Math.max(alpha, min_value_with_pruning(curr_state, alpha, beta));
		for(State s: curr_state.getSuccessors('1')) {
			max = Math.max(max, min_value_with_pruning(s, alpha, beta));
			alpha = Math.max(alpha, max);
			if(beta <= alpha) return beta;
		}
		
		return alpha;
		
	}
	
	private static int min_value_with_pruning(State curr_state, int alpha, int beta) {
		count++;
		int min = 999999;
		if(curr_state.isTerminal()) return curr_state.getScore();
		if(curr_state.getSuccessors('2').length == 0 && !curr_state.isTerminal())
			beta = Math.min(beta, max_value_with_pruning(curr_state, alpha, beta)); 
		
		for(State s: curr_state.getSuccessors('2')) {
			min = Math.min(min, max_value_with_pruning(s, alpha, beta));
			beta = Math.min(beta, min);
			if(beta <= alpha) return alpha;
		}
		
		return beta;
	}
	
	public static int run(State curr_state, char player) {
		int result = 0;
		if(player == '1') result = max_value(curr_state);
		else result = min_value(curr_state);
		return result;
	}
	
	public static int run_with_pruning(State curr_state, char player) {
		int result = 0;
		if(player == '1') result = max_value_with_pruning(curr_state, -999999, 999999);
		else result = min_value_with_pruning(curr_state, -999999, 999999);
		return result;
	}
}

public class Reversi {
    public static void main(String args[]) {
        if (args.length != 3) {
            System.out.println("Invalid Number of Input Arguments");
            return;
        }
        if(args[2].length() != 16) {
        		System.out.println("Invalid board length");
        		return;
        }
        int flag = Integer.valueOf(args[0]);
        char[] board = new char[16];
        for (int i = 0; i < 16; i++) {
            board[i] = args[2].charAt(i);
        }
        int option = flag / 100;
        char player = args[1].charAt(0);
        if ((player != '1' && player != '2') || args[1].length() != 1) {
            System.out.println("Invalid Player Input");
            return;
        }
        State init = new State(board);
        init.printState(option, player);
    }
}
