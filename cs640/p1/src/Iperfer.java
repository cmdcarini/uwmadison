import java.net.*;
import java.io.*;

public class Iperfer {
    private static void exit(int err) {
        if(err == 1) {
            System.out.println("Error: missing or additional arguments");
            System.exit(0);
        } else if(err == 2) {
            System.out.println("Error: port number must be in the range 1024 to 65535");
            System.exit(0);
        }
    }

    public static void main(String[] args) {
        String serverName;
        int serverPort, totalTime, listenPort, count = 0, gotMessage, total = 0;
        byte [] sentMessage = new byte[1000], receiveMessage = new byte[1000];
        long startTime, timeInSec, finalTime, startTimeServer = 0;
        boolean waiting = false, done = true;

        if (args.length == 0)
            exit(1);

        if (args[0].equals("-c")) {
            if (args.length != 7)
                exit(1);

            serverPort = Integer.parseInt(args[4]);
            if (serverPort < 1024 || serverPort > 65535)
                exit(2);

            serverName = args[2];
            totalTime = Integer.parseInt(args[6]);

            try (Socket socket = new Socket(serverName, serverPort);
                 DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                 ) {
                startTime = System.currentTimeMillis();
                timeInSec = (System.currentTimeMillis()-startTime)/1000;

                while(timeInSec < totalTime) {
                    count++;
                    out.write(sentMessage, 0, 1000);
                    timeInSec = (System.currentTimeMillis()-startTime)/1000;
                }
                out.close();
                socket.close();
                System.out.println("sent=" + count + " KB " + "rate=" +
                        String.format("%.3f",(count*0.008) / timeInSec) + " Mbps");
            } catch (IOException e) {
                System.out.println("Exception caught when trying to listen on port "
                        + serverPort + " or listening for a connection");
                System.out.println(e.getMessage());
            }
        } else if (args[0].equals("-s")) {
            if (args.length != 3)
                exit(1);

            listenPort = Integer.parseInt(args[2]);
            if (listenPort < 1024 || listenPort > 65535)
                exit(2);

            while(true) {
                try (ServerSocket serverSocket = new ServerSocket(listenPort);
                     Socket input = serverSocket.accept();
                     DataInputStream in = new DataInputStream(input.getInputStream());
                ) {
                    startTimeServer = System.currentTimeMillis();
                    done = true;

                    while (done) {
                        gotMessage = in.read(receiveMessage, 0, 1000);
                        if (gotMessage > 0) {
                            total += gotMessage;
                            waiting = true;
                        }

                        if (gotMessage < 0 && waiting) {
                            finalTime = (System.currentTimeMillis() - startTimeServer) / 1000;
                            System.out.println("received=" + total/1000 + " KB " + "rate="
                                    + String.format("%.3f", (total/1000 * 0.008) / finalTime) + " Mbps");
                            done = false;
                            total = 0;
                            waiting = false;
                            in.close();
                            input.close();
                            serverSocket.close();
                        }
                    }
                } catch (IOException e) {
                    System.out.println("Exception caught when trying to listen on port "
                            + listenPort + " or listening for a connection");
                    System.out.println(e.getMessage());
                }
            }
        } else
            exit(1);
    }
}
