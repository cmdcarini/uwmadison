import javax.xml.crypto.Data;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Sender {
    private static int port, remote_port, mtu, sws_original, sws, numPackets, sequenceCounter = 1, totalBytes = 0,
            numPackWithBadChksm = 0 , numPackRetransmit = 0, numDiscard = 0, numCksm = 0, numPackDup = 0, numDupAck = 0;
    private double timeout = 5000000000.0;
    private String remote_ip, file_name;

    public Sender(int port, String remote_ip, int remote_port, String file_name, int mtu, int sws) {
        this.port = port;
        if (port < 1024 || port > 65535)
            TCPend.exit(2);
        this.remote_ip = remote_ip;
        this.remote_port = remote_port;
        this.file_name = file_name;
        this.mtu = mtu;
        this.sws = sws;
        this.sws_original = sws;
    }

    /*
     * listen listens for activity (acks, syn/acks, syn, fin, etc.)
     */
    public void main() {
        //TODO control flow
        // 1. init connection
        DatagramSocket socket;
        try {
            //2. sends file name
            //Done in initializeConnection
            //3. sends data from file
            socket = initializeConnection();
            ArrayList<TCPPacket> packetsToSend = generatePackets();
            ArrayList<TCPPacket> packetsSent = new ArrayList<>();

            int curr = 0;
            int seqCount = 0;
            while(curr != packetsToSend.size()) {
                for (int i = curr; i < packetsToSend.size(); i++) {
                    //System.out.println("sws = " + sws + " & length = " + packetsToSend.get(i).getLength());
                    if(sws > packetsToSend.get(i).getLength()) {
                        sendPacket(packetsToSend.get(i), socket);
                        sws -= packetsToSend.get(i).getLength();
                        seqCount = packetsToSend.get(i).getByteSequenceNum()+ packetsToSend.get(i).getLength();
                        curr = i+1;
                        packetsSent.add(packetsToSend.get(i));
//                        listen(socket, packetsToSend, packetsToSend.get(i).getByteSequenceNum() + packetsToSend.get(i).getLength());
//                        sws += packetsToSend.get(i).getLength();
                    } else {
                        break;
                    }
                }
                listen(socket, packetsSent, seqCount);
                sws = sws_original;
            }
            closeConnection(socket);

            System.out.println("Amount of Data Transfered: " + totalBytes + " bytes");
            System.out.println("No of Packets Sent: " + (numPackets + 4));
            System.out.println("No of Packets Discarded (out of sequence): 0");
            System.out.println("No of Packets discarded (wrong checksum): " + numPackWithBadChksm);
            System.out.println("No of Retransmissions: " + numPackRetransmit);
            System.out.println("No of Duplicate Acknowledgements: " + numDupAck);

            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        //4. handles and receives acks
    }

    /*
     * listens for activity (acks, syn/acks, syn, fin, etc.)
     */
    public void listen(DatagramSocket socket, ArrayList<TCPPacket> packetsToSend, int seqCounter) throws IOException, ClassNotFoundException {
        byte buf[];
        String a,s,d,f;
        boolean done = false;
        while(!done) {
            if (mtu < sws_original)
                buf = new byte[mtu + 185];
            else
                buf = new byte[sws_original + 185];
            DatagramPacket receivedPayload;
            if (mtu < sws_original)
                receivedPayload = new DatagramPacket(buf, mtu + 185);
            else
                receivedPayload = new DatagramPacket(buf, sws_original + 185);

//            long timeoutInMS = TimeUnit.NANOSECONDS.toMillis((long)timeout);
            socket.setSoTimeout(0);
            socket.receive(receivedPayload);

            if (receivedPayload != null) {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(receivedPayload.getData());
                ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
                TCPPacket receivedAck = (TCPPacket) objectInputStream.readObject();
                objectInputStream.close();
                byteArrayInputStream.close();

                //check to see if checksum is valid
                if (!Arrays.equals(receivedAck.CalcCheckSum(receivedAck.getByteSequenceNum()), receivedAck.getChecksum())) {
                    numPackWithBadChksm++;
                    //continue;
                }
                a = (receivedAck.getAck() == 1) ? "A" : "-";
                s = (receivedAck.getSyn() == 1) ? "S" : "-";
                f = (receivedAck.getFin() == 1) ? "F" : "-";
                d = (receivedAck.getFin() == 0 && receivedAck.getSyn() == 0 && receivedAck.getAck() == 0) ? "D" : "-";
                double output = (double) receivedAck.getTimeStamp() / 1000000000000.0;

                System.out.println("rcv " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                        receivedAck.getByteSequenceNum() + " " + receivedAck.getLength() + " " + receivedAck.getAckNum());
                calculateTimeout(receivedAck.getAckNum(), receivedAck.getTimeStamp());
                //System.out.println("Need : " + receivedAck.getAckNum() + " Looking for: " + seqCounter);
                if(packetsToSend.size() == 0) {
                    done = true;
                    break;
                }
                boolean found = false;
                for(int i = 0; i < packetsToSend.size(); i++) {
                    if(packetsToSend.get(i).getByteSequenceNum() < receivedAck.getAckNum()) {
                        packetsToSend.remove(packetsToSend.get(i));
                        //i--;
                        found = true;
                    }
                }
                for(TCPPacket p: packetsToSend) {
                    System.out.println(p.getByteSequenceNum());
                }
                if(found == false && packetsToSend.size() != 0) {
                    retransmit(packetsToSend.get(0).getByteSequenceNum(), (ArrayList<TCPPacket>)packetsToSend.clone(), socket);
                }
            }
        }
    }

    /*
     * handles retransmissions
     */
    public void retransmit(int sequenceToResend, ArrayList<TCPPacket> packetsToSend, DatagramSocket socket) throws IOException {
        for(TCPPacket p: packetsToSend) {
            if(p.getByteSequenceNum() == sequenceToResend) {
                //System.out.println("Retransmitting : " + p.getByteSequenceNum() + "Asking for " + sequenceToResend);
                sendPacket(p, socket);
                totalBytes -= p.getLength();
                numPackRetransmit++;
            }
        }
    }

    /*
     * converts TCPPacket to DatagramPacket and transfers to sendPacket for Datagram
     *
     */

    public void sendPacket(TCPPacket packetToSend, DatagramSocket socket) throws IOException {
        String a,s,d,f;
        ByteArrayOutputStream outputStream;
        ObjectOutputStream outObj;
        DatagramPacket datagramPacket;
        outputStream = (mtu < sws_original) ? new ByteArrayOutputStream(mtu) : new ByteArrayOutputStream(sws_original);
        outObj = new ObjectOutputStream(outputStream);
        outObj.writeObject(packetToSend);
        outObj.flush();
        outputStream.flush();
        byte[] initPayload = outputStream.toByteArray();
        datagramPacket = new DatagramPacket(initPayload, initPayload.length, InetAddress.getByName(remote_ip),
                remote_port);
        sendPacket(datagramPacket, socket);
        a = (packetToSend.getAck() == 1) ? "A" : "-";
        s = (packetToSend.getSyn() == 1) ? "S" : "-";
        f = (packetToSend.getFin() == 1) ? "F" : "-";
        d = (packetToSend.getFin() == 0 && packetToSend.getSyn() == 0 && packetToSend.getAck() == 0) ? "D" : "-";
        totalBytes += packetToSend.getLength();
        double output = (double)packetToSend.getTimeStamp()/1000000000000.0;
        System.out.println("snd " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                packetToSend.getByteSequenceNum() + " " + packetToSend.getLength() + " " + packetToSend.getAckNum());

    }

    /*
     * sends packet
     *     public TCPPacket(int byteSequenceNum, int ackNum, long timeStamp, int length,
                     byte syn, byte ack, byte fin, byte[] checksum) {
     */

    public void sendPacket(DatagramPacket packetToSend, DatagramSocket socket) throws IOException {
        socket.send(packetToSend);
    }

    /*
     * calculates timeout
     */
    public static double ERTT = 0, EDEV = 0, SRTT = 0, SDEV = 0;
    public void calculateTimeout(int seqNum, double timeSent) {
        double timeReceived = System.nanoTime();
        if(seqNum == 0) {
            ERTT = ((timeReceived) - (timeSent));
            EDEV = 0;
            timeout = 2*ERTT;
        } else  {
            SRTT = ((timeReceived) - (timeSent));
            SDEV = Math.abs(SRTT - ERTT);
            ERTT = (long)(0.875*ERTT + (1-0.875)*SRTT);
            EDEV = (long)(0.75*EDEV + (1-0.75)*SDEV);
            timeout = ERTT + 4*EDEV;
        }
        //return timeout;
    }

    /*
     * generates packets and calls sendPacket
     */
    public ArrayList<TCPPacket> generatePackets() throws IOException {
        ArrayList<TCPPacket> packetsToSend = new ArrayList<>();
        byte[] temp, tempC = {0};
        int curr;
        //sequenceCounter = file_name.length()+1;
        TCPPacket packetToAdd;
        ArrayList<TCPPacket> sendBuff = new ArrayList<>();
        byte syn, ackB, fin;
        File file = new File(file_name);
        byte[] payload = new byte[(int)file.length()];
        FileInputStream fileInputStream = new FileInputStream(file);
        fileInputStream.read(payload);
        fileInputStream.close();
        if(mtu < sws) {
            numPackets = (int) Math.ceil((payload.length) / (double) (mtu));
        } else {
            numPackets = (int) Math.ceil((payload.length) / (double) (sws));
        }
        for(int i = 0; i < numPackets; i++) {
            if (mtu < sws) {
                temp = new byte[mtu];
            } else {
                temp = new byte[sws];
            }
            //temp = new byte[mtu];
            curr = 0;
            if (mtu < sws) {
                while ((curr + (i * mtu) < (payload.length)) && curr < mtu) {
                    temp[curr] = payload[curr + (i * mtu)];
                    curr += 1;
                }
            } else {
                while ((curr + (i * sws) < (payload.length)) && curr < sws) {
                    temp[curr] = payload[curr + (i * sws)];
                    curr += 1;
                }
            }

            syn = 0;
            ackB = 0;
            fin = 0;
            packetToAdd = new TCPPacket(sequenceCounter, 0, System.nanoTime(),
                    curr , syn, ackB, fin, tempC);
            packetToAdd.setPayload(temp);
            packetToAdd.setChecksum(packetToAdd.getByteSequenceNum());

            packetsToSend.add(packetToAdd);
            sequenceCounter = packetToAdd.getByteSequenceNum() + packetToAdd.getLength();
        }

        return packetsToSend;

    }

    /*
     * initializes connection
     */
    public DatagramSocket initializeConnection() throws IOException, ClassNotFoundException {
        DatagramSocket socket = new DatagramSocket();
        ByteArrayOutputStream outputStream;
        String a,s,d,f;
        if(mtu < sws_original)
            outputStream = new ByteArrayOutputStream(mtu);
        else
            outputStream = new ByteArrayOutputStream(sws_original);
        ObjectOutputStream outObj;
        DatagramPacket datagramPacket;
        byte syn = 1, ack = 0, fin = 0;
        byte[] checksum = {0};

        TCPPacket packetToSend = new TCPPacket(0, 0, System.nanoTime(), 0, syn, ack, fin, checksum);

        //sends file name in first packet!!!!!!!!
        packetToSend.setPayload(file_name.getBytes());
        packetToSend.setChecksum(packetToSend.getByteSequenceNum());
        outObj = new ObjectOutputStream(outputStream);
        outObj.writeObject(packetToSend);
        outObj.flush();
        outputStream.flush();
        byte[] initPayload = outputStream.toByteArray();
        datagramPacket = new DatagramPacket(initPayload, initPayload.length, InetAddress.getByName(remote_ip), remote_port);
        sendPacket(datagramPacket, socket);
        a = (packetToSend.getAck() == 1) ? "A" : "-";
        s = (packetToSend.getSyn() == 1) ? "S" : "-";
        f = (packetToSend.getFin() == 1) ? "F" : "-";
        d = (packetToSend.getFin() == 0 && packetToSend.getSyn() == 0 && packetToSend.getAck() == 0) ? "D" : "-";
        totalBytes += packetToSend.getLength();
        double output = (double)packetToSend.getTimeStamp()/1000000000000.0;
        System.out.println("snd " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                packetToSend.getByteSequenceNum() + " " + packetToSend.getLength() + " " + packetToSend.getAckNum());
        byte buf[];
        if(mtu < sws_original)
            buf = new byte[mtu + 185];
        else
            buf = new byte[sws_original + 185];
        DatagramPacket receivedPayload;
        if(mtu < sws_original)
            receivedPayload = new DatagramPacket(buf, mtu + 185);
        else
            receivedPayload = new DatagramPacket(buf, sws_original + 185);
        socket.setSoTimeout(2000);
        while(true) {
            try {
                socket.receive(receivedPayload);
                break;
            } catch (SocketTimeoutException e) {
                sendPacket(datagramPacket, socket);
                a = (packetToSend.getAck() == 1) ? "A" : "-";
                s = (packetToSend.getSyn() == 1) ? "S" : "-";
                f = (packetToSend.getFin() == 1) ? "F" : "-";
                d = (packetToSend.getFin() == 0 && packetToSend.getSyn() == 0 && packetToSend.getAck() == 0) ? "D" : "-";
                totalBytes += packetToSend.getLength();
                output = (double)packetToSend.getTimeStamp()/1000000000000.0;
                System.out.println("snd " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                        packetToSend.getByteSequenceNum() + " " + packetToSend.getLength() + " " + packetToSend.getAckNum());
            }
        }
        socket.setSoTimeout(0);

        if(receivedPayload != null) {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(receivedPayload.getData());
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            TCPPacket initAck = (TCPPacket)objectInputStream.readObject();
            objectInputStream.close();
            inputStream.close();
            a = (initAck.getAck() == 1) ? "A" : "-";
            s = (initAck.getSyn() == 1) ? "S" : "-";
            f = (initAck.getFin() == 1) ? "F" : "-";
            d = (initAck.getFin() == 0 && initAck.getSyn() == 0 && initAck.getAck() == 0) ? "D" : "-";
            output = (double) initAck.getTimeStamp() / 1000000000000.0;

            System.out.println("rcv " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                    initAck.getByteSequenceNum() + " " + initAck.getLength() + " " + initAck.getAckNum());

            ack = 1;
            syn = 0;
            packetToSend = new TCPPacket(0, 1, System.nanoTime(), 0, syn, ack, fin, checksum);
            packetToSend.setChecksum(packetToSend.getByteSequenceNum());
            if(mtu < sws_original)
                outputStream = new ByteArrayOutputStream(mtu);
            else
                outputStream = new ByteArrayOutputStream(sws_original);
            outObj = new ObjectOutputStream(outputStream);
            outObj.writeObject(packetToSend);
            outObj.flush();
            outputStream.flush();
            initPayload = outputStream.toByteArray();
            datagramPacket = new DatagramPacket(initPayload, initPayload.length, InetAddress.getByName(remote_ip), remote_port);
            sendPacket(datagramPacket, socket);
            a = (packetToSend.getAck() == 1) ? "A" : "-";
            s = (packetToSend.getSyn() == 1) ? "S" : "-";
            f = (packetToSend.getFin() == 1) ? "F" : "-";
            d = (packetToSend.getFin() == 0 && packetToSend.getSyn() == 0 && packetToSend.getAck() == 0) ? "D" : "-";
            totalBytes += packetToSend.getLength();
            output = (double)packetToSend.getTimeStamp()/1000000000000.0;
            System.out.println("snd 1" + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                    packetToSend.getByteSequenceNum() + " " + packetToSend.getLength() + " " + packetToSend.getAckNum());
        }

        return socket;
    }

    /*
     * closes connection
     */
    public DatagramSocket closeConnection(DatagramSocket socket) throws IOException, ClassNotFoundException {
        //DatagramSocket socket = new DatagramSocket();
        ByteArrayOutputStream outputStream;
        String a,s,d,f;
        if(mtu < sws_original)
            outputStream = new ByteArrayOutputStream(mtu);
        else
            outputStream = new ByteArrayOutputStream(sws_original);
        ObjectOutputStream outObj;
        DatagramPacket datagramPacket;
        byte syn = 0, ack = 0, fin = 1;
        byte[] checksum = {0};

        TCPPacket packetToSend = new TCPPacket(sequenceCounter+1, 1, System.nanoTime(), 0, syn, ack, fin, checksum);

        //sends file name in first packet!!!!!!!!
        packetToSend.setPayload(file_name.getBytes());
        packetToSend.setChecksum(packetToSend.getByteSequenceNum());
        outObj = new ObjectOutputStream(outputStream);
        outObj.writeObject(packetToSend);
        outObj.flush();
        outputStream.flush();
        byte[] initPayload = outputStream.toByteArray();
        datagramPacket = new DatagramPacket(initPayload, initPayload.length, InetAddress.getByName(remote_ip), remote_port);
        sendPacket(datagramPacket, socket);
        a = (packetToSend.getAck() == 1) ? "A" : "-";
        s = (packetToSend.getSyn() == 1) ? "S" : "-";
        f = (packetToSend.getFin() == 1) ? "F" : "-";
        d = (packetToSend.getFin() == 0 && packetToSend.getSyn() == 0 && packetToSend.getAck() == 0) ? "D" : "-";
        totalBytes += packetToSend.getLength();
        double output = (double)packetToSend.getTimeStamp()/1000000000000.0;
        System.out.println("snd " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                packetToSend.getByteSequenceNum() + " " + packetToSend.getLength() + " " + packetToSend.getAckNum());
        byte buf[];
        if(mtu < sws_original)
            buf = new byte[mtu + 185];
        else
            buf = new byte[sws_original + 185];
        DatagramPacket receivedPayload;
        if(mtu < sws_original)
            receivedPayload = new DatagramPacket(buf, mtu + 185);
        else
            receivedPayload = new DatagramPacket(buf, sws_original + 185);
        socket.setSoTimeout(2000);
        while(true) {
            try {
                socket.receive(receivedPayload);
                break;
            } catch (SocketTimeoutException e) {
                sendPacket(datagramPacket, socket);
                a = (packetToSend.getAck() == 1) ? "A" : "-";
                s = (packetToSend.getSyn() == 1) ? "S" : "-";
                f = (packetToSend.getFin() == 1) ? "F" : "-";
                d = (packetToSend.getFin() == 0 && packetToSend.getSyn() == 0 && packetToSend.getAck() == 0) ? "D" : "-";
                totalBytes += packetToSend.getLength();
                output = (double)packetToSend.getTimeStamp()/1000000000000.0;
                System.out.println("snd " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                        packetToSend.getByteSequenceNum() + " " + packetToSend.getLength() + " " + packetToSend.getAckNum());
            }
        }
        if(receivedPayload != null) {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(receivedPayload.getData());
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            TCPPacket initAck = (TCPPacket)objectInputStream.readObject();
            objectInputStream.close();
            inputStream.close();
            a = (initAck.getAck() == 1) ? "A" : "-";
            s = (initAck.getSyn() == 1) ? "S" : "-";
            f = (initAck.getFin() == 1) ? "F" : "-";
            d = (initAck.getFin() == 0 && initAck.getSyn() == 0 && initAck.getAck() == 0) ? "D" : "-";
            output = (double) initAck.getTimeStamp() / 1000000000000.0;

            System.out.println("rcv " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                    initAck.getByteSequenceNum() + " " + initAck.getLength() + " " + initAck.getAckNum());

            ack = 1;
            fin = 0;
            packetToSend = new TCPPacket(sequenceCounter +1, 2, System.nanoTime(), 0, syn, ack, fin, checksum);
            packetToSend.setChecksum(packetToSend.getByteSequenceNum());
            if(mtu < sws_original)
                outputStream = new ByteArrayOutputStream(mtu);
            else
                outputStream = new ByteArrayOutputStream(sws_original);
            outObj = new ObjectOutputStream(outputStream);
            outObj.writeObject(packetToSend);
            outObj.flush();
            outputStream.flush();
            initPayload = outputStream.toByteArray();
            datagramPacket = new DatagramPacket(initPayload, initPayload.length, InetAddress.getByName(remote_ip), remote_port);
            sendPacket(datagramPacket, socket);
            a = (packetToSend.getAck() == 1) ? "A" : "-";
            s = (packetToSend.getSyn() == 1) ? "S" : "-";
            f = (packetToSend.getFin() == 1) ? "F" : "-";
            d = (packetToSend.getFin() == 0 && packetToSend.getSyn() == 0 && packetToSend.getAck() == 0) ? "D" : "-";
            totalBytes += packetToSend.getLength();
            output = (double)packetToSend.getTimeStamp()/1000000000000.0;
            System.out.println("snd 1" + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                    packetToSend.getByteSequenceNum() + " " + packetToSend.getLength() + " " + packetToSend.getAckNum());
        }

        return socket;
    }


}