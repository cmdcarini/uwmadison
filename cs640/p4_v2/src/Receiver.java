import javax.xml.crypto.Data;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;

public class Receiver {

    private static int port, mtu, sws, totalBytes, numPackets, numPackRetransmit, numDiscard, numCksm, numPackDup;
    private static String filename;
    private static File file;
    private static FileOutputStream fileOutputStream;

    public Receiver(int port, int mtu, int sws) {
        this.port = port;
        this.mtu = mtu;
        this.sws = sws;
    }
    public void main() {
        DatagramSocket socket;
        //1. initialize
        try {
            socket = initializeConnection();
            filename = "tmp_" + filename;
            file = new File(filename);
            fileOutputStream = new FileOutputStream(file);
            listen(socket);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        //2. wait for all data
        //3. send ack for last correct packet received
        //4. wait for close


    }

    public void listen(DatagramSocket socket) throws IOException, ClassNotFoundException {
        byte[] buf, tempC = {0};
        byte syn = 0, ack = 1, fin = 0;
        String a,s,d,f;
        TCPPacket ackPacket;
        int expectedValue = 1;
        InetAddress inetAddress = null;
        int remote_port = -1;
        //String filename;
        while(true) {
            if (mtu < sws)
                buf = new byte[mtu + 185];
            else
                buf = new byte[sws + 185];
            DatagramPacket receivedPayload;
            if (mtu < sws)
                receivedPayload = new DatagramPacket(buf, mtu + 185);
            else
                receivedPayload = new DatagramPacket(buf, sws + 185);
            socket.setSoTimeout(1000);
            try {
                socket.receive(receivedPayload);
                numPackets++;
                if (receivedPayload != null) {

                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(receivedPayload.getData());
                    ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
                    TCPPacket receivedPacket = (TCPPacket) objectInputStream.readObject();
                    objectInputStream.close();
                    byteArrayInputStream.close();
                    remote_port = receivedPayload.getPort();
                    inetAddress = receivedPayload.getAddress();
                    a = (receivedPacket.getAck() == 1) ? "A" : "-";
                    s = (receivedPacket.getSyn() == 1) ? "S" : "-";
                    f = (receivedPacket.getFin() == 1) ? "F" : "-";
                    d = (receivedPacket.getFin() == 0 && receivedPacket.getSyn() == 0 && receivedPacket.getAck() == 0) ? "D" : "-";
                    double output = (double) receivedPacket.getTimeStamp() / 1000000000000.0;

                    System.out.println("rcv " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                            receivedPacket.getByteSequenceNum() + " " + receivedPacket.getLength() + " " + receivedPacket.getAckNum());
                    totalBytes += receivedPacket.getLength();
                    if (receivedPacket.getFin() == 1) {
                        closeConnection(socket, receivedPacket, receivedPayload);
                        break;
                    }

                    if (receivedPacket.getByteSequenceNum() == expectedValue) {
                        ackPacket = new TCPPacket(0, expectedValue + receivedPacket.getLength(), receivedPacket.getTimeStamp(),
                                0, syn, ack, fin, tempC);
                        expectedValue += receivedPacket.getLength();
                        ackPacket.setChecksum(ackPacket.getByteSequenceNum());
                        fileOutputStream.write(receivedPacket.getPayload(), 0, receivedPacket.getLength());

                    } else {
                        ackPacket = new TCPPacket(0, expectedValue, receivedPacket.getTimeStamp(),
                                0, syn, ack, fin, tempC);
                        ackPacket.setChecksum(ackPacket.getByteSequenceNum());
                    }
                    ByteArrayOutputStream outputStream;
                    if (mtu < sws)
                        outputStream = new ByteArrayOutputStream(mtu);
                    else
                        outputStream = new ByteArrayOutputStream(sws);

                    ObjectOutputStream outObj = new ObjectOutputStream(outputStream);
                    outObj.writeObject(ackPacket);
                    byte TCPbuf[] = outputStream.toByteArray();
                    DatagramPacket synAck = new DatagramPacket(TCPbuf, TCPbuf.length,
                            receivedPayload.getAddress(), receivedPayload.getPort());
                    sendPacket(synAck, socket);
                    a = (ackPacket.getAck() == 1) ? "A" : "-";
                    s = (ackPacket.getSyn() == 1) ? "S" : "-";
                    f = (ackPacket.getFin() == 1) ? "F" : "-";
                    d = (ackPacket.getFin() == 0 && ackPacket.getSyn() == 0 && ackPacket.getAck() == 0) ? "D" : "-";
                    output = (double) ackPacket.getTimeStamp() / 1000000000000.0;

                    System.out.println("snd " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                            ackPacket.getByteSequenceNum() + " " + ackPacket.getLength() + " " + ackPacket.getAckNum());
                }
            } catch(SocketTimeoutException e) {
                ackPacket = new TCPPacket(0, expectedValue, System.nanoTime(),
                        0, syn, ack, fin, tempC);
                ackPacket.setChecksum(ackPacket.getByteSequenceNum());
                //numPackDup++;
                numPackRetransmit++;
                ByteArrayOutputStream outputStream;
                if (mtu < sws)
                    outputStream = new ByteArrayOutputStream(mtu);
                else
                    outputStream = new ByteArrayOutputStream(sws);

                ObjectOutputStream outObj = new ObjectOutputStream(outputStream);
                outObj.writeObject(ackPacket);
                byte TCPbuf[] = outputStream.toByteArray();
                DatagramPacket synAck = new DatagramPacket(TCPbuf, TCPbuf.length,
                        inetAddress, remote_port);
                sendPacket(synAck, socket);
                a = (ackPacket.getAck() == 1) ? "A" : "-";
                s = (ackPacket.getSyn() == 1) ? "S" : "-";
                f = (ackPacket.getFin() == 1) ? "F" : "-";
                d = (ackPacket.getFin() == 0 && ackPacket.getSyn() == 0 && ackPacket.getAck() == 0) ? "D" : "-";
                double output = (double) ackPacket.getTimeStamp() / 1000000000000.0;

                System.out.println("snd " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                        ackPacket.getByteSequenceNum() + " " + ackPacket.getLength() + " " + ackPacket.getAckNum());
            }
        }
    }

    public void sendPacket(DatagramPacket datagramPacket, DatagramSocket socket) throws IOException {
        socket.send(datagramPacket);
    }

    public DatagramSocket initializeConnection() throws IOException, ClassNotFoundException {
        byte[] buf, tempC = {0};
        byte syn = 1, ack = 1, fin = 0;
        String a,s,d,f;
        TCPPacket synAckPacket;
        //String filename;
        DatagramSocket socket = new DatagramSocket(port);
        if(mtu < sws)
            buf = new byte[mtu + 185];
        else
            buf = new byte[sws + 185];
        DatagramPacket receivedPayload;
        if(mtu < sws)
            receivedPayload = new DatagramPacket(buf, mtu + 185);
        else
            receivedPayload = new DatagramPacket(buf, sws + 185);

        socket.receive(receivedPayload);
        numPackets++;

        if (receivedPayload != null) {

            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(receivedPayload.getData());
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            TCPPacket receivedSyn = (TCPPacket) objectInputStream.readObject();
            objectInputStream.close();
            byteArrayInputStream.close();

            a = (receivedSyn.getAck() == 1) ? "A" : "-";
            s = (receivedSyn.getSyn() == 1) ? "S" : "-";
            f = (receivedSyn.getFin() == 1) ? "F" : "-";
            d = (receivedSyn.getFin() == 0 && receivedSyn.getSyn() == 0 && receivedSyn.getAck() == 0) ? "D" : "-";
            double output = (double) receivedSyn.getTimeStamp() / 1000000000000.0;

            System.out.println("rcv " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                    receivedSyn.getByteSequenceNum() + " " + receivedSyn.getLength() + " " + receivedSyn.getAckNum());
            synAckPacket = new TCPPacket(0, 1, receivedSyn.getTimeStamp(),
                    0, syn, ack, fin, tempC);
            byte[] payload = receivedSyn.getPayload();
            filename = new String(payload);

            ByteArrayOutputStream outputStream;
            if(mtu < sws)
                outputStream = new ByteArrayOutputStream(mtu);
            else
                outputStream = new ByteArrayOutputStream(sws);

            ObjectOutputStream outObj = new ObjectOutputStream(outputStream);
            outObj.writeObject(synAckPacket);
            byte TCPbuf[] = outputStream.toByteArray();
            DatagramPacket synAck = new DatagramPacket(TCPbuf, TCPbuf.length,
                    receivedPayload.getAddress(), receivedPayload.getPort());
            sendPacket(synAck, socket);
            a = (synAckPacket.getAck() == 1) ? "A" : "-";
            s = (synAckPacket.getSyn() == 1) ? "S" : "-";
            f = (synAckPacket.getFin() == 1) ? "F" : "-";
            d = (synAckPacket.getFin() == 0 && synAckPacket.getSyn() == 0 && synAckPacket.getAck() == 0) ? "D" : "-";
            output = (double)synAckPacket.getTimeStamp()/1000000000000.0;

            System.out.println("snd " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                    synAckPacket.getByteSequenceNum() + " " + synAckPacket.getLength() + " " + synAckPacket.getAckNum());
            socket.setSoTimeout(2000);

            if(mtu < sws)
                buf = new byte[mtu + 185];
            else
                buf = new byte[sws + 185];
            if(mtu < sws)
                receivedPayload = new DatagramPacket(buf, mtu + 185);
            else
                receivedPayload = new DatagramPacket(buf, sws + 185);

            while(true) {
                try {
                    socket.receive(receivedPayload);
                    numPackets++;
                    byteArrayInputStream = new ByteArrayInputStream(receivedPayload.getData());
                    objectInputStream = new ObjectInputStream(byteArrayInputStream);
                    TCPPacket receivedAck = (TCPPacket) objectInputStream.readObject();
                    objectInputStream.close();
                    byteArrayInputStream.close();
                    a = (receivedAck.getAck() == 1) ? "A" : "-";
                    s = (receivedAck.getSyn() == 1) ? "S" : "-";
                    f = (receivedAck.getFin() == 1) ? "F" : "-";
                    d = (receivedAck.getFin() == 0 && receivedAck.getSyn() == 0 && receivedAck.getAck() == 0) ? "D" : "-";
                    output = (double) receivedAck.getTimeStamp() / 1000000000000.0;

                    System.out.println("rcv1 " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                            receivedAck.getByteSequenceNum() + " " + receivedAck.getLength() + " " + receivedAck.getAckNum());
                    break;
                } catch (SocketTimeoutException e) {
                    sendPacket(synAck, socket);
                    numPackRetransmit++;
                    a = (synAckPacket.getAck() == 1) ? "A" : "-";
                    s = (synAckPacket.getSyn() == 1) ? "S" : "-";
                    f = (synAckPacket.getFin() == 1) ? "F" : "-";
                    d = (synAckPacket.getFin() == 0 && synAckPacket.getSyn() == 0 && synAckPacket.getAck() == 0) ? "D" : "-";
                    output = (double)synAckPacket.getTimeStamp()/1000000000000.0;

                    System.out.println("snd " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                            synAckPacket.getByteSequenceNum() + " " + synAckPacket.getLength() + " " + synAckPacket.getAckNum());
                }
            }
        }
        return socket;
    }

    public DatagramSocket closeConnection(DatagramSocket socket, TCPPacket receivedFin, DatagramPacket receivedPayload) throws IOException, ClassNotFoundException {
        byte[] buf, tempC = {0};
        byte syn = 0, ack = 1, fin = 0;
        String a,s,d,f;
        TCPPacket finAckPacket;
        //String filename;
        //DatagramSocket socket = new DatagramSocket(port);
        if(mtu < sws)
            buf = new byte[mtu + 185];
        else
            buf = new byte[sws + 185];
//        DatagramPacket receivedPayload;
//        if(mtu < sws)
//            receivedPayload = new DatagramPacket(buf, mtu + 185);
//        else
//            receivedPayload = new DatagramPacket(buf, sws + 185);

        //socket.receive(receivedPayload);
       // if (receivedPayload != null) {

//            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(receivedPayload.getData());
//            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
//            TCPPacket receivedFin = (TCPPacket) objectInputStream.readObject();
//            objectInputStream.close();
//            byteArrayInputStream.close();
//
//            a = (receivedFin.getAck() == 1) ? "A" : "-";
//            s = (receivedFin.getSyn() == 1) ? "S" : "-";
//            f = (receivedFin.getFin() == 1) ? "F" : "-";
//            d = (receivedFin.getFin() == 0 && receivedFin.getSyn() == 0 && receivedFin.getAck() == 0) ? "D" : "-";
//            double output = (double) receivedFin.getTimeStamp() / 1000000000000.0;
//
//            System.out.println("rcv " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
//                    receivedFin.getByteSequenceNum() + " " + receivedFin.getLength() + " " + receivedFin.getAckNum());
            fin = 1;
            finAckPacket = new TCPPacket(1, receivedFin.getByteSequenceNum() + 1, receivedFin.getTimeStamp(),
                    0, syn, ack, fin, tempC);

            ByteArrayOutputStream outputStream;
            if(mtu < sws)
                outputStream = new ByteArrayOutputStream(mtu);
            else
                outputStream = new ByteArrayOutputStream(sws);

            ObjectOutputStream outObj = new ObjectOutputStream(outputStream);
            outObj.writeObject(finAckPacket);
            byte TCPbuf[] = outputStream.toByteArray();
            DatagramPacket synAck = new DatagramPacket(TCPbuf, TCPbuf.length,
                    receivedPayload.getAddress(), receivedPayload.getPort());
            sendPacket(synAck, socket);
            a = (finAckPacket.getAck() == 1) ? "A" : "-";
            s = (finAckPacket.getSyn() == 1) ? "S" : "-";
            f = (finAckPacket.getFin() == 1) ? "F" : "-";
            d = (finAckPacket.getFin() == 0 && finAckPacket.getSyn() == 0 && finAckPacket.getAck() == 0) ? "D" : "-";
            double output = (double)finAckPacket.getTimeStamp()/1000000000000.0;

            System.out.println("snd2 " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                    finAckPacket.getByteSequenceNum() + " " + finAckPacket.getLength() + " " + finAckPacket.getAckNum());
            socket.setSoTimeout(2000);

            if(mtu < sws)
                buf = new byte[mtu + 185];
            else
                buf = new byte[sws + 185];
            if(mtu < sws)
                receivedPayload = new DatagramPacket(buf, mtu + 185);
            else
                receivedPayload = new DatagramPacket(buf, sws + 185);

            while(true) {
                try {
                    socket.receive(receivedPayload);
                    numPackets++;
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(receivedPayload.getData());
                    ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
                    TCPPacket receivedAck = (TCPPacket) objectInputStream.readObject();
                    objectInputStream.close();
                    byteArrayInputStream.close();
                    a = (receivedAck.getAck() == 1) ? "A" : "-";
                    s = (receivedAck.getSyn() == 1) ? "S" : "-";
                    f = (receivedAck.getFin() == 1) ? "F" : "-";
                    d = (receivedAck.getFin() == 0 && receivedAck.getSyn() == 0 && receivedAck.getAck() == 0) ? "D" : "-";
                    output = (double) receivedAck.getTimeStamp() / 1000000000000.0;

                    System.out.println("rcv " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                            receivedAck.getByteSequenceNum() + " " + receivedAck.getLength() + " " + receivedAck.getAckNum());
                    break;
                } catch (SocketTimeoutException e) {
                    sendPacket(synAck, socket);
                    numPackRetransmit++;
                    a = (finAckPacket.getAck() == 1) ? "A" : "-";
                    s = (finAckPacket.getSyn() == 1) ? "S" : "-";
                    f = (finAckPacket.getFin() == 1) ? "F" : "-";
                    d = (finAckPacket.getFin() == 0 && finAckPacket.getSyn() == 0 && finAckPacket.getAck() == 0) ? "D" : "-";
                    output = (double)finAckPacket.getTimeStamp()/1000000000000.0;

                    System.out.println("snd " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " +
                            finAckPacket.getByteSequenceNum() + " " + finAckPacket.getLength() + " " + finAckPacket.getAckNum());
                }
            }
       // }
        System.out.println("Amount of Data Received: " + totalBytes + " bytes");
        System.out.println("No of Packets Received: " + numPackets);
        System.out.println("No of Packets Discarded (out of sequence): " + numDiscard);
        System.out.println("No of Packets discarded (wrong checksum): " + numCksm);
        System.out.println("No of Retransmissions: " + numPackRetransmit);
        System.out.println("No of Duplicate Acknowledgements: " + numPackDup);

        System.exit(0);
        return socket;
    }





}