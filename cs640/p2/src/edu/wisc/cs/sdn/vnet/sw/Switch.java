package edu.wisc.cs.sdn.vnet.sw;

import net.floodlightcontroller.packet.Ethernet;
import edu.wisc.cs.sdn.vnet.Device;
import edu.wisc.cs.sdn.vnet.DumpFile;
import edu.wisc.cs.sdn.vnet.Iface;
import net.floodlightcontroller.packet.MACAddress;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Aaron Gember-Jacobson
 */
public class Switch extends Device
{	
	/**
	 * Creates a router for a specific host.
	 * @param host hostname for the router
	 */
	private static Map<String,Iface> interfaces;
	private static Map<MACAddress, Iface> quickLookup = new HashMap();
	private static Map<Iface, Long> timeout = new HashMap();
	private static int count = 0;
	public Switch(String host, DumpFile logfile)
	{
		super(host,logfile);
	}

	/**
	 * Handle an Ethernet packet received on a specific interface.
	 * @param etherPacket the Ethernet packet that was received
	 * @param inIface the interface on which the packet was received
	 */


	public void handlePacket(Ethernet etherPacket, Iface inIface)
	{
		System.out.println("*** -> Received packet: " +
                etherPacket.toString().replace("\n", "\n\t"));
		
		/********************************************************************/
		/* TODO: Handle packets                                             */
		interfaces = getInterfaces();
		//first look for it in our table
		if(quickLookup.containsKey(etherPacket.getDestinationMAC())) {
			if(System.currentTimeMillis() - timeout.get(quickLookup.get(etherPacket.getDestinationMAC())) < 15000) {
				quickLookup.put(etherPacket.getSourceMAC(), inIface);
				timeout.put(inIface, System.currentTimeMillis());
				sendPacket(etherPacket, quickLookup.get(etherPacket.getDestinationMAC()));
				timeout.put(quickLookup.get(etherPacket.getDestinationMAC()), System.currentTimeMillis());
				count++;
				return;
			}
		}
		//couldn't find it so, look in the long list of interfaces

		for(Map.Entry<String, Iface> entry : interfaces.entrySet()) {
			if(!entry.getKey().equals(inIface.getName()))
				sendPacket(etherPacket, entry.getValue());
			else {
				quickLookup.put(etherPacket.getSourceMAC(), inIface);
				timeout.put(inIface, System.currentTimeMillis());
			}
		}
		count++;

		/********************************************************************/
	}
}

