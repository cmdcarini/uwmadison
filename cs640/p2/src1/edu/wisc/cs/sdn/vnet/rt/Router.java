package edu.wisc.cs.sdn.vnet.rt;

import edu.wisc.cs.sdn.vnet.Device;
import edu.wisc.cs.sdn.vnet.DumpFile;
import edu.wisc.cs.sdn.vnet.Iface;
import net.floodlightcontroller.packet.IPacket;
import net.floodlightcontroller.packet.IPv4;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.packet.MACAddress;

import javax.swing.event.DocumentEvent;
import java.nio.ByteBuffer;
import java.util.Map;

/**
 * @author Aaron Gember-Jacobson and Anubhavnidhi Abhashkumar
 */
public class Router extends Device
{	
	/** Routing table for the router */
	private RouteTable routeTable;
	
	/** ARP cache for the router */
	private ArpCache arpCache;
	private static Map<String,Iface> interfaces;
	/**
	 * Creates a router for a specific host.
	 * @param host hostname for the router
	 */
	public Router(String host, DumpFile logfile)
	{
		super(host,logfile);
		this.routeTable = new RouteTable();
		this.arpCache = new ArpCache();
	}
	
	/**
	 * @return routing table for the router
	 */
	public RouteTable getRouteTable()
	{ return this.routeTable; }
	
	/**
	 * Load a new routing table from a file.
	 * @param routeTableFile the name of the file containing the routing table
	 */
	public void loadRouteTable(String routeTableFile)
	{
		if (!routeTable.load(routeTableFile, this))
		{
			System.err.println("Error setting up routing table from file "
					+ routeTableFile);
			System.exit(1);
		}
		
		System.out.println("Loaded static route table");
		System.out.println("-------------------------------------------------");
		System.out.print(this.routeTable.toString());
		System.out.println("-------------------------------------------------");
	}
	
	/**
	 * Load a new ARP cache from a file.
	 * @param arpCacheFile the name of the file containing the ARP cache
	 */
	public void loadArpCache(String arpCacheFile)
	{
		if (!arpCache.load(arpCacheFile))
		{
			System.err.println("Error setting up ARP cache from file "
					+ arpCacheFile);
			System.exit(1);
		}
		
		System.out.println("Loaded static ARP cache");
		System.out.println("----------------------------------");
		System.out.print(this.arpCache.toString());
		System.out.println("----------------------------------");
	}

	/**
	 * Handle an Ethernet packet received on a specific interface.
	 * @param etherPacket the Ethernet packet that was received
	 * @param inIface the interface on which the packet was received
	 */
	public void handlePacket(Ethernet etherPacket, Iface inIface)
	{
		System.out.println("*** -> Received packet: " +
                etherPacket.toString().replace("\n", "\n\t"));
		
		/********************************************************************/
		/* TODO: Handle packets                                             */
		interfaces = getInterfaces();
		IPv4 magicHeader = new IPv4();
		IPv4 header = new IPv4();
		int currTtl;
		int length = header.getTotalLength()*4;
		byte[] data;
		IPacket magicPacket;
		if(etherPacket.getEtherType() != Ethernet.TYPE_IPv4) {
			return;
		}
		data = etherPacket.serialize();
		magicPacket = etherPacket.deserialize(data, 0, data.length);
		//if checksum is good
		header = (IPv4)etherPacket.getPayload();
		magicHeader = (IPv4)magicPacket.getPayload();

		if(header.getChecksum() != magicHeader.getChecksum()) {
			return;
		}

		currTtl = magicHeader.getTtl();
		magicHeader.setTtl((byte)currTtl--);
		if(magicHeader.getTtl() <= 0) {
			return;
		}

		for(Map.Entry<String, Iface> entry : interfaces.entrySet()) {
			if(entry.getValue().getIpAddress() == magicHeader.getDestinationAddress()) {
				return;
			}
		}
		RouteEntry longestMatch = routeTable.lookup(magicHeader.getDestinationAddress());

		if(longestMatch == null) {
			return;
		}

		MACAddress mac;

		if(longestMatch.getGatewayAddress() == 0)
			mac = arpCache.lookup(magicHeader.getDestinationAddress()).getMac();
		else
			mac = arpCache.lookup(longestMatch.getGatewayAddress()).getMac();

		if(etherPacket.getDestinationMAC().toString().equals(mac.toString())) {
			return;
		}
		etherPacket.setSourceMACAddress(etherPacket.getDestinationMACAddress());
		etherPacket.setDestinationMACAddress(mac.toString());

		sendPacket(etherPacket, longestMatch.getInterface());
		/********************************************************************/
	}
}

