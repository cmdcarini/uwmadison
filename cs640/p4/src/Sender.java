import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.text.DecimalFormat;
import java.util.*;

public class Sender {
    private static int port, remote_port, mtu, sws_original;
    private static int sws;
    private String remote_ip, file_name;
    private static ArrayList<TCPPacket> packetBuff = new ArrayList<TCPPacket>();
    private static HashMap<Integer, Integer[]> timeoutCount = new HashMap<>();
    private static int sizeOfData = 0 , numPackSent = 0 , numPackOutOfOrder = 0 , numPackWithBadChksm = 0 , numPackRetransmit = 0 , numDupAcks = 0 ;
    private double timeout = 5000000000.0;
    private int totalBytes = 0;
    public static boolean foundSomethingToRemove = false;
    private static ArrayList<TCPPacket> ackBuff = new ArrayList<>();
    private static int dupAckCount = 0;
    private static boolean hasReceivedFinalAck = false;
    private static boolean hasInitAck;
    private static boolean isActuallyDone = false;
    private static boolean hasFinalFinalAck = false;
    private static ArrayList<TCPPacket> sentBuff = new ArrayList<TCPPacket>();
    private static void exit(int err) {
        if(err == 1) {
            System.out.println("Error: missing or additional arguments");
            System.exit(0);
        } else if(err == 2) {
            System.out.println("Error: port number must be in the range 1024 to 65535");
            System.exit(0);
        } else if(err == 3) {
            System.out.println("Error: file not found");
            System.exit(0);
        } else if(err == 4) {
            System.out.println("Error: io exception");
            System.exit(0);
        }
    }

    public Sender() {

    }

    public Sender(int port, String remote_ip, int remote_port, String file_name, int mtu, int sws) {
        this.port = port;
        if (port < 1024 || port > 65535)
            exit(2);
        this.remote_ip = remote_ip;
        this.remote_port = remote_port;
        this.file_name = file_name;
        this.mtu = mtu;
        this.sws = sws;
        this.sws_original = sws;
    }

    /**
     *
     */
    public static int sequenceCounter;
    public void send_listen() {
        byte[] temp = null;
        String a,s,f,d;
        Timer timer = new Timer();
        sws_original = sws;
        File file = new File(file_name);
        int curr = 0, numPackets = 0;
        sequenceCounter = 1;
        byte[] payload = new byte[(int) file.length()], tempC = {0}, TCPbuf;
        TCPPacket packetToAdd, p;
        final Sender instance = new Sender();
        ArrayList<TCPPacket> sendBuff = new ArrayList<>();
        byte syn, ackB, fin;
        ByteArrayOutputStream outputStream;
        ObjectOutputStream outObj;
        DatagramPacket ack;
        InetAddress inet_addr;
        DatagramSocket out;
        final int seqCount;
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            fileInputStream.read(payload);
            fileInputStream.close();
            if(mtu < sws) {
                numPackets = (int) Math.ceil((payload.length) / (double) (mtu));
            } else {
                numPackets = (int) Math.ceil((payload.length) / (double) (sws));
            }
            for(int i = 0; i < numPackets; i++) {
                if(mtu < sws) {
                    temp = new byte[mtu];
                } else {
                    temp = new byte[sws];
                }
                //temp = new byte[mtu];
                curr = 0;
                if(mtu < sws) {
                    while((curr + (i*mtu) < (payload.length)) && curr < mtu) {
                        temp[curr] = payload[curr + (i*mtu)];
                        curr+=1;
                    }
                } else {
                    while((curr + (i*sws) < (payload.length)) && curr < sws) {
                        temp[curr] = payload[curr + (i*sws)];
                        curr+=1;
                    }
                }

                syn = 0;
                ackB = 0;
                fin = 0;
                packetToAdd = new TCPPacket(sequenceCounter, 0, System.nanoTime(),
                        curr , syn, ackB, fin, tempC);
                packetToAdd.setPayload(temp);
                packetToAdd.setChecksum(packetToAdd.getByteSequenceNum());

                sendBuff.add(packetToAdd);
                sequenceCounter = packetToAdd.getByteSequenceNum() + packetToAdd.getLength();
            }

            inet_addr = InetAddress.getByName(remote_ip);
            out = new DatagramSocket();
            seqCount = sequenceCounter;
            final InetAddress inetAddressFinal = inet_addr;
            final DatagramSocket outFinal = out;
            timer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    checkBuff(outFinal, inetAddressFinal, remote_port);
                }
            }, 0, 100);

            timer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    handleAck(outFinal.getLocalPort(), outFinal.getInetAddress(), outFinal, inetAddressFinal, remote_port);
                }
            }, 0, 10);

            double timestarted = System.nanoTime();
            instance.establishConnection(out, inet_addr, remote_port, timestarted, file_name);

            while(sendBuff.size() != 0) {
                for (int i = 0; i < sendBuff.size(); i++) {
                    p = sendBuff.get(i);
                    if (sws >= p.getLength() ) {
                        if(mtu < sws_original)
                            outputStream = new ByteArrayOutputStream(mtu);
                        else
                            outputStream = new ByteArrayOutputStream(sws_original);

                        outObj = new ObjectOutputStream(outputStream);
                        outObj.writeObject(p);
                        outObj.flush();
                        outputStream.flush();
                        TCPbuf = outputStream.toByteArray();
                        ack = new DatagramPacket(TCPbuf, TCPbuf.length, inet_addr, remote_port);
                        out.send(ack);
                        sentBuff.add(p);
                        synchronized (packetBuff) {
                            packetBuff.add(p);
                        }
                        a = (p.getAck() == 1) ? "A" : "-";
                        s = (p.getSyn() == 1) ? "S" : "-";
                        f = (p.getFin() == 1) ? "F" : "-";
                        d = (p.getFin() == 0 && p.getSyn() == 0 && p.getAck() == 0) ? "D" : "-";
                        totalBytes += p.getLength();
                        double output = (double)p.getTimeStamp()/1000000000000.0;
                        System.out.println("snd " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " + p.getByteSequenceNum() + " " + p.getLength() + " " + p.getAckNum());
                        sendBuff.remove(i);
                        i-=1;
                        synchronized ((Object) sws) {
                            sws -= p.getLength();
                        }
                        outObj.close();
                        outputStream.close();
                    } else {

                        while(sws < p.getLength()) {
                            synchronized ((Object) sws) {
                                if (sws >= p.getLength()) {

                                    break;
                                }
                            }
                        }
                        i-=1;
                        break;
                    }
                }
            }
            while(true) {
                synchronized ((Object) hasReceivedFinalAck) {
                    if(hasReceivedFinalAck) {
                        closeConnection(out, inet_addr, remote_port);
                        break;
                    }
                }
            }
            synchronized ((Object) totalBytes) {
                //this.totalBytes = instance.totalBytes + this.totalBytes;
                System.out.println("Amount of Data Transfered: " + totalBytes + " bytes");
            }
            System.out.println("No of Packets Sent: " + (numPackets + 4));
            System.out.println("No of Packets Discarded (out of sequence): 0");
            System.out.println("No of Packets discarded (wrong checksum): " + numPackWithBadChksm);
            System.out.println("No of Retransmissions: " + numPackRetransmit);
            System.out.println("No of Duplicate Acknowledgements: " + dupAckCount);
            //socket.close();
            //out.close();
        } catch (FileNotFoundException e) {
            exit(3);
        } catch (IOException e) {
            exit(4);
        }
        System.exit(0);
    }

    public void checkBuff(DatagramSocket out, InetAddress inet_addr, int remote_port) {
        TCPPacket p;
        String a,s,f,d;
        ByteArrayOutputStream outputStream;
        byte TCPbuf[];
        DatagramPacket ack;
            for (int i = 0; i < packetBuff.size(); i++) {
                synchronized (packetBuff) {
                    p = packetBuff.get(i);
                }

                synchronized ((Object) timeout) {
                    if ((System.nanoTime() - p.getTimeStamp()) > timeout) {
                        if (timeoutCount.containsKey(p.getByteSequenceNum()) && timeoutCount.get(p.getByteSequenceNum())[1] > 16) {
                            System.out.println("Error: Ack not received for packet before max num retransmissions");
                            System.exit(-1);
                        }
                        if (!timeoutCount.containsKey(p.getByteSequenceNum())) {
                            Integer[] arr = {0, 1};
                            timeoutCount.put(p.getByteSequenceNum(), arr);
                        } else {
                            Integer[] arr = timeoutCount.get(p.getByteSequenceNum());
                            arr[1] += 1;
                            timeoutCount.put(p.getByteSequenceNum(), arr);
                        }
                        if (mtu < sws_original)
                            outputStream = new ByteArrayOutputStream(mtu);
                        else
                            outputStream = new ByteArrayOutputStream(sws_original);

                        p.setTimeStamp(System.nanoTime());
                        try (ObjectOutputStream outObj = new ObjectOutputStream(outputStream)) {
                            outObj.writeObject(p);
                            outObj.flush();
                            outputStream.flush();
                            TCPbuf = outputStream.toByteArray();
                            ack = new DatagramPacket(TCPbuf, TCPbuf.length, inet_addr, remote_port);
                            numPackRetransmit++;
                            out.send(ack);
                            sentBuff.add(p);
                            a = (p.getAck() == 1) ? "A" : "-";
                            s = (p.getSyn() == 1) ? "S" : "-";
                            f = (p.getFin() == 1) ? "F" : "-";
                            d = (p.getFin() == 0 && p.getSyn() == 0 && p.getAck() == 0) ? "D" : "-";
                            //sws += p.getByteSequenceNum();
                            //totalBytes += p.getLength();
                            double output = (double) p.getTimeStamp() / 1000000000000.0;
                            System.out.println("snd " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " + p.getByteSequenceNum() + " " + p.getLength() + " " + p.getAckNum());

                            outObj.close();
                            outputStream.close();

                        } catch (IOException e) {
                            e.printStackTrace();
                            exit(4);
                        }
                    }

                }

        }


    }
    public static double ERTT = 0, EDEV = 0, SRTT = 0, SDEV = 0;

    public void timeoutCalculator(int seqNum, double timeSent) {
        double timeReceived = System.nanoTime();
        if(seqNum == 0) {
            ERTT = ((timeReceived) - (timeSent));
            EDEV = 0;
            timeout = 2*ERTT;
        } else  {
            SRTT = ((timeReceived) - (timeSent));
            SDEV = Math.abs(SRTT - ERTT);
            ERTT = (long)(0.875*ERTT + (1-0.875)*SRTT);
            EDEV = (long)(0.75*EDEV + (1-0.75)*SDEV);
            timeout = ERTT + 4*EDEV;
        }
        //return timeout;
    }

    public boolean establishConnection(DatagramSocket out, InetAddress inet_addr, int remote_port, double timeStarted, String fileName) {
        //TODO: Implement an establish Connection class
        String a,s,f,d;
        int count = 1;
        byte syn = 1;
        byte fin = 0;
        byte ackByte = 0;
        byte[] tempC = {0};
        hasInitAck = false;
        double resendTime = 5000000000.0;
        while(!hasInitAck) {
            if (count == 1 || System.nanoTime() - timeStarted > resendTime) {
                try {
                    TCPPacket init = new TCPPacket(0, 0, System.nanoTime(), 0, syn, ackByte, fin, tempC);
                    init.setPayload(fileName.getBytes());
                    init.setChecksum(init.getByteSequenceNum());
                    ByteArrayOutputStream initStream;
                    if (mtu < sws_original)
                        initStream = new ByteArrayOutputStream(mtu);
                    else
                        initStream = new ByteArrayOutputStream(sws_original);

                    ObjectOutputStream initObj = new ObjectOutputStream(initStream);
                    initObj.writeObject(init);
                    initObj.flush();
                    initStream.flush();
                    byte TCPbufinit[] = initStream.toByteArray();
                    DatagramPacket ackpack = new DatagramPacket(TCPbufinit, TCPbufinit.length, inet_addr, remote_port);
                    out.send(ackpack);
                    sentBuff.add(init);
                    a = (init.getAck() == 1) ? "A" : "-";
                    s = (init.getSyn() == 1) ? "S" : "-";
                    f = (init.getFin() == 1) ? "F" : "-";
                    d = (init.getFin() == 0 && init.getSyn() == 0 && init.getAck() == 0) ? "D" : "-";
                    // totalBytes += init.getLength();
                    double output = (double) init.getTimeStamp() / 1000000000000.0;
                    System.out.println("snd " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " + init.getByteSequenceNum() + " " + init.getLength() + " " + init.getAckNum());
                    //spin
                    if(count == 1)
                        count--;
                    else {
                        timeStarted = System.nanoTime();
                    }

                    //Thread.sleep(5000);

                } catch (IOException e) {
                    e.printStackTrace();
                    exit(4);
                } /*catch(InterruptedException e) {
                e.printStackTrace();
            }*/
            }
            synchronized ((Object) hasInitAck) {
                if (this.hasInitAck == true)
                    return true;
            }
        }
        return true;
    }

    public boolean closeConnection(DatagramSocket out, InetAddress inet_addr, int remote_port) {
        //TODO: Implement an establish Connection class
        String a,s,f,d;
        byte syn = 0;
        byte fin = 1;
        byte ackByte = 0;
        byte[] tempC = {0};
        hasFinalFinalAck = false;
        try {
            TCPPacket init = new TCPPacket(sequenceCounter, 1, System.nanoTime(), 0, syn, ackByte, fin, tempC);
            init.setChecksum(init.getByteSequenceNum());
            ByteArrayOutputStream initStream;
            if(mtu < sws_original)
                initStream = new ByteArrayOutputStream(mtu);
            else
                initStream = new ByteArrayOutputStream(sws_original);
            ObjectOutputStream initObj = new ObjectOutputStream(initStream);
            initObj.writeObject(init);
            initObj.flush();
            initStream.flush();
            byte TCPbufinit[] = initStream.toByteArray();
            DatagramPacket ackpack = new DatagramPacket(TCPbufinit, TCPbufinit.length, inet_addr, remote_port);
            out.send(ackpack);
            sentBuff.add(init);
            packetBuff.add(init);
            a = (init.getAck() == 1) ? "A" : "-";
            s = (init.getSyn() == 1) ? "S" : "-";
            f = (init.getFin() == 1) ? "F" : "-";
            d = (init.getFin() == 0 && init.getSyn() == 0 && init.getAck() == 0) ? "D" : "-";
            // totalBytes += init.getLength();
            double output = (double)init.getTimeStamp()/1000000000000.0;
            System.out.println("snd " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " + init.getByteSequenceNum() + " " + init.getLength() + " " + init.getAckNum());
            while(true) {
                //spin
                synchronized ((Object)hasFinalFinalAck) {
                    if (hasFinalFinalAck == true)
                        break;
                }
            }
        } catch(IOException e) {
            e.printStackTrace();
            exit(4);
        }
        isActuallyDone = true;
        return true;

    }

    public void handleAck(int port, InetAddress addr, DatagramSocket socket, InetAddress ackAddr, int remote_port) {
        boolean done = false;
        int gotMessage;
        boolean waiting = false;
        boolean hasSentAck = false;
        //Object receivedPayload;
        String a,s,f,d;
        try {
            done = true;
            while (done) {
                byte buf[];
                if(mtu < sws_original)
                    buf = new byte[mtu + 185];
                else
                    buf = new byte[sws_original + 185];
                DatagramPacket receivedPayload;
                if(mtu < sws_original)
                    receivedPayload = new DatagramPacket(buf, mtu + 185);
                else
                    receivedPayload = new DatagramPacket(buf, sws_original + 185);

                socket.receive(receivedPayload);

                if (receivedPayload != null) {
                    waiting = true;
                    ByteArrayInputStream test = new ByteArrayInputStream(receivedPayload.getData());
                    ObjectInputStream testObj = new ObjectInputStream(test);
                    TCPPacket receivedPack = (TCPPacket)testObj.readObject();
                    testObj.close();
                    test.close();

                    //check to see if checksum is valid
                    if(!Arrays.equals(receivedPack.CalcCheckSum(receivedPack.getByteSequenceNum()), receivedPack.getChecksum())){
                        numPackWithBadChksm++;
                        continue;
                    }
                    a = (receivedPack.getAck() == 1) ? "A" : "-";
                    s = (receivedPack.getSyn() == 1) ? "S" : "-";
                    f = (receivedPack.getFin() == 1) ? "F" : "-";
                    d = (receivedPack.getFin() == 0 && receivedPack.getSyn() == 0 && receivedPack.getAck() == 0) ? "D" : "-";
                    double output = (double)receivedPack.getTimeStamp()/1000000000000.0;

                    System.out.println("rcv " + String.format("%.3f", output) + " " + s + " " + a + " " + f + " " + d + " " + receivedPack.getByteSequenceNum() + " " + receivedPack.getLength() + " " + receivedPack.getAckNum());

                    boolean found = false;
                    //checking ACK Buffer for the Ack # of the received packet, increments counter if found
                    for(TCPPacket p: ackBuff) {
                        if(p.getAckNum() == receivedPack.getAckNum()) {
                            dupAckCount++;
                            int maxSeq = 0;
                            for(int i = 0; i < sentBuff.size(); i++) {
                                if(receivedPack.getAckNum() > sentBuff.get(i).getByteSequenceNum() && maxSeq < sentBuff.get(i).getByteSequenceNum()) {
                                    maxSeq =  sentBuff.get(i).getByteSequenceNum();
                                } else {
                                    continue;
                                }

                            }
                            int swsAddr = 0;
                            for(TCPPacket x: sentBuff) {
                                if(x.getByteSequenceNum() == maxSeq) {
                                    swsAddr = x.getLength();
                                }
                            }
                            //sws += swsAddr;
                            if (!timeoutCount.containsKey(p.getAckNum())) {
                                Integer[] arr = {1,0};
                                timeoutCount.put(p.getAckNum(), arr);
                            } else {
                                Integer[] arr = timeoutCount.get(p.getAckNum());
                                arr[0] += 1;
                                timeoutCount.put(p.getAckNum(), arr);
                            }
                            found = true;
                        }
                        if(found) {
                            break;
                        }
                    }

                    //if not found in ACK Buffer, be sure to add it
                    if(!found) {
                        ackBuff.add(receivedPack);
                        timeoutCalculator(receivedPack.getByteSequenceNum(), receivedPack.getTimeStamp());

                    }

                    boolean foundPacket = false;
                    for(TCPPacket p: packetBuff) {
                        if(p.getByteSequenceNum() == receivedPack.getAckNum()) {
                            foundPacket = true;
                        }
                    }

//                    for(Map.Entry<TCPPacket, Integer> p: timeoutCount.entrySet()) {
//                        if(p.getKey().getByteSequenceNum() == receivedPack.getAckNum()) {
//
//                        }
//                    }
                    //checking to see if its been sent more than 16 times
                    if(timeoutCount.containsKey(receivedPack.getAckNum()) &&
                            timeoutCount.get(receivedPack.getAckNum())[1] > 16) {
                        System.out.println("Error: Ack not received for packet before max num retransmissions");
                        System.exit(-1);
                    }
                    TCPPacket dupPacket = null;

                    //retransmit if greater than 3 acks
                    if(foundPacket && timeoutCount.containsKey(receivedPack.getAckNum()) &&
                            timeoutCount.get(receivedPack.getAckNum())[0] % 1 == 0 && timeoutCount.get(receivedPack.getAckNum())[0] != 0) {
                        for (int i = 0; i < packetBuff.size(); i++) {
                            if (packetBuff.get(i).getByteSequenceNum() == receivedPack.getAckNum()) {
                                synchronized (packetBuff) {
                                    dupPacket = packetBuff.get(i);
                                }
                            }
                        }
                        Integer[] arr = timeoutCount.get(receivedPack.getAckNum());
                        //arr[0] = 0;
                        timeoutCount.put(receivedPack.getAckNum(), arr);
                        ByteArrayOutputStream outputStream;
                        if(mtu < sws_original)
                            outputStream = new ByteArrayOutputStream(mtu);
                        else
                            outputStream = new ByteArrayOutputStream(sws_original);

                        dupPacket.setTimeStamp(System.nanoTime());
                        try (ObjectOutputStream outObj = new ObjectOutputStream(outputStream)) {
                            outObj.writeObject(dupPacket);
                            outObj.flush();
                            outputStream.flush();
                            byte TCPbuf[] = outputStream.toByteArray();
                            DatagramPacket ack = new DatagramPacket(TCPbuf, TCPbuf.length, ackAddr, remote_port);
                            numPackRetransmit++;
                            socket.send(ack);
                            sentBuff.add(dupPacket);
                            a = (dupPacket.getAck() == 1) ? "A" : "-";
                            s = (dupPacket.getSyn() == 1) ? "S" : "-";
                            f = (dupPacket.getFin() == 1) ? "F" : "-";
                            d = (dupPacket.getFin() == 0 && dupPacket.getSyn() == 0 && dupPacket.getAck() == 0) ? "D" : "-";
                            //totalBytes += dupPacket.getLength();
                            double outputTmp = (double)dupPacket.getTimeStamp()/1000000000000.0;
                            //sws += dupPacket.getLength();
                            System.out.println("snd " + String.format("%.3f", outputTmp) + " " + s + " " + a + " " + f + " " + d + " " + dupPacket.getByteSequenceNum() + " " + dupPacket.getLength() + " " + dupPacket.getAckNum());
                            outObj.close();
                            outputStream.close();

                        } catch (IOException e) {
                            e.printStackTrace();
                            exit(4);
                        }
                    }
//                     else if(foundPacket && timeoutCount.containsKey(receivedPack.getAckNum())) {
//                        timeoutCount.put(receivedPack.getAckNum(), timeoutCount.get(receivedPack.getAckNum()) + 1);
//                    }

                    //special condition for receiving first SYN/ACK
                    if(receivedPack.getAckNum() == 1 && receivedPack.getAck() == 1 && receivedPack.getSyn() == 1) {
                        byte s2 = 0, ackByte = 1, fin = 0;
                        byte[] tempC = {0};
                        TCPPacket init = new TCPPacket(0, 1, System.nanoTime(), 0, s2, ackByte, fin, tempC);
                        init.setChecksum(init.getByteSequenceNum());
                        ByteArrayOutputStream outputStream;
                        if(mtu < sws_original)
                            outputStream = new ByteArrayOutputStream(mtu);
                        else
                            outputStream = new ByteArrayOutputStream(sws_original);

                        ObjectOutputStream outObj = new ObjectOutputStream(outputStream);
                        outObj.writeObject(init);
                        outObj.flush();
                        outputStream.flush();
                        byte TCPbuf[] = outputStream.toByteArray();
                        DatagramPacket ack = new DatagramPacket(TCPbuf, TCPbuf.length, ackAddr, remote_port);
                        socket.send(ack);
                        sentBuff.add(init);
                        a = (init.getAck() == 1) ? "A" : "-";
                        s = (init.getSyn() == 1) ? "S" : "-";
                        f = (init.getFin() == 1) ? "F" : "-";
                        d = (init.getFin() == 0 && init.getSyn() == 0 && init.getAck() == 0) ? "D" : "-";
                        //totalBytes += init.getLength();
                        double outputTmp = (double)init.getTimeStamp()/1000000000000.0;
                        System.out.println("snd " + String.format("%.3f", outputTmp) + " " + s + " " + a + " " + f + " " + d + " " + init.getByteSequenceNum() + " " + init.getLength() + " " + init.getAckNum());

                        //String filename = fileName;
//                        byte[] payload = fileName.getBytes();
//                        ackByte = 0;
//                        TCPPacket file = new TCPPacket(1, 0, System.nanoTime(), payload.length, s2, ackByte, fin, tempC);
//                        file.setPayload(payload);
//                        file.setChecksum(file.getByteSequenceNum());
//                        if(mtu < sws_original)
//                            outputStream = new ByteArrayOutputStream(mtu);
//                        else
//                            outputStream = new ByteArrayOutputStream(sws_original);
//
//                        outObj = new ObjectOutputStream(outputStream);
//                        outObj.writeObject(file);
//                        outObj.flush();
//                        outputStream.flush();
//                        TCPbuf = outputStream.toByteArray();
//                        ack = new DatagramPacket(TCPbuf, TCPbuf.length, ackAddr, remote_port);
//                        socket.send(ack);
//                        sentBuff.add(file);
//                        synchronized (packetBuff) {
//                            packetBuff.add(file);
//                        }
//                        a = (file.getAck() == 1) ? "A" : "-";
//                        s = (file.getSyn() == 1) ? "S" : "-";
//                        f = (file.getFin() == 1) ? "F" : "-";
//                        d = (file.getFin() == 0 && file.getSyn() == 0 && file.getAck() == 0) ? "D" : "-";
//                        totalBytes += file.getLength();
//                        double outputTmpTmp = (double)file.getTimeStamp()/1000000000000.0;
//                        System.out.println("snd " + String.format("%.3f", outputTmpTmp) + " " + s + " " + a + " " + f + " " + d + " " + file.getByteSequenceNum() + " " + file.getLength() + " " + file.getAckNum());
                        this.hasInitAck = true;


                    }

                    //special condition for receiving last ACK
                    if(receivedPack.getAckNum() == sequenceCounter && receivedPack.getAck() == 1) {
                        hasReceivedFinalAck = true;
                    }

                    //special condition for receiving FIN/ACK
                    if(receivedPack.getAck() == 1 && receivedPack.getFin() == 1) {
                        byte s2 = 0, ackByte = 1, fin = 0;
                        byte[] tempC = {0};
                        TCPPacket init = new TCPPacket(sequenceCounter, receivedPack.getByteSequenceNum() +1, System.nanoTime(), 0, s2, ackByte, fin, tempC);
                        init.setChecksum(init.getByteSequenceNum());
                        ByteArrayOutputStream outputStream;
                        if(mtu < sws_original)
                            outputStream = new ByteArrayOutputStream(mtu);
                        else
                            outputStream = new ByteArrayOutputStream(sws_original);

                        ObjectOutputStream outObj = new ObjectOutputStream(outputStream);
                        outObj.writeObject(init);
                        outObj.flush();
                        outputStream.flush();
                        byte TCPbuf[] = outputStream.toByteArray();
                        DatagramPacket ack = new DatagramPacket(TCPbuf, TCPbuf.length, ackAddr, remote_port);
                        socket.send(ack);
                        sentBuff.add(init);
                        a = (init.getAck() == 1) ? "A" : "-";
                        s = (init.getSyn() == 1) ? "S" : "-";
                        f = (init.getFin() == 1) ? "F" : "-";
                        d = (init.getFin() == 0 && init.getSyn() == 0 && init.getAck() == 0) ? "D" : "-";
                        //totalBytes += init.getLength();
                        double outputTmp = (double)init.getTimeStamp()/1000000000000.0;
                        System.out.println("snd " + String.format("%.3f", outputTmp) + " " + s + " " + a + " " + f + " " + d + " " + init.getByteSequenceNum() + " " + init.getLength() + " " + init.getAckNum());
                        hasFinalFinalAck = true;
                    }

                    TCPPacket p;
                    for (int i = 0; i < packetBuff.size(); i++) {
                        synchronized (packetBuff) {
                            p = packetBuff.get(i);
                        }
                        if (p.getByteSequenceNum() < receivedPack.getAckNum()) {

                            packetBuff.remove(i);
                            i-=1;
                            timeoutCount.remove(p.getByteSequenceNum());

                            synchronized ((Object) sws) {
                                sws += p.getLength();
                            }
                        }
                        if (packetBuff.size() == 0 && isActuallyDone)
                            done = false;
                    }
                }

                if (receivedPayload != null && waiting) {
                    done = false;
                    waiting = false;
                    //input.close();
                }
            }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                    + port + " or listening for a connection");
            e.printStackTrace();
            exit(4);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}