import java.io.Serializable;
import java.util.Arrays;
import java.nio.ByteBuffer;

public class TCPPacket implements Serializable {

    private int byteSequenceNum;
    private int ackNum;
    private long timeStamp;
    private int length;
    private byte syn;
    private byte ack;
    private byte fin;
    private byte[] checksum;
    private byte[] payload;
    //default constructor
    public TCPPacket(int byteSequenceNum, int ackNum, long timeStamp, int length,
                     byte syn, byte ack, byte fin, byte[] checksum) {
        this.byteSequenceNum = byteSequenceNum;
        this.ackNum = ackNum;
        this.timeStamp = timeStamp;
        this.length = length;
        this.syn = syn;
        this.ack = ack;
        this.fin = fin;
        this.checksum = checksum;
    }

    public void setByteSequenceNum(int byteSequenceNum) {
        this.byteSequenceNum = byteSequenceNum;
    }

    public void setAckNum(int ackNum) {
        this.ackNum = ackNum;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setSyn(byte syn) {
        this.syn = syn;
    }

    public void setAck(byte ack) {
        this.ack = ack;
    }

    public void setFin(byte fin) {
        this.fin = fin;
    }

    public void setChecksum(int sequenceNum) {
        //this.checksum = checksum;
        this.checksum = CalcCheckSum(sequenceNum);
    }

    public int getByteSequenceNum() {
        return byteSequenceNum;
    }

    public int getAckNum() {
        return ackNum;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public int getLength() {
        return length;
    }

    public int getSyn() {
        return syn;
    }

    public int getAck() {
        return ack;
    }

    public int getFin() {
        return fin;
    }

    public byte[] getChecksum() {
        return checksum;
    }

    public byte[] getPayload() { return payload; }

    public void setPayload(byte[] payload) {
        this.payload = payload;
    }


    public static byte[] CalcCheckSum (int header)
    {
        String first = Integer.toBinaryString(header);

        //add extra 0s if needed
        for (int i = first.length(); i < 32; i++)
        {
            first = "0" + first;
        }
        String a = first.substring(0, 15);
        String b = first.substring(16, 31);
        // Initialize result
        String result = "";
        // Initialize digit sum
        int s = 0;
        // Traverse both strings starting
        // from last characters
        int i = a.length() - 1, j = b.length() - 1;
        while (i >= 0 || j >= 0 || s == 1)
        {
            // Compute sum of last
            // digits and carry
            s += ((i >= 0)? a.charAt(i) - '0': 0);
            s += ((j >= 0)? b.charAt(j) - '0': 0);

            // If current digit sum is
            // 1 or 3, add 1 to result
            result = (char)(s % 2 + '0') + result;

            // Compute carry
            s /= 2;

            // Move to next digits
            i--; j--;
        }
        //move extra bit to the back
        if (result.length() > 16)
        {
            String result2 = result.substring(1, 14);
            if (result.charAt(0) == '1' && result.charAt(15) == '1')
            {
                result2 += "0";
            }
            else if (result.charAt(0) == '1' && result.charAt(15) == '0')
            {
                result2 += "1";
            }
            result = result2;
        }
        String result3 ="";
        //flip all bits
        for (int k = 0; k < 15; k++)
        {
            if (result.charAt(k) == '1')
            {
                result3 += '0';
            }
            else
            {
                result3 += '1';
            }
        }
        result = result3;
        short n = Short.parseShort(result, 2);
        ByteBuffer bytes = ByteBuffer.allocate(2).putShort(n);
        byte[] returnArray = bytes.array();
        return returnArray;

    }

}