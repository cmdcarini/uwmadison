import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.lang.instrument.Instrumentation;
import java.util.*;
import java.util.zip.Adler32;

public class TCPend {

    private static void exit(int err) {
        if(err == 1) {
            System.out.println("Error: missing or additional arguments");
            System.exit(0);
        } else if(err == 2) {
            System.out.println("Error: port number must be in the range 1024 to 65535");
            System.exit(0);
        } else if(err == 3) {
            System.out.println("Error: file not found");
            System.exit(0);
        } else if(err == 4) {
            System.out.println("Error: io exception");
            System.exit(0);
        }
    }

    public static void main(String[] args) {
        //command line vars go here
        int port, remote_port, mtu, sws;
        String remote_ip, file_name;
        Sender sender;
        Receiver receiver;
        //"receiver" == 6 args
        if(args.length == 6) {
            port = Integer.parseInt(args[1]);
            if (port < 1024 || port > 65535)
                exit(2);
            mtu = Integer.parseInt(args[3]);
            sws = Integer.parseInt(args[5]);
            receiver = new Receiver(port, mtu, sws);
            receiver.main();
        }
        //"sender" == 12 args
        else if(args.length == 12){
            port = Integer.parseInt(args[1]);
            remote_ip = args[3];
            remote_port = Integer.parseInt(args[5]);
            if (port < 1024 || port > 65535 || remote_port < 1024 || remote_port > 65535)
                exit(2);
            file_name = args[7];
            mtu = Integer.parseInt(args[9]);
            sws = Integer.parseInt(args[11]);
            sender = new Sender(port, remote_ip, remote_port, file_name, mtu, sws);
            sender.send_listen();
        } else {
            exit(1);
        }
        //System.out.println("Hello World!");
    }



    public boolean timeoutChecker() {
        return false;
    }


}