package edu.wisc.cs.sdn.vnet.rt;

import edu.wisc.cs.sdn.vnet.Device;
import edu.wisc.cs.sdn.vnet.DumpFile;
import edu.wisc.cs.sdn.vnet.Iface;
import net.floodlightcontroller.packet.*;

import java.lang.reflect.Array;
import java.util.List;
import java.util.*;
/**
 * @author Aaron Gember-Jacobson and Anubhavnidhi Abhashkumar
 */
public class Router extends Device
{	
	/** Routing table for the router */
	private RouteTable routeTable;
	private Map<Integer, RIPv2Entry> ripTable;
	private Map<Integer, Long> timeoutTable;
	/** ARP cache for the router */
	private ArpCache arpCache;
	//private long time;
	/**
	 * Creates a router for a specific host.
	 * @param host hostname for the router
	 */
	public Router(String host, DumpFile logfile)
	{
		super(host,logfile);
		this.routeTable = new RouteTable();
		this.arpCache = new ArpCache();
		this.ripTable = new HashMap<Integer, RIPv2Entry>();
		this.timeoutTable = new HashMap<Integer, Long>();
	}
	
	/**
	 * @return routing table for the router
	 */
	public RouteTable getRouteTable()
	{ return this.routeTable; }
	
	/**
	 * Load a new routing table from a file.
	 * @param routeTableFile the name of the file containing the routing table
	 */
	public void loadRouteTable(String routeTableFile)
	{
		if (!routeTable.load(routeTableFile, this))
		{
			System.err.println("Error setting up routing table from file "
					+ routeTableFile);
			System.exit(1);
		}
		
		System.out.println("Loaded static route table");
		System.out.println("-------------------------------------------------");
		System.out.print(this.routeTable.toString());
		System.out.println("-------------------------------------------------");
	}
	
	/**
	 * Load a new ARP cache from a file.
	 * @param arpCacheFile the name of the file containing the ARP cache
	 */
	public void loadArpCache(String arpCacheFile)
	{
		if (!arpCache.load(arpCacheFile))
		{
			System.err.println("Error setting up ARP cache from file "
					+ arpCacheFile);
			System.exit(1);
		}
		
		System.out.println("Loaded static ARP cache");
		System.out.println("----------------------------------");
		System.out.print(this.arpCache.toString());
		System.out.println("----------------------------------");
	}

	/**
	 * Handle an Ethernet packet received on a specific interface.
	 * @param etherPacket the Ethernet packet that was received
	 * @param inIface the interface on which the packet was received
	 */
	public void handlePacket(Ethernet etherPacket, Iface inIface)
	{
		System.out.println("*** -> Received packet: " +
                etherPacket.toString().replace("\n", "\n\t"));
		
		/********************************************************************/
		/* TODO: Handle packets                                             */
		int RIP_IP = IPv4.toIPv4Address("224.0.0.9");
		//byte[] RIP_MAC = Ethernet.toMACAddress("FF:FF:FF:FF:FF:FF");
		UDP udp = null;
		IPv4 ip = (IPv4)etherPacket.getPayload();
		//List<RIPv2Entry> entries = null;
		if(ip.getProtocol() == IPv4.PROTOCOL_UDP) {
			udp = (UDP)ip.getPayload();
			//rip = (RIPv2)udp.getPayload();
		}
//		if(rip != null)
//			entries = rip.getEntries();
//		if((System.currentTimeMillis() - time) > 30000) {
////			populateTable();
////		}
		//System.out.println("Dest: " + ip.getDestinationAddress() + "Proto: " + ip.getProtocol() + "Port: " + udp.getDestinationPort());

		switch(etherPacket.getEtherType())
		{
		case Ethernet.TYPE_IPv4:
			if((ip.getDestinationAddress() == RIP_IP ||  ip.getDestinationAddress() == inIface.getIpAddress()) && ip.getProtocol() == IPv4.PROTOCOL_UDP && udp.getDestinationPort() == (short)520) {
				//System.out.println("Hey I found a RIP Packet!");
				this.handleRipPacket(etherPacket, inIface, ip);
				break;
			}

			this.handleIpPacket(etherPacket, inIface);
			break;
		// Ignore all other packet types, for now
		}
		
		/********************************************************************/
	}

	private void handleRipPacket(Ethernet etherPacket, Iface inIface, IPv4 ip) {
		UDP udp = (UDP)ip.getPayload();
		IPv4 etherToIP = (IPv4)etherPacket.getPayload();
//		System.out.println("*** -> Received RIP packet: " +
//		      etherPacket.toString().replace("\n", "\n\t"));
		RIPv2 ripPacket = (RIPv2) udp.getPayload();
		RIPv2 ripResponse = new RIPv2();
		Ethernet response = new Ethernet();
		int RIP_IP = IPv4.toIPv4Address("224.0.0.9");
		byte[] RIP_MAC = Ethernet.toMACAddress("FF:FF:FF:FF:FF:FF");
		IPv4 ipPacket = new IPv4();
		UDP udpPacket = new UDP();
		//System.out.println("Handle RIP packet");
		List<RIPv2Entry> responseTable = new ArrayList<RIPv2Entry>();
		RIPv2Entry entryToTable;
		if(ripPacket.getCommand() == RIPv2.COMMAND_REQUEST) {
			//send response
			for(RIPv2Entry r: this.ripTable.values()) {
				responseTable.add(r);
			}
			ripResponse.setCommand(RIPv2.COMMAND_RESPONSE);
			ripResponse.setEntries(responseTable);
			udpPacket.setPayload(ripResponse);
			udpPacket.setDestinationPort((short)520);
			udpPacket.setSourcePort((short)520);
			udpPacket.resetChecksum();
			udpPacket.serialize();
			ipPacket.setPayload(udpPacket);
			//ipPacket.setDestinationAddress(i.getIpAddress());
			ipPacket.setTtl((byte)16);
			ipPacket.setProtocol(IPv4.PROTOCOL_UDP);
			//old
//			ipPacket.setDestinationAddress(RIP_IP);
//			ipPacket.setSourceAddress(inIface.getIpAddress());
			ipPacket.setDestinationAddress(etherToIP.getSourceAddress());
			ipPacket.setSourceAddress(inIface.getIpAddress());
			//ipPacket.set

			ipPacket.resetChecksum();
			ipPacket.serialize();
			response.setPayload(ipPacket);
			response.setDestinationMACAddress(etherPacket.getSourceMACAddress());
			response.setSourceMACAddress(inIface.getMacAddress().toBytes());
			response.setEtherType(Ethernet.TYPE_IPv4);
			response.serialize();
			//sending responses
			this.sendPacket(response, inIface);
		} else if(ripPacket.getCommand() == RIPv2.COMMAND_RESPONSE){
			//update table
			for(RIPv2Entry r: ripPacket.getEntries()) {
				if(this.ripTable.containsKey(r.getAddress())) {
					if(this.ripTable.get(r.getAddress()).getMetric() > r.getMetric()) {
						if(this.ripTable.get(r.getAddress()).getNextHopAddress() != etherToIP.getSourceAddress())
							this.ripTable.get(r.getAddress()).setMetric(r.getMetric()+1);
						this.ripTable.get(r.getAddress()).setNextHopAddress(etherToIP.getSourceAddress());
						this.timeoutTable.put(r.getAddress(), System.currentTimeMillis());
						this.routeTable.update(r.getAddress(), etherToIP.getSourceAddress(), r.getSubnetMask(), inIface);
						//maybe update otherstuff too
						//TODO
					}
				} else {
					entryToTable = new RIPv2Entry(r.getAddress(), r.getSubnetMask(), r.getMetric());
					if(etherToIP.getSourceAddress() != 0)
						entryToTable.setMetric(entryToTable.getMetric()+1);
					entryToTable.setNextHopAddress(etherToIP.getSourceAddress());
					this.ripTable.put(r.getAddress(), entryToTable);
					this.timeoutTable.put(r.getAddress(), System.currentTimeMillis());
					//this is probably wrong
					this.routeTable.insert(r.getAddress(), etherToIP.getSourceAddress(), r.getSubnetMask(), inIface);
				}
			}
		} else {
			return;
		}
	}

	private void handleIpPacket(Ethernet etherPacket, Iface inIface)
	{
		// Make sure it's an IP packet
		if (etherPacket.getEtherType() != Ethernet.TYPE_IPv4)
		{ return; }
		
		// Get IP header
		IPv4 ipPacket = (IPv4)etherPacket.getPayload();
        System.out.println("Handle IP packet");

        // Verify checksum
        short origCksum = ipPacket.getChecksum();
        ipPacket.resetChecksum();
        byte[] serialized = ipPacket.serialize();
        ipPacket.deserialize(serialized, 0, serialized.length);
        short calcCksum = ipPacket.getChecksum();
        if (origCksum != calcCksum)
        { return; }
        
        // Check TTL
        ipPacket.setTtl((byte)(ipPacket.getTtl()-1));
        if (0 == ipPacket.getTtl())
        { return; }
        
        // Reset checksum now that TTL is decremented
        ipPacket.resetChecksum();
        
        // Check if packet is destined for one of router's interfaces
        for (Iface iface : this.interfaces.values())
        {
        	if (ipPacket.getDestinationAddress() == iface.getIpAddress())
        	{ return; }
        }
		
        // Do route lookup and forward
        this.forwardIpPacket(etherPacket, inIface);
	}

    private void forwardIpPacket(Ethernet etherPacket, Iface inIface)
    {
        // Make sure it's an IP packet
		if (etherPacket.getEtherType() != Ethernet.TYPE_IPv4)
		{ return; }
        System.out.println("Forward IP packet");
		
		// Get IP header
		IPv4 ipPacket = (IPv4)etherPacket.getPayload();
        int dstAddr = ipPacket.getDestinationAddress();

        // Find matching route table entry 
        RouteEntry bestMatch = this.routeTable.lookup(dstAddr);

        // If no entry matched, do nothing
        if (null == bestMatch)
        { return; }

        // Make sure we don't sent a packet back out the interface it came in
        Iface outIface = bestMatch.getInterface();
        if (outIface == inIface)
        { return; }

        // Set source MAC address in Ethernet header
        etherPacket.setSourceMACAddress(outIface.getMacAddress().toBytes());

        // If no gateway, then nextHop is IP destination
        int nextHop = bestMatch.getGatewayAddress();
        if (0 == nextHop)
        { nextHop = dstAddr; }

        // Set destination MAC address in Ethernet header
        ArpEntry arpEntry = this.arpCache.lookup(nextHop);
        if (null == arpEntry)
        { return; }
        etherPacket.setDestinationMACAddress(arpEntry.getMac().toBytes());
        
        this.sendPacket(etherPacket, outIface);
    }

    public void populateTable() {
		//time = System.currentTimeMillis();
		//System.out.println("Populate Table");
		this.routeTable = new RouteTable();
		this.ripTable = new HashMap<Integer, RIPv2Entry>();
		for(Iface i : this.interfaces.values()) {
			this.routeTable.insert(i.getIpAddress(), 0, i.getSubnetMask(), i);
		}
		for(Iface i : this.interfaces.values()) {
			this.ripTable.put(i.getIpAddress(), new RIPv2Entry(i.getIpAddress(), i.getSubnetMask(), 0));
		}
		for(Iface i: this.interfaces.values()) {
			this.timeoutTable.put(i.getIpAddress(), System.currentTimeMillis());
		}
	}

	public void unsolicitedResponse() {
		//flood system with responses
		RIPv2 ripResponse = new RIPv2();
		int RIP_IP = IPv4.toIPv4Address("224.0.0.9");
		byte[] RIP_MAC = Ethernet.toMACAddress("FF:FF:FF:FF:FF:FF");
		//System.out.println("Sending Unsolicited Response(s)");
		Ethernet etherPacket = new Ethernet();
		IPv4 ipPacket = new IPv4();
		UDP udpPacket = new UDP();
		List<RIPv2Entry> responseTable = new ArrayList<RIPv2Entry>();
		for(RIPv2Entry r: this.ripTable.values()) {
			responseTable.add(r);
		}
		for(Iface i: this.interfaces.values()) {
			ripResponse.setCommand(RIPv2.COMMAND_RESPONSE);
			ripResponse.setEntries(responseTable);
			udpPacket.setPayload(ripResponse);
			udpPacket.setDestinationPort((short)520);
			udpPacket.setSourcePort((short)520);
			udpPacket.resetChecksum();
			udpPacket.serialize();
			ipPacket.setPayload(udpPacket);
			//ipPacket.setDestinationAddress(i.getIpAddress());
			ipPacket.setTtl((byte)16);
			ipPacket.setProtocol(IPv4.PROTOCOL_UDP);
			ipPacket.setDestinationAddress(RIP_IP);
			ipPacket.setSourceAddress(i.getIpAddress());
			//ipPacket.set

			ipPacket.resetChecksum();
			ipPacket.serialize();
			etherPacket.setPayload(ipPacket);
			etherPacket.setDestinationMACAddress(RIP_MAC);
			etherPacket.setSourceMACAddress(RIP_MAC);
			etherPacket.setEtherType(Ethernet.TYPE_IPv4);
			etherPacket.serialize();
			this.sendPacket(etherPacket, i);
		}
	}

	public void refreshTable() {
		//look through table and determine if something has been there for more than 30 secs. and is not immediate interface


					boolean found = false;
					ArrayList<Integer> remove = new ArrayList<Integer>();
					//Iterator<Map.Entry<Integer, Long>> itr = timeoutTable.entrySet().iterator();
					//Map.Entry<Integer, Long> l;
					for (Map.Entry<Integer, Long> l : timeoutTable.entrySet()){
					//while (itr.hasNext()) {
						//l = itr.next();
						if (System.currentTimeMillis() - l.getValue() > 30000) {
							if(routeTable.lookup(l.getKey()).getGatewayAddress() != 0)
								remove.add(l.getKey());

							//for (Iface i : this.interfaces.values()) {
							//	if (i.getIpAddress() == l.getKey())
//									found = true;
//							//}
//							if (!found) {
//							}
						}
						//found = false;
					}
		synchronized(this.timeoutTable) {
			synchronized (this.routeTable) {
				synchronized (this.ripTable) {
					for(Integer i: remove) {
						timeoutTable.remove(i);
						routeTable.remove(i, ripTable.get(i).getSubnetMask());
						ripTable.remove(i);
					}
				}
			}
		}
	}

	public void printTables() {
		synchronized (this.routeTable) {
			System.out.println("The Route Table: \n " + this.routeTable.toString());
		}
		synchronized (this.ripTable) {
			System.out.println("The RIP Table: \n" + this.ripTable.toString());
		}
	}
}
