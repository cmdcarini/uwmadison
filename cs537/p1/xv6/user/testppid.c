#include "user.h"
#include "stat.h"
#include "types.h"

int
main()
{
	int cpid;
	int pid;
	int ppid;

	pid = getpid();
	cpid = fork();

	if(cpid == 0){
		ppid = getppid();
		if(ppid == pid){
			printf(1, "SUCCESS\n");
		} else { 
			printf(1, "FAILURE\n");
		}
	} else { 
		while((cpid = wait()) > 0 ); 
	}
	exit();
}
