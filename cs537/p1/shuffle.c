///////////////////////////////////////////////////////////////////////////////
// Main File:        shuffle.c
// This File:        shuffle.c
//
// Semester:         CS 537 Fall 2017
//
// Author:           Marco Carini
// Email:            carini@cs.wisc.edu
// CS Login:         carini
//
////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

int numStrings = 0;
int existsEmpty = 0;

char **populateStringArray(int fileSize, char * inputStream){
  char **result = malloc(fileSize*sizeof(char*));
  if(result == NULL){
    fprintf(stderr, "Failure to allocate memory\n");
    exit(1);
  }

  char *token;
  token = strtok(inputStream, "\n");
  int curr = 0;

  if(token == NULL){
    existsEmpty = 1;
    for(int i = 0; i < fileSize; i++){
      result[curr] = malloc(2);
      if(result[curr] == NULL){
        fprintf(stderr, "Failure to allocate memory\n");
        exit(1);
      }
      strcpy(result[curr], "\n");
      curr++;
      numStrings++;
    }

  }

  while(token != NULL){
    if(token!= NULL && curr == 0){
      result[curr] = malloc(strlen(token) + 2);
      if(result[curr] == NULL){
        fprintf(stderr, "Failure to allocate memory\n");
        exit(1);
      }
      strcpy(result[curr], token);
      strcat(result[curr], "\n");
      curr++;
    }
    token = strtok(NULL, "\n");
    if(token != NULL){
      result[curr] = malloc(strlen(token) + 2);
      if(result[curr] == NULL){
        fprintf(stderr, "Failure to allocate memory\n");
        exit(1);
      }
      strcpy(result[curr],token);
      strcat(result[curr], "\n");
    }
    if(token != NULL)
      curr = curr +1;
    numStrings ++;
  }
  return result;
}

int main(int argc, char *argv[]){
  char* inputFileName;
  char* outputFileName;
  if(argc != 5){
    fprintf(stderr, "Usage: shuffle -i inputfile -o outputfile\n");
    exit(1);
  }

  opterr = 0;
  int c;
  while((c = getopt (argc, argv, "i:o:")) != -1)
  switch (c) {
    case 'i':
    inputFileName = optarg;
    break;
    case 'o':
    outputFileName = optarg;
    break;
    case '?':
    fprintf(stderr, "Usage: shuffle -i inputfile -o outputfile\n");
    exit(1);
    default:
    fprintf(stderr, "Usage: shuffle -i inputfile -o outputfile\n");
    exit(1);
  }

  FILE *input;
  if(inputFileName == NULL){
    fprintf(stderr, "Error: Cannot open file\n");
    exit(1);
  }
  input = fopen(inputFileName, "r");
  if(input == NULL){
    fprintf(stderr, "Error: Cannot open file %s\n", inputFileName);
    exit(1);
  }

  struct stat buffer;
  int canParse = stat(inputFileName, &buffer);
  if(canParse == -1){
    fprintf(stderr, "Error %d, unable to parse %s", errno, inputFileName);
  }

  char *testFile = malloc(buffer.st_size*sizeof(char*));
  if(testFile == NULL){
    fprintf(stderr, "Failure to allocate memory\n");
    exit(1);
  }

  int readIn = fread(testFile, buffer.st_size, 1, input);
  if(readIn == -1){
    fprintf(stderr, "Error reading input file\n");
    exit(1);
  }

  char **lineArray = populateStringArray(buffer.st_size, testFile);
  int x = 0;
  int y = numStrings-1;
  FILE *output;
  if(outputFileName == NULL){
    fprintf(stderr, "Error opening outputfile\n");
    exit(1);
  }
  output = fopen(outputFileName, "w");

  if(output == NULL){
    fprintf(stderr, "Error: Cannot open file %s\n", outputFileName);

  }

  for(int i = 0; i < buffer.st_size; i++){
    if(y < x)
      break;

    if(x == y){
      fwrite(lineArray[x], 1, strlen(lineArray[x]), output);
      break;
    }
    if(x > buffer.st_size || y < 0)
      break;

    if(lineArray[x] != NULL)
      fwrite(lineArray[x], 1, strlen(lineArray[x]), output);


    if(lineArray[y] != NULL)
      fwrite(lineArray[y], 1, strlen(lineArray[y]), output);

    x++;
    y--;
  }

  if(existsEmpty){
    for(int i = 0; i < buffer.st_size;i++)
      free(lineArray[i]);
    free(lineArray);
    free(testFile);
    return (0);
  }

  int numElem = sizeof(lineArray)/sizeof(char*);
  for(int i = 0; i < numElem; i++)
    free(lineArray[i]);
  free(lineArray);
  free(testFile);
  return(0);
}
