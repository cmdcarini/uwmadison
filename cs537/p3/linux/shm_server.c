#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>
#define SHM_NAME "carini"
#define PAGESIZE 4096


pthread_mutex_t* mutex;
pthread_mutexattr_t mutexAttribute;
typedef struct {
		int valid;
    int pid;
    char birth[25];
    char clientString[10];
    int elapsed_sec;
    double elapsed_msec;
} stats_t;

void* shm_ptr;

void exit_handler(int sig) {
	int maxClients = 63;
	stats_t stats;

	for(int i = 0; i < maxClients; i++){
		memcpy(&stats, shm_ptr + sizeof(pthread_mutex_t) + sizeof(stats_t)*i,sizeof(stats_t));
		if(stats.valid){
			stats.valid = 0;
			kill(stats.pid, SIGTERM);
		}
	}
	munmap(shm_ptr, PAGESIZE);
	shm_unlink(SHM_NAME);
	exit(0);
}

int main(int argc, char *argv[]) {
	struct sigaction act;
	act.sa_handler = exit_handler;
	if (sigaction(SIGINT, &act, NULL) == -1) exit(1);
	if (sigaction(SIGTERM, &act, NULL) == -1) exit(1);
	int maxClients = 63;

	int fd_shm = shm_open(SHM_NAME, O_RDWR | O_CREAT, 0660);
	if(fd_shm < 0) exit(1);

	if(ftruncate(fd_shm, PAGESIZE) < 0) exit(1);

	shm_ptr = mmap(NULL, PAGESIZE, PROT_READ, MAP_SHARED, fd_shm, 0);
	if(shm_ptr < 0) exit(1);

	mutex = malloc(sizeof(pthread_mutex_t));
	pthread_mutexattr_init(&mutexAttribute);
	pthread_mutexattr_setpshared(&mutexAttribute, PTHREAD_PROCESS_SHARED);
	pthread_mutex_init(mutex, &mutexAttribute);
	mutex=shm_ptr;
	int itno = 1;
	while (1) {
		stats_t stats;

		for(int i = 0 ; i < maxClients; i++){
			memcpy(&stats,(shm_ptr+ sizeof(pthread_mutex_t) + (sizeof(stats_t)*i)), sizeof(stats_t));
			if(stats.valid == 1)
				printf("%d, pid : %d, birth : %s, elapsed : %d s %.4f ms, %s\n" , itno, stats.pid, stats.birth, stats.elapsed_sec, stats.elapsed_msec, stats.clientString);
		}
		itno++;
		sleep(1);
	}
	return 0;
}
