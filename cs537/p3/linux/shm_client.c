#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <sys/types.h>
#include <string.h>
#include <time.h>
#include <signal.h>

#define SHM_NAME "carini"
#define PAGESIZE 4096


typedef struct {
  int valid;
  int pid;
  char birth[25];
  char clientString[10];
  int elapsed_sec;
  double elapsed_msec;
} stats_t;

stats_t* stats_ptr;
pthread_mutex_t* mutex;
void* shm_stats;

void exit_handler(int signal) {
  // critical section begins
  stats_ptr = shm_stats;
  stats_ptr->valid = 0;
  pthread_mutex_lock(mutex);
  memcpy(shm_stats, stats_ptr, sizeof(stats_t));
  pthread_mutex_unlock(mutex);
  // critical section ends
  exit(0);
}

int main(int argc, char *argv[]) {

  //check argcount
  struct sigaction act;
  act.sa_handler = exit_handler;
  if (sigaction(SIGINT, &act, NULL) < 0) exit(1);
  if (sigaction(SIGTERM, &act, NULL) < 0) exit(1);
  if(argc != 2)
    exit(1);
  if(strlen(argv[1]) > 10)
    exit(1);

  int fd_shm = shm_open(SHM_NAME, O_RDWR, 0660);
  if(fd_shm < 0){
    exit(1);
  }

  void *shm_ptr = mmap(NULL, PAGESIZE, PROT_WRITE | PROT_READ, MAP_SHARED, fd_shm, 0);
  if(shm_ptr < 0){
    exit(1);
  }
  int found = 0;
  mutex = shm_ptr;
  stats_t stats;
  stats_ptr = &stats;
  int maxClients = 63;
  time_t currTime;
  struct tm *ts;
  struct timespec start, now;
  clock_gettime(CLOCK_MONOTONIC_RAW, &start);
  stats.valid = 1;
  currTime = time(NULL);
  ts = localtime(&currTime);
  strftime(stats.birth, 25, "%a %b %d %H:%M:%S %Y", ts);
  stats.pid = getpid();
  strcpy(stats.clientString, argv[1]);
  stats_t tmp;
  shm_stats = shm_ptr + sizeof(pthread_mutex_t);

  for(int i = 0; i < maxClients; i++){
    memcpy(&tmp, shm_stats + sizeof(stats_t)*i, sizeof(stats_t));
    if(tmp.valid == 0){
      found = 1;
      shm_stats = shm_stats + sizeof(stats_t)*i;
      break;
    }
  }

  if(found == 0){
    exit(1);
  }
  //pthread_mutex_lock(mutex);
  memcpy(shm_stats, &stats, sizeof(stats_t));
  //pthread_mutex_unlock(mutex);

  stats_t tmp_print;
  while (1) {
    clock_gettime(CLOCK_MONOTONIC_RAW, &now);
    stats.elapsed_sec = (int)(now.tv_sec - start.tv_sec);
    stats.elapsed_msec = (double)((double)(now.tv_nsec - start.tv_nsec)/1000000);

    memcpy(shm_stats, &stats, sizeof(stats_t));

    sleep(1);

    printf("Active clients : ");
    for(int i = 0; i < maxClients; i++){
      memcpy(&tmp_print, shm_ptr + sizeof(pthread_mutex_t) + sizeof(stats_t)*i, sizeof(stats_t));
      if(tmp_print.valid){
        printf("%d ", tmp_print.pid);
      }
    }
    printf("\n");
  }
  return 0;
}
