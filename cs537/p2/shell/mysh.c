#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <wait.h>

int main(int argc, char* argv[]){

  //initialize variables
  int cmdHistCount = 1;
  int numProc = 0;
  int done = 0;
  pid_t* runProc = malloc(sizeof(20*sizeof(pid_t)));

  //if mysh has args, kill itself
  if(argc > 1){
    char error_message[30] = "An error has occurred\n";
    write(STDERR_FILENO, error_message, strlen(error_message));
    exit(1);
  }

  //infinite loop
  do {
    size_t buffSize = 0;
    int hasPipe = 0;
    int hasFileDirectionIn = 0;
    int hasFileDirectionOut = 0;
    int pipeIndex = 0;
    int directIndexIn = 0;
    int directIndexOut = 0;
    int hasAnd = 0;
    int andIndex = 0;

    //print shell text
    fflush(stdout);
    printf("%s (%d)> ","mysh", cmdHistCount);
    fflush(stdout);

    //get input cmd
    char* input;
    //error check this shit out of this

    ssize_t lineLen = getline(&input, &buffSize, stdin);
    while(lineLen > 128){
      char error_message[30] = "An error has occurred\n";
      write(STDERR_FILENO, error_message, strlen(error_message));
      fflush(stdout);
      cmdHistCount++;
      printf("%s (%d)> ","mysh", cmdHistCount);
      fflush(stdout);
      lineLen = getline(&input, &buffSize, stdin);

    }
    //parse for cmd
    int lineSize = 128;
    char **tokens = malloc(lineSize/** sizeof(char*)*/);
    char *token;
    token = strtok(input, " \t\r\n\a");

    int argCount = 0;
    while(token != NULL){
      tokens[argCount] = token;
      argCount++;
      token = strtok(NULL, " \t\r\n\a");
    }
    tokens[argCount] = NULL;

    if(argCount > 0 && (strcmp(tokens[0], "exit") == 0)){
      done = 1;
    }

    int builtIn = 0;
    if(argCount > 0 && done != 1 && strcmp(tokens[0], "cd") == 0){
      builtIn = 1;
      if(tokens[1] == NULL){
        if(chdir(getenv("HOME")) != 0){
          char error_message[30] = "An error has occurred\n";
          write(STDERR_FILENO, error_message, strlen(error_message));
        }
      }
      else {
        if(chdir(tokens[1]) != 0){
          if(argCount > 2){
            char error_message[30] = "An error has occurred\n";
            write(STDERR_FILENO, error_message, strlen(error_message));

          }
          else{
          char error_message[30] = "An error has occurred\n";
          write(STDERR_FILENO, error_message, strlen(error_message));
        }
        }
      }
    }

    int s = 128;
    if(argCount > 0 && done != 1 && strcmp(tokens[0], "pwd") ==0){
      builtIn = 1;
      if(argCount > 1){
        char error_message[30] = "An error has occurred\n";
        write(STDERR_FILENO, error_message, strlen(error_message));
      } else{
        if(getcwd(input, s) == NULL){
          char error_message[30] = "An error has occurred\n";
          write(STDERR_FILENO, error_message, strlen(error_message));
        }
        fprintf(stdout, "%s\n", input);

      }
    }

    if(argCount > 0 && done != 1){
      //detect | or </>
      for(int i = 0; i < argCount; i++){
        if(strcmp(tokens[i], "&") == 0){
          hasAnd = 1;
          andIndex = i;
        }
        if(strcmp(tokens[i], "|") == 0){
          hasPipe = 1;
          pipeIndex = i;
        }
        if(strcmp(tokens[i], "<") == 0){
          hasFileDirectionIn = 1;
          directIndexIn = i;
        }
        if(strcmp(tokens[i], ">") == 0){
          hasFileDirectionOut = 1;
          directIndexOut = i;
        }
      }
      char* inputFileName = tokens[directIndexIn + 1];
      char* outputFileName = tokens[directIndexOut + 1];


//      //eliminates the rest of the cmd, after storing file name
  //     if(hasFileDirectionOut || hasFileDirectionIn){
    //      if(hasFileDirectionOut ){
  //          for(int i = directIndexOut; i < argCount; i++){
      //       tokens[i] = NULL;
     //       }
     //     } else if (hasFileDirectionIn){
     //       for(int i = directIndexIn; i < argCount; i++){
     //         tokens[i] = NULL;
     //       }
     //     }
     //  }
      if(hasAnd){
        for(int i = andIndex; i < argCount; i++){
          tokens[i] = NULL;
        }
      }
      if(!builtIn){
      //execute cmd
      pid_t pid;
      int status;
      pid = fork();

      if(pid == 0){
        if(!hasPipe && !hasFileDirectionOut && !hasFileDirectionIn){
          if(execvp(tokens[0], tokens)== -1){
            char error_message[30] = "An error has occurred\n";
            write(STDERR_FILENO, error_message, strlen(error_message));
            done = 1;
          }
        } else if(hasPipe){
          //handle pipe
          pid_t fork1;
          pid_t fork2;
          int magicPipeArr[2];
          char** instrs = malloc((argCount - pipeIndex - 1)*sizeof(char**));
          instrs[0] = tokens[pipeIndex + 1];
          if(instrs[0] == NULL){
            char error_message[30] = "An error has occurred\n";
            write(STDERR_FILENO, error_message, strlen(error_message));
            exit(1);
          }

          if(pipe(magicPipeArr) == -1){
            char error_message[30] = "An error has occurred\n";
            write(STDERR_FILENO, error_message, strlen(error_message));
            exit(1);
          }

          //fork attempt 1
          fork1 = fork();
          if(!fork1){
            close(magicPipeArr[1]);
            dup2(magicPipeArr[0], STDIN_FILENO);
            if(execvp(instrs[0], instrs) == -1){
              char error_message[30] = "An error has occurred\n";
              write(STDERR_FILENO, error_message, strlen(error_message));
              exit(1);
            }
          } else{
            //fork attempt 2
            fork2 = fork();
            if(!fork2){
              close(magicPipeArr[0]);
              dup2(magicPipeArr[1], STDOUT_FILENO);
              tokens[pipeIndex] = NULL;
              if(execvp(tokens[0], tokens) == -1){
                char error_message[30] = "An error has occurred\n";
                write(STDERR_FILENO, error_message, strlen(error_message));
                exit(1);
              }
              close(magicPipeArr[1]);
            }
            close(magicPipeArr[0]);
            close(magicPipeArr[1]);

            wait(NULL);
            wait(NULL);
          }

       //   exit(1);

        } else if(hasFileDirectionIn && !hasFileDirectionOut && !hasPipe){

          int file = open(inputFileName, O_RDONLY, S_IRWXU);
          if(tokens[directIndexIn + 2] != NULL || file < 0){
            char error_message[30] = "An error has occurred\n";
            write(STDERR_FILENO, error_message, strlen(error_message));
            free(tokens);
            return 1;
          }
          dup2(file, STDIN_FILENO);
          tokens[directIndexIn] = NULL;
          close(file);

          if(execvp(tokens[0], tokens)== -1){
            char error_message[30] = "An error has occurred\n";
            write(STDERR_FILENO, error_message, strlen(error_message));
            free(tokens);
            return 1;
          }
        } else if(hasFileDirectionOut && !hasFileDirectionIn && !hasPipe){
          //handle file direct out
          int file = open(outputFileName, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);
          if(tokens[directIndexOut + 2] != NULL || file < 0){
            char error_message[30] = "An error has occurred\n";
            write(STDERR_FILENO, error_message, strlen(error_message));
            free(tokens);
            return 1;
          }
          dup2(file, STDOUT_FILENO);
          tokens[directIndexOut] = NULL;
          close(file);

          if(execvp(tokens[0], tokens) == -1){
            char error_message[30] = "An error has occurred\n";
            write(STDERR_FILENO, error_message, strlen(error_message));
            free(tokens);
            return 1;
          }
        } else if(hasFileDirectionIn && hasFileDirectionOut && !hasPipe){
          int file_in = open(inputFileName, O_RDONLY, S_IRWXU);
          if(file_in < 0){
            char error_message[30] = "An error has occurred\n";
            write(STDERR_FILENO, error_message, strlen(error_message));
            free(tokens);
            return 1;
          }
          dup2(file_in, STDIN_FILENO);

          int file_out = open(outputFileName, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);
          if(file_out < 0){
            char error_message[30] = "An error has occurred\n";
            write(STDERR_FILENO, error_message, strlen(error_message));
            free(tokens);
            return 1;
          }
          dup2(file_out, STDOUT_FILENO);
          tokens[directIndexOut] = NULL;
          tokens[directIndexIn] = NULL;
          if(execvp(tokens[0], tokens) == -1){
            char error_message[30] = "An error has occurred\n";
            write(STDERR_FILENO, error_message, strlen(error_message));
            free(tokens);
            return 1;
          }
        }

      } else {
        do{
          if(numProc < 20){
            runProc[numProc] = pid;
            numProc++;
          } else {
            numProc = 0;
            kill(runProc[numProc], SIGINT);
            runProc[numProc] = pid;
            numProc++;
          }
          if(!hasAnd)
            waitpid(pid, &status, WUNTRACED);
          else
            break;

          for(int i = 0; i < numProc; i++){
            if(!kill(runProc[i], 0))
              kill(runProc[i], SIGINT);
          }
        } while (!WIFEXITED(status) && !WIFSIGNALED(status));
      }
    }
  }
    if(argCount > 0)
      cmdHistCount++;
    if(done == 1){
      for(int i = 0; i < numProc; i ++){
        if(!kill(runProc[i], 0))
          kill(runProc[i], SIGINT);
      }
    }
	fflush(stdout);
        free(input);
      free(tokens);
  } while(done < 1);

  return 0;
}
