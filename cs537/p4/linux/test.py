import requests
import multiprocessing

BASE_URI = 'http://jarvis.cs.wisc.edu:8080/home.html'

def internet_resource_getter(request_count):
    stuff_got = []

    for i in request_count:
        response = requests.get(BASE_URI)
        stuff_got.append(response.text)

    return stuff_got

request_count = [0] * 1000

for i in range(0, len(request_count)):
    request_count[i] = 'a'

pool = multiprocessing.Pool(processes=len(request_count))
pool_outputs = pool.map(internet_resource_getter, request_count)

pool.close()
pool.join()
#print pool_outputs
print len(pool_outputs)
