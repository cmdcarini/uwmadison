#include "cs537.h"
#include "request.h"

typedef struct {
  int valid;
  int taken;
  int connfd;
} stats_t;
stats_t **buffer;
pthread_cond_t empty, full;
pthread_mutex_t mutex;
int count;
int buffSize;

void getargs(int *port, int *threads, int *buffers, int argc, char *argv[])
{
  if (argc != 4) {
    fprintf(stderr, "Usage: %s <port> <threads> <buffers>\n", argv[0]);
    exit(1);
  }
  *port = atoi(argv[1]);
  *threads = atoi(argv[2]);
  *buffers = atoi(argv[3]);
}

void *worker(void){
  int connfd;
  while(1){
    pthread_mutex_lock(&mutex);
    while(count == 0){
      pthread_cond_wait(&full, &mutex);
    }
    for(int i = 0; i < buffSize; i++){
      if(buffer[i]->valid == 1 && buffer[i]->taken == 0){
        buffer[i]->taken = 1;
        fflush(stdout);
        connfd = buffer[i]->connfd;
        buffer[i]->valid = 0;
        buffer[i]->taken = 0;
        break;
      }
    }
    count--;
    pthread_cond_signal(&empty);
    pthread_mutex_unlock(&mutex);
    requestHandle(connfd);
    Close(connfd);
  }
  return 0;
}

int main(int argc, char *argv[])
{
  int listenfd, connfd, port, clientlen, numThreads, buffNum;
  struct sockaddr_in clientaddr;
  pthread_mutexattr_t mutexAttribute;
  pthread_mutexattr_init(&mutexAttribute);
  pthread_mutexattr_setpshared(&mutexAttribute, PTHREAD_PROCESS_SHARED);
  pthread_mutex_init(&mutex, &mutexAttribute);
  pthread_cond_init(&empty, NULL);
  pthread_cond_init(&full, NULL);
  getargs(&port, &numThreads, &buffNum, argc, argv);
  if(port < 0 || numThreads < 0 || buffNum < 0){
    printf("there was an error");
    exit(1);
  }
  buffSize  = buffNum;
  pthread_t threads[numThreads];
  if((buffer = malloc(sizeof(stats_t*)*buffSize))<0){
    printf("there was an error");
    exit(1);
  }
  for(int i = 0; i < buffSize; i++){
    if((buffer[i] = malloc(sizeof(stats_t)))<0){
      printf("there was an error");
      exit(1);
    }
    buffer[i]->valid = 0;
    buffer[i]->taken = 0;
    buffer[i]->connfd = 0;
  }
  for(int i = 0; i < numThreads; i++){
    if(pthread_create(&threads[i], NULL, (void*)worker, NULL) < 0){
      printf("there was an error");
      exit(1);
    }
  }
  listenfd = Open_listenfd(port);
  while (1) {
    clientlen = sizeof(clientaddr);
    connfd = Accept(listenfd, (SA *)&clientaddr, (socklen_t *) &clientlen);
    if(connfd < 0){
      printf("there was an error");
      exit(1);
    }
    pthread_mutex_lock(&mutex);
    while(count == buffSize){
      pthread_cond_wait(&empty, &mutex);
    }
    for(int i = 0; i < buffSize; i++){
      if(buffer[i]->valid == 0){
        buffer[i]->valid = 1;
        buffer[i]->taken = 0;
        buffer[i]->connfd = connfd;
        count++;
        fflush(stdout);
        break;
      }
    }
    pthread_cond_signal(&full);
    pthread_mutex_unlock(&mutex);
  }
}
