#ifndef _TYPES_H_
#define _TYPES_H_

// Type definitions


typedef unsigned int   uint;
typedef uint lock_t;
typedef unsigned short ushort;
typedef unsigned char  uchar;
typedef uint pde_t;
typedef struct {
  int head;
  int tail;
  void* procs[8];
  lock_t lock;
} cond_t;
#ifndef NULL
#define NULL (0)
#endif

#endif //_TYPES_H_
