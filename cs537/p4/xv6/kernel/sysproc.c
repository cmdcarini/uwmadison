#include "types.h"
#include "x86.h"
#include "defs.h"
#include "param.h"
#include "mmu.h"
#include "proc.h"
#include "sysfunc.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return proc->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = proc->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(proc->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since boot.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

int
sys_clone(void)
{
  int func, funcArg, stack;
  if(argint(0, &func) < 0) return -1;
  if(argint(1, &funcArg) < 0) return -1;
  if(argint(2, &stack) < 0) return -1;
  return clone((void*)func, (void*)funcArg, (void*)stack);
}

int
sys_join(void)
{
  int stack;
  if(argint(0, &stack) < 0) return -1;
  return join((void**)stack);
}

int
sys_cond_wait(void){
  int cond, lock;
  if(argint(0, &cond) < 0) return -1;
  if(argint(1, &lock) < 0) return -1;
  return cond_wait((cond_t *)cond, (lock_t *)lock);
}

int
sys_cond_signal(void)
{
  int cond;
  if(argint(0, &cond) < 0) return -1;
  return cond_signal((cond_t*)cond);
}

int
sys_cond_init(void)
{
  int cond;
  if(argint(0, &cond) < 0) return -1;
  return cond_init((cond_t*)cond);
}
