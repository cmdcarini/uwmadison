#include "types.h"
#include "stat.h"
#include "fcntl.h"
#include "user.h"
#include "x86.h"

int
thread_create(void (*start_routine)(void*), void *arg){
  void* stack;
  if((stack = malloc(4096)) < 0){
    printf(1, "Malloc failed");
    exit();
  }
  return clone((void*)start_routine, (void*)arg, (void*)stack);
}

int
thread_join(){
  void* userstack;
  int result;
  if((result = join(&userstack)) > 0) free(userstack);
  return result;
}

void
lock_acquire(lock_t *lock){
  while(xchg(lock, 1) == 1) ;
}

void
lock_release(lock_t *lock){
  *lock = 0;
}

void
lock_init(lock_t *lock){
  *lock = 0;
}

int
condwait(cond_t *cond, lock_t *lock){
  return cond_wait(cond, lock);
}

int
condsignal(cond_t *cond){
  return cond_signal(cond);
}

int
condinit(cond_t *cond){
  return cond_init(cond);
}
