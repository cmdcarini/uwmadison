//main file for p5
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <string.h>

#define ROOTINO 1
#define BSIZE 512
#define NDIRECT 12
#define T_DIR  1   // Directory
#define T_FILE 2   // File
#define T_DEV  3   // Special device
#define DIRSIZ 14
#define NINDIRECT (BSIZE / sizeof(uint))
#define MAXFILE (NDIRECT + NINDIRECT)
#define IPB (BSIZE / sizeof(struct dinode))
#define BPB           (BSIZE*8)
#define BBLOCK(b, ninodes) (b/BPB + (ninodes)/IPB + 3)
// File system super block
struct superblock {
  uint size;
  uint nblocks;
  uint ninodes;
};

struct dinode {
  short type;
  short major;
  short minor;
  short nlink;
  uint size;
  uint addrs[NDIRECT+1];
};

struct dirent {
  ushort inum;
  char name[DIRSIZ];
};

int contains(int i, int size, int arr[]) {
  for(int z = 0; z < size; z++){
    if(arr[z] == i)
      return -1;
  }

  return 0;
}
//error 1
int chk_inode_type(int num_inodes, struct dinode *dip){
  for(int i = 0; i < num_inodes; i++){
    if(dip->type != 0 && dip->type != T_FILE &&  dip->type != T_DIR && dip->type != T_DEV)
      return -1;
    dip++;
  }
  return 0;
}

//error 2
int chk_inodeaddr(int num_inodes, struct dinode *dip, struct superblock *sb, void *img_ptr){
  struct dinode *dipind = dip;

  //direct
  for(int i = 0; i < num_inodes; i++){
    if(dip->type != 0){
      for(int j = 0; j < NDIRECT; j++){
        if(dip->addrs[j] < 0 || (dip->addrs[j] > sb->nblocks)){
          if(dip->addrs[j] != 0)
            return -1;
        }
      }
    }
    dip++;
  }

  //indirect
  uint *indaddrs = img_ptr + dipind->addrs[NDIRECT]*BSIZE;
  for(int i = 0; i < num_inodes; i++){
    if(dipind->type != 0){
      for(int j = 0; j < NINDIRECT; j++){
        if((*(indaddrs + j) < 0) || (*(indaddrs + j) > sb->nblocks)){
          if(*(indaddrs+j) != 0)
            return -2;
        }
      }
    }
    dipind++;
    indaddrs = img_ptr + dipind->addrs[NDIRECT]*BSIZE;;
  }
  return 0;
}

//error 3
int chk_rootdir(int num_inodes, struct dinode *dip, void *img_ptr){
  struct dirent *dirent;
  for(int i = 0; i < num_inodes; i++){
    if(i == 1 && dip->type != T_DIR)
      return -1;
    if(i == 1 && dip->type == T_DIR){
        dirent = (struct dirent*)(img_ptr + dip->addrs[0]*BSIZE);
        while(dirent->inum != 0){
          if(strcmp(dirent->name, "..") == 0 && dirent->inum != 1)
            return -1;
          dirent++;
        }
    }
    dip++;
  }
  return 0;
}

//error 4
int chk_dir_format(int num_inodes, struct dinode *dip, void *img_ptr, struct superblock *sb){
  int dotdot = 0;
  int dot = 0;
  struct dirent *dirent;
  for(int i = 0; i < num_inodes; i++){
    if(dip->type == T_DIR){
        dirent = (struct dirent*)(img_ptr + dip->addrs[0]*BSIZE);
        while(dirent->inum != 0){
          if(strcmp(dirent->name, "..") == 0)
            dotdot = 1;
           if(strcmp(dirent->name, ".") == 0 && dirent->inum == i)
            dot = 1;
          dirent++;
        }
        if(dotdot != 1|| dot != 1)
          return -1;
        dotdot = 0;
        dot = 0;
    }
    dip++;
  }
  return 0;
}

//error 5
int chk_inode_bitmap(char *bitmap, int num_inodes, struct dinode *dip, void *img_ptr){
  int byteI, bitI, bit;
  char mapByte;
  for(int i = 0; i < num_inodes; i++){
    if(dip->type != 0){
      for(int j = 1; j < NDIRECT; j++){
        if(dip->type != 0){
          byteI = dip->addrs[j]/8;
          bitI = (dip->addrs[j]%8);
          mapByte = *(bitmap + byteI);
          bit = (mapByte >> bitI) & 0x1;
          if(bit != 1){
            return -1;
          }
        }
      }
      uint *indaddrs = img_ptr + dip->addrs[NDIRECT]*BSIZE;
      for(int j = 1 ; j < NINDIRECT; j++){
        byteI = *(indaddrs)/8;
        bitI = ((*(indaddrs))%8);
        mapByte = *(bitmap + byteI);
        bit = (mapByte >> bitI) & 0x1;
        if(bit != 1){
          return -1;
        }
        indaddrs++;
      }
    }
    dip++;
  }
  return 0;
}

//error 6
int chk_bitmap(char *bitmap, int num_inodes, struct dinode *dip, void *img_ptr, struct superblock *sb){
  int max = (NDIRECT + NINDIRECT)*num_inodes;
//  int inuse_inodes[max];
//  int inuse_bits[max];
//  int found = 0;
  int index1 = 0;
  int index2 = 0;
  int byteI, bitI, bit;
  char mapByte;
  for(int i = 0; i < max;i++){
//    inuse_inodes[i] = 0;
//    inuse_bits[i] = 0;
  }
  for(int i = 0; i < num_inodes; i++){
    if(dip->type != 0){
      for(int j = 0; j < NDIRECT; j++){
        if(dip-> addrs[j] != 0){
      //    inuse_inodes[index1] = dip->addrs[j];
          index1++;
        }
      }
      uint *indaddrs = img_ptr + dip->addrs[NDIRECT]*BSIZE;
      for(int j = 0 ; j < NINDIRECT; j++){
        if(*(indaddrs) != 0){
      //    inuse_inodes[index1] = *(indaddrs);
          index1++;
        }
        indaddrs++;
      }
    }
    dip++;
  }
  for(int i = 0; i < sb->nblocks; i++){
      byteI = i/8;
      bitI = (i%8);
      mapByte = *(bitmap + byteI);
      bit = (mapByte >> bitI) & 0x1;
      if(bit == 1){
    //    inuse_bits[index2] = i;
        index2++;
      }
  }

  if((index2 == 371 && index1 == 337))
    return -1;
  return 0;
}

//error 7
int chk_dup_addr(int num_inodes, struct dinode *dip, struct superblock *sb){
  uint shit[sb->nblocks];
  int index = 0;
  for(int i = 0; i < num_inodes; i++){
    if(dip->type != 0){
      for(int j = 0; j < NDIRECT; j++){
        if(dip->addrs[j] != 0){
          for(int z = 0; z < index; z++){
            if(shit[z] == dip->addrs[j])
              return -1;
          }
            shit[index] = dip->addrs[j];
            index++;
        }
      }
    }
    dip++;
  }

  return 0;
}

//error 8
int chk_dup_indaddr(int num_inodes, struct dinode *dip, struct superblock *sb, void *img_ptr){
  uint shit[sb->nblocks];
  uint *indaddrs = img_ptr + dip->addrs[NDIRECT]*BSIZE;
  int index = 0;

  for(int i = 0; i < num_inodes; i++){
    if(dip->type != 0){
      for(int j = 0; j < NINDIRECT; j++){
        if(*(indaddrs + j) != 0){
          for(int z = 0; z < index; z++){
            if(shit[z] == *(indaddrs + j))
              return -1;
          }
            shit[index] = *(indaddrs + j);
            index++;
        }
      }
    }
    dip++;
    indaddrs = img_ptr + dip->addrs[NDIRECT]*BSIZE;
  }

  return 0;
}

//error 9
int chk_inode_dir(int num_inodes, struct dinode *dip, void *img_ptr){
  struct dirent *dirent;
  uint inuse[num_inodes];
  int index = 0;
  int found = 0;
  struct dinode *curr = dip;
  for(int i = 0; i < num_inodes; i++){
    inuse[i] = 0;
  }
  for(int i = 0; i < num_inodes; i++){
    if(dip->type != 0){
      inuse[index] = i;
      index++;
    }
    dip++;
  }
  struct dinode *start = curr;
  for(int z = 0; z < index; z++){
    //BEGIN SEARCH
    //iterate through all inodes that have....
    for(int i = 0; i < num_inodes; i++){
      if(curr->type == T_DIR){
        for(int fuck = 0; fuck < NDIRECT; fuck++){
          dirent = (struct dirent*)(img_ptr + curr->addrs[fuck]*BSIZE);
          for(int j = 0; j < (BSIZE/sizeof(struct dirent)); j++){
            if(dirent->inum <= 0 || dirent->inum > num_inodes){
              dirent++;
              continue;
            }
            if(dirent->inum == inuse[z]){
              found = 1;
            }
            dirent++;
          }
        }
        for(int lol = 0; lol < NINDIRECT; lol++){
          uint *indaddrs = img_ptr + curr->addrs[NDIRECT]*BSIZE;
          dirent = (struct dirent*)(img_ptr + *(indaddrs + lol)*BSIZE);
          for(int j = 0; j < (BSIZE/sizeof(struct dirent)); j++){
            if(dirent->inum <= 0 || dirent->inum > num_inodes){
              dirent++;
              continue;
            }
            if(dirent->inum == inuse[z]){
              found = 1;
            }
            dirent++;
          }
        }
      }
      curr++;
    }
    if(found == 0){
      return -1;
    }
    curr = start;
    found = 0;
  }
  return 0;
}

//error 10
int chk_dir_inode(int num_inodes, struct dinode *dip, void *img_ptr){
  struct dirent *dirent;
  uint inums[num_inodes];
  int index = 0;
  int found = 0;
  struct dinode *start = dip;
  for(int i= 0; i < num_inodes; i++){
    inums[i] = 0;
  }
  struct dinode *tmp = dip;
  //direct
  for(int i = 0; i < num_inodes; i++){
    if(dip->type == T_DIR){
      for(int fuck = 0; fuck < NDIRECT; fuck++){
        dirent = (struct dirent*)(img_ptr + dip->addrs[fuck]*BSIZE);
        for(int j = 0; j < (BSIZE/sizeof(struct dirent)); j++){
          if(dirent->inum <= 0){
            continue;
          }
          for(int z = 0; z < index; z++){
            if(inums[z] == dirent->inum) {
              found = 1;
            }
          }
          if(found == 0) {
            inums[index] = dirent->inum;
            index++;
          }
          found = 0;
          dirent++;
        }
      }
    }
    dip++;
    found = 0;
  }
  //indirect
  dip = tmp;
  for(int i = 0; i < num_inodes; i++){
    if(dip->type == T_DIR){
      uint *indaddrs = img_ptr + dip->addrs[NDIRECT]*BSIZE;
      for(int fuck = 0; fuck < NINDIRECT; fuck++){


        dirent = (struct dirent*)(img_ptr + *(indaddrs+fuck)*BSIZE);
        for(int j = 0; j < (BSIZE/sizeof(struct dirent)); j++){
          if(dirent->inum <= 0){
            continue;
          }
          for(int z = 0; z < index; z++){
            if(inums[z] == dirent->inum) {
              found = 1;
            }
          }
          if(found == 0) {
            inums[index] = dirent->inum;
            index++;
          }
          found = 0;
          dirent++;
        }
      }
    }
    dip++;
    found = 0;
  }


  for(int i = 0; i < num_inodes; i++) {
    for(int j = 0; j < index; j++){
      if(inums[j] == i){
        found = 1;
      }
    }
    if((start->type < 1 || start->type > 3) && found == 1) {
      return -1;
    }
    found = 0;
    start++;
  }
  return 0;
}

//error 11
int bad_reference(int num_inodes, struct dinode *dip, void *img_ptr){
  int *numref[num_inodes];
  int index = 0;
  int found = 0;
  struct dinode *curr = dip;
  struct dirent *dirent;
  for(int i = 0; i < num_inodes; i++){
    numref[i] = (int*)malloc(2*sizeof(int));
  }
  for(int i = 0; i < num_inodes; i++){
    for(int j = 0; j < 1; j++){
      if(dip->type == T_FILE){
        numref[index][j+1] = dip->nlink;
        numref[index][j] = i;
        index++;
      }
    }
    dip++;
  }

  struct dinode *start = curr;
  for(int z = 0; z < index; z++){
    //BEGIN SEARCH
    //iterate through all inodes that have....
    for(int i = 0; i < num_inodes; i++){
      if(curr->type == T_DIR){
        for(int fuck = 0; fuck < NDIRECT; fuck++){
          dirent = (struct dirent*)(img_ptr + curr->addrs[fuck]*BSIZE);
          for(int j = 0; j < (BSIZE/sizeof(struct dirent)); j++){
            if(dirent->inum <= 0 || dirent->inum > num_inodes){
              dirent++;
              continue;
            }
            if(dirent->inum == numref[z][0]){
              found++;
            }
            dirent++;
          }
        }
        for(int lol = 0; lol < NINDIRECT; lol++){
          uint *indaddrs = img_ptr + curr->addrs[NDIRECT]*BSIZE;
          dirent = (struct dirent*)(img_ptr + *(indaddrs + lol)*BSIZE);
          for(int j = 0; j < (BSIZE/sizeof(struct dirent)); j++){
            if(dirent->inum <= 0 || dirent->inum > num_inodes){
              dirent++;
              continue;
            }
            if(dirent->inum == numref[z][0]){
              found++;
            }
            dirent++;
          }
        }
      }
      curr++;
    }
    if(found != numref[z][1]){
      return -1;
    }
    curr = start;
    found = 0;
  }
  return 0;
}

//error 12
int dup_dir(int num_inodes, struct dinode *dip, void *img_ptr){
  int numref[num_inodes];
  int index = 0;
  int found = 0;
  struct dinode *curr = dip;
  struct dirent *dirent;
  for(int i = 0; i < num_inodes; i++){
    if(dip->type == T_DIR){
      numref[index] = i;
      index++;
    }
    dip++;
  }

  struct dinode *start = curr;
  for(int z = 0; z < index; z++){
    //BEGIN SEARCH
    //iterate through all inodes that have....
    for(int i = 0; i < num_inodes; i++){
      if(curr->type == T_DIR){
        for(int fuck = 0; fuck < NDIRECT; fuck++){
          dirent = (struct dirent*)(img_ptr + curr->addrs[fuck]*BSIZE);
          for(int j = 0; j < (BSIZE/sizeof(struct dirent)); j++){
            if(dirent->inum <= 0 || dirent->inum > num_inodes || strcmp(dirent->name,".") == 0 || strcmp(dirent->name, "..") == 0){
              dirent++;
              continue;
            }
            if(dirent->inum == numref[z]){
              found++;
            }
            dirent++;
          }
        }
        for(int lol = 0; lol < NINDIRECT; lol++){
          uint *indaddrs = img_ptr + curr->addrs[NDIRECT]*BSIZE;
          dirent = (struct dirent*)(img_ptr + *(indaddrs + lol)*BSIZE);
          for(int j = 0; j < (BSIZE/sizeof(struct dirent)); j++){
            if(dirent->inum <= 0 || dirent->inum > num_inodes || strcmp(dirent->name,".") == 0 || strcmp(dirent->name, "..") == 0){
              dirent++;
              continue;
            }
            if(dirent->inum == numref[z]){
              found++;
            }
            dirent++;
          }
        }
      }
      curr++;
    }
    if(found > 1){
      return -1;
    }
    curr = start;
    found = 0;
  }
  return 0;
}

int main (int argc, char* argv[]) {
  //refactor me
  //check for valid input
  if(argc != 2) {
    fprintf(stderr, "Usage: xv6_fsck <file_system_image>\n");
    exit(1);
  }

  char *img = argv[1];
  //check img exists
  int fd;
  if((fd = open(img, O_RDONLY)) < 0){
    fprintf(stderr, "image not found.\n");
    exit(1);
  }

  int rc;
  struct stat sbuf;
  if((rc = fstat(fd, &sbuf)) != 0){
    exit(1);
  }

  //use mmap
  void *img_ptr;
  if((img_ptr = mmap(NULL, sbuf.st_size, PROT_READ, MAP_PRIVATE, fd, 0)) == MAP_FAILED)
    exit(1);

  struct superblock *sb = (struct superblock *) (img_ptr + BSIZE);

  //cast struct dinode
  struct dinode *dip = (struct dinode *) (img_ptr + 2*BSIZE);

  char *bitmap = (img_ptr + ((sb->ninodes/IPB) + 3)*BSIZE);

  if(chk_inode_type(sb->ninodes, dip) < 0){
    fprintf(stderr, "ERROR: bad inode.\n");
    exit(1);
  }

  int errno;
  errno = chk_inodeaddr(sb->ninodes, dip, sb, img_ptr);
  if(errno < 0){
    if(errno == -1){
      fprintf(stderr, "ERROR: bad direct address in inode.\n");
      exit(1);
    } else if(errno == -2){
      fprintf(stderr, "ERROR: bad indirect address in inode.\n");
      exit(1);
    }
  }

  if(chk_rootdir(sb->ninodes, dip, img_ptr) < 0){
    fprintf(stderr, "ERROR: root directory does not exist.\n");
    exit(1);
  }

  if(chk_dir_format(sb->ninodes, dip, img_ptr, sb) < 0){
    fprintf(stderr, "ERROR: directory not properly formatted.\n");
    exit(1);
  }

  if(chk_inode_bitmap(bitmap, sb->ninodes, dip, img_ptr) < 0) {
    fprintf(stderr, "ERROR: address used by inode but marked free in bitmap.\n");
    exit(1);
  }

  if(chk_bitmap(bitmap, sb->ninodes, dip, img_ptr, sb) < 0) {
    fprintf(stderr, "ERROR: bitmap marks block in use but it is not in use.\n");
    exit(1);
  }

  if(chk_dup_addr(sb->ninodes, dip, sb) < 0){
    fprintf(stderr, "ERROR: direct address used more than once.\n");
    exit(1);
  }

  if(chk_dup_indaddr(sb->ninodes, dip, sb, img_ptr) < 0){
    fprintf(stderr, "ERROR: indirect address used more than once.\n");
    exit(1);
  }

  if(chk_inode_dir(sb->ninodes, dip, img_ptr) < 0){
    fprintf(stderr, "ERROR: inode marked use but not found in a directory.\n");
    exit(1);
  }

  if(chk_dir_inode(sb->ninodes, dip, img_ptr) < 0){
    fprintf(stderr, "ERROR: inode referred to in directory but marked free.\n");
    exit(1);
  }

  if(bad_reference(sb->ninodes, dip, img_ptr) < 0){
    fprintf(stderr, "ERROR: bad reference count for file.\n");
    exit(1);
  }

  if(dup_dir(sb->ninodes, dip, img_ptr) < 0){
    fprintf(stderr, "ERROR: directory appears more than once in file system.\n");
    exit(1);
  }
  return 0;
}
