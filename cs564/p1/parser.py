

"""
FILE: goat.py
------------------
Author: Michael Guyse (guyse@wisc.edu)
Author: Maggie Marxen (mmarxen@wisc.edu)
Author: Marco Carini  (marco.carini@wisc.edu)
Modified: 10/04/2018

This parser takes one or multiple ebay data json files and parses it to 
create 6 .dat files to be loaded into a sqlite database to be able to 
successfully run queries on it

The format of the .dat files is all the information for a given table in
the form info|info|moreinfo| etc...

There are 4 helper functions for the parser to be able to transform the month
date-time and dollar amount into the correct form and then also the last function
is meant for adding a single quote around any " so that it is read in as a " like
for example it will read in 29" tire correctly
"""

import sys
from json import loads
from re import sub

# create necessary global variables to hold information for checking for doubles
userId = []
sellerId = []
bidderId = []
categoryId = {}

# Dictionary of months used for date transformation
MONTHS = {'Jan':'01','Feb':'02','Mar':'03','Apr':'04','May':'05','Jun':'06',\
        'Jul':'07','Aug':'08','Sep':'09','Oct':'10','Nov':'11','Dec':'12'}

"""
Returns true if a file ends in .json
"""
def isJson(f):
    return len(f) > 5 and f[-5:] == '.json'

"""
If there is a " in a string it changes it to be three quotes so the quote prints out
"""
def transformQuote(str):
    strList = list(str)
    for letter in strList:
        if letter == '"':
            index = strList.index(letter)
            strList[index] = "'" + '"' + "'"
    return "".join(strList)

"""
Converts month to a number, e.g. 'Dec' to '12'
"""
def transformMonth(mon):
    if mon in MONTHS:
        return MONTHS[mon]
    else:
        return mon

"""
Transforms a timestamp from Mon-DD-YY HH:MM:SS to YYYY-MM-DD HH:MM:SS
"""
def transformDttm(dttm):
    dttm = dttm.strip().split(' ')
    dt = dttm[0].split('-')
    date = '20' + dt[2] + '-'
    date += transformMonth(dt[0]) + '-' + dt[1]
    return date + ' ' + dttm[1]

"""
Transform a dollar value amount from a string like $3,453.23 to XXXXX.xx
"""

def transformDollar(money):
    if money == None or len(money) == 0:
        return money
    return sub(r'[^\d.]', '', money)

"""
Parses a single json file. Currently, there's a loop that iterates over each
item in the data set. Your job is to extend this functionality to create all
of the necessary SQL tables for your database.
"""
def parseJson(json_file, userFile, bidderFile, sellerFile, bidFile, productFile, categoryFile):
    with open(json_file, 'r') as f:
        items = loads(f.read())['Items'] # creates a Python dictionary of Items for the supplied json file
      
        for item in items:
            userLine = bidderLine = sellerLine = bidLine = productLine = categoryLine = location = country = ""
            for index in item:

                # add itemid to product line and bidline
                if index == "ItemID":
                    productLine = item[index]
                    bidLine = item[index]   

                # add name to product line                 
                elif index == "Name":
                    productLine += "|" + transformQuote(item[index])

                # add category to category table
                elif index == "Category":
                    for category in item[index]:

                        # check to see if the category is created in the hashtable that holds all the categories and their itemids
                        if transformQuote(category) not in categoryId:
                            categoryId[category] = {
                                "category": transformQuote(category),
                                "item": [item["ItemID"]]
                            }
                            categoryLine = item["ItemID"]
                            categoryLine += "|" + transformQuote(category)

                            # write category to the file
                            categoryFile.write(categoryLine + "\n")
                            categoryLine = ""

                        # check to see if the itemid is already within the corresponding category
                        elif transformQuote(category) in categoryId and item["ItemID"] not in categoryId[category]["item"]:
                            categoryId[category]["item"].append(item["ItemID"])
                            categoryLine = item["ItemID"]
                            categoryLine += "|" + transformQuote(category)

                            # write category to the file
                            categoryFile.write(categoryLine + "\n")
                            categoryLine = ""

                # add number of bids to product line
                elif index == "Number_of_Bids":
                    productLine += "|" + item[index]

                # add current price to product line
                elif index == "Currently":
                    productLine += "|" + transformDollar(item[index])

                # add first bid to product line
                elif index == "First_Bid":
                    productLine += "|" + transformDollar(item[index])

                # go through bids to add in
                elif index == "Bids":

                    # skip over if item doesn't have a bid yet
                    if item[index] == None:
                        continue

                    # iterate over bids and add user id to bidline
                    for bid in item[index]:
                        bidLine += "|" + transformQuote(bid["Bid"]["Bidder"]["UserID"])
                        indexes = ["UserID","Rating","Location","Country"]

                        # only add the bidder information to user if it is not already in userId array
                        if bid["Bid"]["Bidder"]["UserID"] not in userId:
                            userId.append(bid["Bid"]["Bidder"]["UserID"])
                            for bidder in indexes:
                                if bidder in bid["Bid"]["Bidder"]:
                                    if bidder =="UserID":
                                        userLine = transformQuote(bid["Bid"]["Bidder"]["UserID"])
                                    elif bidder == "Rating":
                                        userLine += "|" + bid["Bid"]["Bidder"][bidder]
                                    else:
                                        userLine += "|" + transformQuote(bid["Bid"]["Bidder"][bidder])
                                else:
                                    userLine += "|NULL"

                            # write user info to the user file
                            userFile.write(userLine + "\n")
                            userLine = ""

                        # only add the bidder information to bidder file if it is not already in the file
                        if bid["Bid"]["Bidder"]["UserID"] not in bidderId:
                            bidderId.append(bid["Bid"]["Bidder"]["UserID"])
                            for bidder in indexes:
                                if bidder in bid["Bid"]["Bidder"]:
                                    if bidder =="UserID":
                                        bidderLine = transformQuote(bid["Bid"]["Bidder"]["UserID"])
                                    elif bidder == "Rating":
                                        bidderLine += "|" + bid["Bid"]["Bidder"][bidder]
                                    else:
                                        bidderLine += "|" + transformQuote(bid["Bid"]["Bidder"][bidder])
                                else:
                                    bidderLine += "|NULL"

                            # write the bidder information to the file
                            bidderFile.write(bidderLine + "\n")
                            bidderLine = ""

                        # add time and amount of bid to the bid table
                        if "Time" in bid["Bid"]:
                            bidLine += "|" + transformDttm(bid["Bid"]["Time"])
                        if "Amount" in bid["Bid"]:
                            bidLine += "|" + transformDollar(bid["Bid"]["Amount"])

                        # write the bid info to the bid file
                        bidFile.write(bidLine + "\n")
                        bidLine = item["ItemID"]
                
                # add seller info to seller, user, and the userid to product line
                elif index == "Seller":
                    productLine += "|" + transformQuote(item[index]["UserID"])

                    # add userid and rating to user line for creating seller in user file
                    if item[index]["UserID"] not in userId:
                        userId.append(item[index]["UserID"])
                        if "UserID" in item[index]:
                            userLine = transformQuote(item[index]["UserID"])
                        if "Rating" in item[index]:
                            userLine += "|" + item[index]["Rating"]

                    # add userid and rating to seller
                    if item[index]["UserID"] not in sellerId:
                        sellerId.append(item[index]["UserID"])
                        if "UserID" in item[index]:
                            sellerLine = transformQuote(item[index]["UserID"])
                        if "Rating" in item[index]:
                            sellerLine += "|" + item[index]["Rating"]

                # save location of seller to be added if seller isnt already in seller file            
                elif index == "Location":
                    location = transformQuote(item[index])

                # save country of seller to be added if seller isnt already in seller file
                elif index == "Country":
                    country = transformQuote(item[index])

                # add the start of the bid to the product line
                elif index == "Started":
                    productLine += "|" + transformDttm(item[index])

                # add the end time of the bid to the product line
                elif index == "Ends":
                    productLine += "|" + transformDttm(item[index])

                # check to see if there is a description and if so add it to the product line
                elif index == "Description":
                    if item[index] != None:
                        productLine += "|" + transformQuote(item[index])
                    else:
                        productLine += "|NULL"

            # make sure the seller wasn't a duplicate and if not add location and country and write to seller file
            if sellerLine != "":
                sellerLine += "|" + location + "|" + country
                sellerFile.write(sellerLine + "\n")

            # write product line to the product file
            productFile.write(productLine + "\n")

            # make sure the user wasn't a duplicate and if not add location and country and write to seller file
            if userLine != "":
                userLine += "|" + location +"|" + country
                userFile.write(userLine + "\n")

            
            

"""
Loops through each json files provided on the command line and passes each file
to the parser
"""
def main(argv):
    if len(argv) < 2:
        print >> sys.stderr, 'Usage: python skeleton_json_parser.py <path to json files>'
        sys.exit(1)
    
    # open all the necessary table files
    user = open("user.dat", "w+")
    bidder = open("bidder.dat", "w+")
    seller = open("seller.dat", "w+")
    bid = open("bids.dat", "w+")
    product = open("product.dat", "w+")
    category = open("category.dat", "w+")

    # loops over all .json files in the argument
    for f in argv[1:]:
        if isJson(f):
            parseJson(f, user, bidder, seller, bid, product, category)
            print ("Success parsing " + f)

    # close all the necessary table files
    user.close()
    bidder.close()
    seller.close()
    bid.close()
    product.close()
    category.close()

if __name__ == '__main__':
    main(sys.argv)