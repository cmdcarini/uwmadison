SELECT COUNT(*)
FROM (
    SELECT *
    FROM Category, Bids
    WHERE ((Category.itemID = Bids.product_itemID) AND (Bids.amount > 100.0))
    GROUP BY Category.category
    HAVING COUNT(*) >= 1
);