drop table if exists User;
drop table if exists Bidder;
drop table if exists Seller;
drop table if exists Bids;
drop table if exists Product;
drop table if exists Category;

CREATE TABLE User(userId TEXT NOT NULL,
    rating INT NOT NULL,            """[null?]"""
    country TEXT,  
    location TEXT,
    PRIMARY KEY(userId))

CREATE TABLE Bidder(userId TEXT NOT NULL,
    rating INT NOT NULL
    location TEXT,
    country TEXT,                      
    PRIMARY KEY(userId)
    FOREIGN KEY(userId)
        REFERENCES User,
    FOREIGN KEY(rating)
        REFERENCES User,
    FOREIGN KEY(location)
        REFERENCES User,
    FOREIGN KEY(country)
        REFERENCES User)

CREATE TABLE Seller(userId TEXT NOT NULL,
    rating INT NOT NULL,
    location TEXT,
    country TEXT,
    PRIMARY KEY(userId)
    FOREIGN KEY(userId)
        REFERENCES User,
    FOREIGN KEY(rating)
        REFERENCES User,
    FOREIGN KEY(location)
        REFERENCES User,
    FOREIGN KEY(country)
        REFERENCES User)

CREATE TABLE Bids(product_itemId TEXT NOT NULL,
    bidder_userId TEXT NOT NULL,
    time DATETIME NOT NULL,
    amount FLOAT NOT NULL,
    PRIMARY KEY(product_itemId)
    FOREIGN KEY(bidder_userId)
        REFERENCES Bidder(userId),
     FOREIGN KEY(product_itemId)
        REFERENCES Product(itemId))  """[piazza question]"""

CREATE TABLE Product(itemId TEXT NOT NULL,
    name TEXT NOT NULL,
    category TEXT,
    number_of_bids INT NOT NULL,
    currently FLOAT NOT NULL,
    first_bid FLOAT NOT NULL,
    started DATETIME NOT NULL,
    ends DATETIME NOT NULL,
    description TEXT,        
    seller_userId TEXT NOT NULL,
    PRIMARY KEY(itemId)
    FOREIGN KEY(seller_userId)
        REFERENCES Seller(userId))    """[piazza question]"""
        
CREATE TABLE Category(itemId TEXT NOT NULL,
    category TEXT NOT NULL,
    FOREIGN KEY(itemId)
        REFERENCES Product(itemId)