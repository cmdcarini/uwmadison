/**
 * FILE: buffer.cpp
 * ------------------
 * Author: Michael Guyse (guyse@wisc.edu)
 * Author: Maggie Marxen (mmarxen@wisc.edu)
 * Author: Marco Carini  (marco.carini@wisc.edu)
 * Modified: 11/08/2018
 * 
 * The following is our implementation of a buffer manager using the clock replacement algorithm. 
 * 
 * @author See Contributors.txt for code contributors and overview of BadgerDB.
 *
 * @section LICENSE
 * Copyright (c) 2012 Database Group, Computer Sciences Department, University of Wisconsin-Madison.
 */

#include <memory>
#include <iostream>
#include "buffer.h"
#include "exceptions/buffer_exceeded_exception.h"
#include "exceptions/page_not_pinned_exception.h"
#include "exceptions/page_pinned_exception.h"
#include "exceptions/bad_buffer_exception.h"
#include "exceptions/hash_not_found_exception.h"

namespace badgerdb
{

/**
 * BufMgr: the class constructor
 * 
 * @params bufs
 */ 
BufMgr::BufMgr(std::uint32_t bufs)
	: numBufs(bufs)
{
	bufDescTable = new BufDesc[bufs];

	for (FrameId i = 0; i < bufs; i++)
	{
		bufDescTable[i].frameNo = i;
		bufDescTable[i].valid = false;
	}

	bufPool = new Page[bufs];

	int htsize = ((((int)(bufs * 1.2)) * 2) / 2) + 1;
	hashTable = new BufHashTbl(htsize); // allocate the buffer hash table

	clockHand = bufs - 1;
}

/**
 * ~BufMgr: the class deconstructor. Responsible for flushing out dirty pages
 * and deallocating the bufDescTable and bufPool.
 */ 
BufMgr::~BufMgr()
{
	for (uint32_t i = 0; i < numBufs; i++)
	{
		if (bufDescTable[i].valid == true && bufDescTable[i].dirty == true)
		{
			bufDescTable[i].file->writePage(bufPool[bufDescTable[i].frameNo]);
		}
	}
	delete[] bufDescTable;
	delete[] bufPool;
	delete hashTable;
}

/**
 * advanceClock: increments the clock hand by one, and 
 * checks to see if clockHand is equal to numBufs.
 */ 
void BufMgr::advanceClock()
{
	clockHand++;
	if (clockHand == numBufs)
	{
		clockHand = 0;
	}
}

/**
 * allocBuf: allocates a free frame using the clock replacement algorithm
 * 
 * @params &frame
 */ 
void BufMgr::allocBuf(FrameId &frame)
{
	uint32_t originalClock = clockHand;
	uint32_t pinnedPages = 0;
	bool found = false;
	BufDesc curr;

	advanceClock();

	// loop through all the pages until a page is found, throw BufferExceededExecption if not found
	while (pinnedPages < numBufs && !found)
	{

		// if we have cycled all the way through reset pinnedPages
		if (originalClock == clockHand)
		{
			pinnedPages = 0;
		}

		// set curr to be the current page in the Table
		curr = bufDescTable[clockHand];

		// if current page isn't valid use this to allocate
		if (!curr.valid)
		{
			frame = curr.frameNo;
			found = true;
		}

		// if refbit is true for page set to false since it is not new anymore
		else if (curr.refbit)
		{
			bufDescTable[clockHand].refbit = false;
			advanceClock();
		}

		// if pin count isn't 0 advance the clock also keep track of the amount of pinned pages there are
		// if all pages are pinned exit loop
		else if (curr.pinCnt > 0)
		{
			advanceClock();
			pinnedPages++;
		}

		// frame is valid but refbit and pin count are false and 0 so write this frame to disk and use
		else
		{
			if (curr.dirty)
			{
				bufDescTable[clockHand].file->writePage(bufPool[curr.frameNo]);
			}

			hashTable->remove(curr.file, curr.pageNo);

			bufDescTable[clockHand].Clear();

			frame = curr.frameNo;
			found = true;
		}
	}

	// no open frame found so throw exception
	if (!found)
	{
		throw BufferExceededException();
	}
}

/**
 * readPage: reads and returns page that was read
 * 
 * @params file, pageNo, page
 */ 
void BufMgr::readPage(File *file, const PageId pageNo, Page *&page)
{
	FrameId frameNo;

	try
	{
		// try to see if the page is already in the hashtable
		hashTable->lookup(file, pageNo, frameNo);

		// it is so increment pincnt and set refbit to true
		bufDescTable[frameNo].pinCnt++;
		bufDescTable[frameNo].refbit = true;

		// return the page that was read
		page = &bufPool[frameNo];
	}

	// frame not found so allocate page and insert the page and file
	catch (HashNotFoundException h)
	{
		Page newPage = file->readPage(pageNo);

		allocBuf(frameNo);

		bufPool[frameNo] = newPage;

		hashTable->insert(file, pageNo, frameNo);

		bufDescTable[frameNo].Set(file, pageNo);

		// return the page that was created and read
		page = &bufPool[frameNo];
	}
}

/**
 * unPinPage: decrements pinCnt of frame
 * 
 * @params file, pageNo, dirty
 */ 
void BufMgr::unPinPage(File *file, const PageId pageNo, const bool dirty)
{
	FrameId frameNo;

	try
	{

		hashTable->lookup(file, pageNo, frameNo);
	}
	catch (HashNotFoundException h)
	{
		// do nothing just precaution incase lookup throws
	}

	// make sure there is an pin count for the page
	if (bufDescTable[frameNo].pinCnt <= 0)
	{
		throw PageNotPinnedException(file->filename(), pageNo, frameNo);
	}

	// check if it is dirty and if so set to true
	if (dirty)
	{
		bufDescTable[frameNo].dirty = true;
	}

	// decrement pin count
	bufDescTable[frameNo].pinCnt--;
}

/**
 * flushFile: flush file from bufTable
 * 
 * @params file
 */ 
void BufMgr::flushFile(const File *file)
{
	// iterate through all frames to check for the file
	for (uint32_t i = 0; i < numBufs; i++)
	{
		BufDesc currFrame = bufDescTable[i];

		// if the frame has the file flush it
		if (currFrame.file == file)
		{

			// make sure the file is valid else throw
			if (!currFrame.valid)
			{
				throw BadBufferException(currFrame.frameNo, currFrame.dirty, currFrame.valid, currFrame.refbit);
			}

			// make sure the file is not pinned  else throw
			if (currFrame.pinCnt > 0)
			{
				throw PagePinnedException(file->filename(), currFrame.pageNo, currFrame.frameNo);
			}

			// if the frame is dirty write it
			if (currFrame.dirty)
			{
				Page page = bufPool[currFrame.frameNo];
				bufDescTable[i].file->writePage(page);
				bufDescTable[i].dirty = false;
			}

			// clear the file and frame
			hashTable->remove(file, currFrame.pageNo);
			bufDescTable[i].Clear();
		}
	}
}

/**
 * allocPage: returns a newly allocated page
 * 
 * @params file, pageNo, page
 */ 
void BufMgr::allocPage(File *file, PageId &pageNo, Page *&page)
{
	FrameId frameNo;

	// allocate a page for the file
	Page newPage = file->allocatePage();

	// allocate buffer frame
	allocBuf(frameNo);

	// put the newPgae in the frame in the buffer pool
	bufPool[frameNo] = newPage;

	// insert into the hashtable
	hashTable->insert(file, newPage.page_number(), frameNo);

	// set it in the bufTable
	bufDescTable[frameNo].Set(file, newPage.page_number());

	// return variables
	pageNo = newPage.page_number();
	page = &bufPool[frameNo];
}

/**
 * disposePage: deletes given page from given file
 * 
 * @params file, pageNo
 */ 
void BufMgr::disposePage(File *file, const PageId PageNo)
{
	FrameId frameNo;

	try
	{
		// make sure the page is within the hashtable before trying to remove
		hashTable->lookup(file, PageNo, frameNo);

		// remove from hashTable
		hashTable->remove(file, PageNo);

		// clear buffer frame
		bufDescTable[frameNo].Clear();
	}
	catch (HashNotFoundException e)
	{
		// do nothing, just here for safety with lookup
	}

	// remove page
	file->deletePage(PageNo);
}

/**
 * printSelf: prints self
 */ 
void BufMgr::printSelf(void)
{
	BufDesc *tmpbuf;
	int validFrames = 0;

	for (std::uint32_t i = 0; i < numBufs; i++)
	{
		tmpbuf = &(bufDescTable[i]);
		std::cout << "FrameNo:" << i << " ";
		tmpbuf->Print();

		if (tmpbuf->valid == true)
			validFrames++;
	}

	std::cout << "Total Number of Valid Frames:" << validFrames << "\n";
}
}
