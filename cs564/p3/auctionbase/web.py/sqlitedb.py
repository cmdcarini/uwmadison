/**
 * FILE: auctionbase.py
 * ------------------
 * Author: Michael Guyse (guyse@wisc.edu)
 * Author: Maggie Marxen (mmarxen@wisc.edu)
 * Author: Marco Carini  (marco.carini@wisc.edu)
 * Modified: 11/08/2018
 * 
 * The following is our implementation of a buffer manager using the clock replacement algorithm. 
 * 
 * @author See Contributors.txt for code contributors and overview of BadgerDB.
 *
 * @section LICENSE
 * Copyright (c) 2012 Database Group, Computer Sciences Department, University of Wisconsin-Madison.
 */

import web

db = web.database(dbn='sqlite',
        db='AuctionBase.db' #TODO: add your SQLite database filename
    )

######################BEGIN HELPER METHODS######################

# Enforce foreign key constraints
# WARNING: DO NOT REMOVE THIS!
def enforceForeignKey():
    db.query('PRAGMA foreign_keys = ON')

# initiates a transaction on the database
def transaction():
    return db.transaction()
# Sample usage (in auctionbase.py):
#
# t = sqlitedb.transaction()
# try:
#     sqlitedb.query('[FIRST QUERY STATEMENT]')
#     sqlitedb.query('[SECOND QUERY STATEMENT]')
# except Exception as e:
#     t.rollback()
#     print str(e)
# else:
#     t.commit()
#
# check out http://webpy.org/cookbook/transactions for examples

# returns the current time from your database
def getTime():
    query_string = 'select Time from CurrentTime'
    results = query(query_string)
    return results[0].Time

# returns a single item specified by the Item's ID in the database
def getItemById(item_id):
    query_string = 'select * from Items where itemID = $itemID'
    result = query(query_string, {'itemID': item_id})
    try: return result[0]
    except IndexError: return None

# returns a single category specified by the Item's ID in the database
def getCategoryById(item_id):
    query_string = 'select group_concat(Category,", ") as Category from Categories where ItemID = $itemID'
    result = query(query_string, {'itemID': item_id})
    try: return result[0]
    except IndexError: return None

# returns a list of bids specified by the Item's ID in the database
def getBidById(item_id):
    query_string = 'select UserID as "Bidder ID", Time as "Bid Time", Amount as "Bid Price" from Bids where ItemID = $itemID'
    result = query(query_string, {'itemID': item_id})
    try: return result
    except IndexError: return None

# returns a single winner specified by the Item's ID in the database
def getWinnerById(item_id):
    query_string = 'select * from Bids where ItemID = $itemID and Amount = (select Max(Amount) from Bids where ItemID = $itemID)'
    result = query(query_string, {'itemID': item_id})
    try: return result[0]
    except IndexError: return None

# returns a single user specified by the Item's ID in the database
def getUserById(user_id):
    query_string = 'select * from Users where UserID = $userID'
    result = query(query_string, {'userID': user_id})
    try: return result[0]
    except IndexError: return None

# wrapper method around web.py's db.query method
# check out http://webpy.org/cookbook/query for more info
def query(query_string, vars = {}):
    return list(db.query(query_string, vars))

#####################END HELPER METHODS#####################

# method to update the time in the database
def updateTime(query_input):
    t = transaction()
    try: db.update('CurrentTime', where = 'Time = $newTime', vars = {'newTime': getTime() }, Time = query_input)
    except Exception as timeException:
        t.rollback()
        print(str(timeException))
        raise Exception
    else: t.commit()

# method to add a new bid on an item in the database
def new_bid(currentItem, currentUser, currentBid):
    t = transaction()
    try: db.insert('Bids', UserID = currentUser, ItemID = currentItem, Amount = currentBid, Time = getTime())
    except Exception as bidException:
        t.rollback()
        return False
    else:
        t.commit()
        return True

# method to search the database based off of multiple possible parameters
def search(itemID, userID, category, description, minPrice, maxPrice, status):
    
    # manipulate the data to be correct for queries
    if description is None: description = '%%'
    else: description = '%' + description + '%'
    if minPrice == '': minPrice = 0
    else: minPrice = int(minPrice)
    if maxPrice == '': maxPrice = 999999999999999999
    else: maxPrice = int(maxPrice)

    # change query for data based on what the user choose for the status of the auction
    if status == 'open':
    	query_string = 'select *, group_concat(category,", ") as Category from Items, Categories, CurrentTime where (Categories.ItemID = Items.ItemID) AND (IFNULL($category, "") = "" OR $category = Categories.category) AND (IFNULL($itemID, "") = "" OR $itemID = Items.ItemID) AND (IFNULL($userID, "") = "" OR $userID = Items.Seller_UserID) AND (Items.Description LIKE $description) AND (IFNULL(Items.Currently, Items.First_Bid) >= $minPrice) AND (IFNULL(Items.Currently, Items.First_Bid) <= $maxPrice) AND (Items.Started <= CurrentTime.Time AND Items.Ends >= CurrentTime.Time) group by Items.ItemID'
    if status == 'close':
    	query_string = 'select *, group_concat(category,", ") as Category from Items, Categories, CurrentTime where (Categories.ItemID = Items.ItemID) AND (IFNULL($category, "") = "" OR $category = Categories.category) AND (IFNULL($itemID, "") = "" OR $itemID = Items.ItemID) AND (IFNULL($userID, "") = "" OR $userID = Items.Seller_UserID) AND (Items.Description LIKE $description) AND (IFNULL(Items.Currently, Items.First_Bid) >= $minPrice) AND (IFNULL(Items.Currently, Items.First_Bid) <= $maxPrice) AND (Items.Ends < CurrentTime.Time) group by Items.ItemID'
    if status == 'notStarted':
    	query_string = 'select *, group_concat(category,", ") as Category from Items, Categories, CurrentTime where (Categories.ItemID = Items.ItemID) AND (IFNULL($category, "") = "" OR $category = Categories.category) AND (IFNULL($itemID, "") = "" OR $itemID = Items.ItemID) AND (IFNULL($userID, "") = "" OR $userID = Items.Seller_UserID) AND (Items.Description LIKE $description) AND (IFNULL(Items.Currently, Items.First_Bid) >= $minPrice) AND (IFNULL(Items.Currently, Items.First_Bid) <= $maxPrice) AND (Items.Started > CurrentTime.Time) group by Items.ItemID'
    if status == 'all':
        query_string = 'select *, group_concat(category,", ") as Category from Items, Categories, CurrentTime where (Categories.ItemID = Items.ItemID) AND (IFNULL($category, "") = "" OR $category = Categories.category) AND (IFNULL($itemID, "") = "" OR $itemID = Items.ItemID) AND (IFNULL($userID, "") = "" OR $userID = Items.Seller_UserID) AND (Items.Description LIKE $description) AND (IFNULL(Items.Currently, Items.First_Bid) >= $minPrice) AND (IFNULL(Items.Currently, Items.First_Bid) <= $maxPrice) group by Items.ItemID'

    # get the results of the query and return it if it exists
    result = query(query_string, {'category': category, 'itemID': itemID, 'userID': userID, 'description': description, 'minPrice': minPrice, 'maxPrice': maxPrice})
    try: return result
    except IndexError: return None
