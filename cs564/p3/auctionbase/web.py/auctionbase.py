/**
 * FILE: auctionbase.py
 * ------------------
 * Author: Michael Guyse (guyse@wisc.edu)
 * Author: Maggie Marxen (mmarxen@wisc.edu)
 * Author: Marco Carini  (marco.carini@wisc.edu)
 * Modified: 11/08/2018
 * 
 * The following is our implementation of a buffer manager using the clock replacement algorithm. 
 * 
 * @author See Contributors.txt for code contributors and overview of BadgerDB.
 *
 * @section LICENSE
 * Copyright (c) 2012 Database Group, Computer Sciences Department, University of Wisconsin-Madison.
 */
#!/usr/bin/env python

import sys; sys.path.insert(0, 'lib') # this line is necessary for the rest
import os                             # of the imports to work!

import web
import sqlitedb
from jinja2 import Environment, FileSystemLoader
from datetime import datetime

###########################################################################################
##########################DO NOT CHANGE ANYTHING ABOVE THIS LINE!##########################
###########################################################################################

######################BEGIN HELPER METHODS######################

# helper method to convert times from database (which will return a string)
# into datetime objects. This will allow you to compare times correctly (using
# ==, !=, <, >, etc.) instead of lexicographically as strings.

# Sample use:
# current_time = string_to_time(sqlitedb.getTime())

def string_to_time(date_str):
    return datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')

# helper method to render a template in the templates/ directory
#
# `template_name': name of template file to render
#
# `**context': a dictionary of variable names mapped to values
# that is passed to Jinja2's templating engine
#
# See curr_time's `GET' method for sample usage
#
# WARNING: DO NOT CHANGE THIS METHOD
def render_template(template_name, **context):
    extensions = context.pop('extensions', [])
    globals = context.pop('globals', {})

    jinja_env = Environment(autoescape=True,
            loader=FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')),
            extensions=extensions,
            )
    jinja_env.globals.update(globals)

    web.header('Content-Type','text/html; charset=utf-8', unique=True)

    return jinja_env.get_template(template_name).render(context)

#####################END HELPER METHODS#####################

# urls to interact with our webpage
urls = ('/currtime', 'curr_time',
        '/selecttime', 'select_time',
        '/add_bid', 'add_bid', 
        '/search', 'search',
        '/items', 'items',
        '/', 'search'
        # TODO: add additional URLs here
        # first parameter => URL, second parameter => class name
        )

# class for rendering current time page
class curr_time:
    # A simple GET request, to '/currtime'
    #
    # Notice that we pass in `current_time' to our `render_template' call
    # in order to have its value displayed on the web page
    def GET(self):
        current_time = sqlitedb.getTime()
        return render_template('curr_time.html', time = current_time)

# class for rendering page to select what time it is
class select_time:
    # Aanother GET request, this time to the URL '/selecttime'
    def GET(self):
        return render_template('select_time.html')

    # A POST request
    def POST(self):
        post_params = web.input()
        MM = post_params['MM']
        dd = post_params['dd']
        yyyy = post_params['yyyy']
        HH = post_params['HH']
        mm = post_params['mm']
        ss = post_params['ss']
        enter_name = post_params['entername']


        selected_time = '%s-%s-%s %s:%s:%s' % (yyyy, MM, dd, HH, mm, ss)
        update_message = '(Hello, %s. Previously selected time was: %s.)' % (enter_name, selected_time)
        try: sqlitedb.updateTime(selected_time)
        except Exception as timeEx:
            update_message = 'Cannot update time, value is not valid'
            print(str(timeEx))
        return render_template('select_time.html', message = update_message)

# class for rendering the search page with adding a link to the items page
class search:
    def GET(self):
        return render_template('search.html')

    def POST(self):
        # get all the necessary params for a search
        post_params = web.input()
        itemID = post_params['itemID']
        userID = post_params['userID']
        category = post_params['category']
        description = post_params['description']
        minimumPrice = post_params['minPrice']
        maximumPrice = post_params['maxPrice']
        status = post_params['status']

        # make sure there is at least one param that is not empty call render template if there is no param to search
        if itemID == '' and userID == '' and category == '' and description == '' and minimumPrice == '' and maximumPrice == '':
            return render_template('search.html', message = 'Unable to search the database, please enter a search parameter')
        else:
            queryResult = sqlitedb.search(itemID, userID, category, description, minimumPrice, maximumPrice, status)
            return render_template('search.html', search_result = queryResult)

# class for rendering the page to add a bid on an item
class add_bid:
    def GET(self):
        return render_template('add_bid.html')

    def POST(self):
        # get necessary params for adding a bid
        post_params = web.input()
        itemID = post_params['itemID']
        userID = post_params['userID']
        Bid = post_params['price']

        # make sure that there all of the parameters exist
        if itemID == '' or userID == '' or Bid == '':
            return render_template('add_bid.html', message = 'Invalid criteria, make sure itemID, userID, and bid are valid')
        else:
            currentItem = sqlitedb.getItemById(itemID)
            currentUser = sqlitedb.getUserById(userID)

        # render page if item or user cannot be found
        if currentItem is None:
            return render_template('add_bid.html', message = 'Unable to find item')
        elif currentUser is None:
            return render_template('add_bid.html', message = 'Unable to find user')

        # seller cannot bid on their own product
        elif currentUser.UserID == currentItem.Seller_UserID:
            return render_template('add_bid.html', message = 'Unable to place bid, seller cannot bid on their own product')

        # render page if auction is over or hasn't started
        elif string_to_time(sqlitedb.getTime()) >= string_to_time(currentItem.Ends):
            return render_template('add_bid.html', message = 'Unable to place bid, auction has ended')
        elif string_to_time(sqlitedb.getTime()) < string_to_time(currentItem.Started):
            return render_template('add_bid.html', message = 'Unable to place bid, auction has yet to begin')

        # render page if bid was below starting or current price
        elif float(Bid) < 0 or float(Bid) <= float(currentItem.First_Bid) or float(Bid) <= float(currentItem.Currently):
            return render_template('add_bid.html', message = 'Unable to place bid, it was below the current price')

        # if price is higher than buy price then user bought the item
        if currentItem.Buy_Price is not None:
            if float(Bid) >= float(currentItem.Buy_Price):
                Purchase = 'Congratulations. You have bought item: %s for: %s.' % (currentItem.Name, Bid)
                return render_template('add_bid.html', message = Purchase, add_result = sqlitedb.new_bid(itemID, userID, Bid))

        # render page for a successful bid on the product
        Purchase = 'Congratulations. You have bid on item: %s for: %s.' % (currentItem.Name, Bid)
        return render_template('add_bid.html', message = Purchase, add_result = sqlitedb.new_bid(itemID, userID, Bid))

# class for rendering the items page
class items:
    def GET(self):
        # get necessary information to be able to render the page
        finished = False
        hasBuyPrice = False
        post_params = web.input()
        itemID = post_params['id']
        item = sqlitedb.getItemById(itemID)
        category = sqlitedb.getCategoryById(itemID)
        bid = sqlitedb.getBidById(itemID)
        buyPrice = ''
        auctionWinner = ''

        # create message for the status of the auction
        if item.Started <= sqlitedb.getTime() and item.Ends >= sqlitedb.getTime(): status = 'Auction is open!'
        elif item.Started > sqlitedb.getTime(): status = 'Auction has not started'
        else: status = 'Closed'

        # if there was a bid get the auction winner
        if item.Number_of_Bids == 0: noBid = True
        else:
            noBid = False
            auctionWinner = sqlitedb.getWinnerById(itemID).UserID

        # get proper information to see if the item was bought
        if item.Buy_Price is not None:
            hasBuyPrice = True
            buyPrice = item.Buy_Price
            if status == 'Closed' or float(item.Currently) >= float(item.Buy_Price):
                status = 'Closed'
                finished = True
        elif status == 'Closed':
            ended = True

        return render_template('items.html', id = itemID, bids = bid, Name = item.Name, category = category.Category, Ends = item.Ends, Started = item.Started, Number_of_Bids = item.Number_of_Bids, Seller = item.Seller_UserID, Description = item.Description, Currently = item.Currently, noBids = noBid, ended = ended, Status = status, Winner = auctionWinner, buyPrice = buyPrice, hasBuyPrice = hasBuyPrice)


###########################################################################################
##########################DO NOT CHANGE ANYTHING BELOW THIS LINE!##########################
###########################################################################################

if __name__ == '__main__':
    web.internalerror = web.debugerror
    app = web.application(urls, globals())
    app.add_processor(web.loadhook(sqlitedb.enforceForeignKey))
    app.run()
