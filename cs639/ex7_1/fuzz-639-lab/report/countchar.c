#include <stdio.h>
#include <stdlib.h>

int *cnttable;

void initcharcount ()
{
    cnttable = (int *)malloc(sizeof(int) * 100000);
}

void countchar (char c)
{
    if (c < 0) return;
    int i;

    i = (c*10000+7) % 100000;
    cnttable[i]++;
    /* Debug: printf ("c=%d (%x), cnttable[%d]=%d\n", (int)c, (int)c, i, cnttable[i]); */
    return;
}

int chartotal (char c)
{
    if (c < 0) return 0;
    int i;

    i = (c*10000+7) % 100000;
    return (cnttable[i]);
}
