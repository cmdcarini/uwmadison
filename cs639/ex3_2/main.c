#include <stdio.h>
#include <stdlib.h>
#include <limits.h>


signed int add();
signed int subtract();
signed int multiply();
signed int divide();

signed int add ( signed int x, signed int y ) {
	signed int total = x + y;
	
	if ((x > 0 && y > 0 && total < 0) || (x < 0 && y < 0 && total > 0)) {
		fprintf(stderr, "%s", "WARNING: Overflow detected in add().\n");
    	exit(-1);
	}

	return total;
}

signed int subtract ( signed int x, signed int y ) {
	signed int total = x - y;
	
	if ( (x > 0 && y < 0 && total < 0) || (x < 0 && y > 0 && total > 0) ) {
		fprintf(stderr, "%s", "WARNING: Overflow detected in substract().\n");
    	exit(-1);
	}

	return total;
}

signed int multiply ( signed int x, signed int y ) {
	
	
	if( INT_MAX / y < x || INT_MIN / y > x ) {
		fprintf(stderr, "%s", "WARNING: Overflow detected in multiply().\n");
		exit(-1);
	}

	signed int total = x * y;
	printf("%d\n", total );

	return total;
}

signed int divide ( signed int x, signed int y ) {
	if( y == 0 ) {
		fprintf(stderr, "%s", "WARNING: DIV by zero in divide().\n");
		exit(-1);
	}
	printf("%d %d\n", x, y );

	if ( (x == INT_MIN) && (y == -1)) {
		fprintf(stderr, "%s", "WARNING: Overflow detected in divide().\n");
		exit(-1);
	}
	signed int total = x / y;


	printf("%d\n", total );

	return total;
}

int main ( int argc, char **argv ) {
	signed int x = INT_MIN;
	signed int y = -1;

	printf("x: %d\ty:%d\n", x,y);
	//printf("ADD: %d\n", add(x,y));
	//printf("SUB: %d\n", subtract(x,y));
	//printf("MUL: %d\n", multiply(x,y));
	printf("DIV: %d\n", divide(x,y));
}