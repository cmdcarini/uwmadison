import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.SecureRandom;
import java.util.Base64.Encoder;
import java.util.Base64;

/**
 * Java servlet for creating a session and attaching two attributes: a username
 * and a counter. Does not consider existing sessions.
 * 
 * @author Joseph Eichenhofer
 */
public class LoginServlet extends HttpServlet {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 * 
	 * Create a session for the specified username (no checks of
	 * password/authentication). Does not consider existing sessions with same
	 * username. Initialize clicks to zero.
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		// check for username parameter
		String username = req.getParameter("username");
		if (username != null && !username.equals("")) {
			// request contained a username, set session attribute for username and
			// initialize click count to zero
			SecureRandom random = new SecureRandom();
			byte[] tokenBytes = new byte[32];
			random.nextBytes(tokenBytes);
			Encoder encoder = Base64.getUrlEncoder().withoutPadding();
			String token = encoder.encodeToString(tokenBytes);
			req.getSession(true).setAttribute("username", username);
			req.getSession().setAttribute("clicks", new Integer(0));
			req.getSession().setAttribute("token", token);
		}

		// redirect to main page
		res.sendRedirect("view");
	}
}
