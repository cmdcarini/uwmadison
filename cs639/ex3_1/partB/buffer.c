#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE_OF_ARRAY 10
#define OVERFLOW_NUM 10000

// Global Variable (Data)
//long array[SIZE_OF_ARRAY];

int main(void) {
    // Stack
    //long array[SIZE_OF_ARRAY];

    // Heap
    long *array = malloc(sizeof(long) * SIZE_OF_ARRAY);

    
    for(int i=0; i<SIZE_OF_ARRAY+OVERFLOW_NUM; i++) {
        array[i]=1000;
    }
    
    for(int i=0; i<SIZE_OF_ARRAY+OVERFLOW_NUM; i++) {
        printf("%-5d:%ld\n",i,array[i]);
    }
    
    return 0;
}
