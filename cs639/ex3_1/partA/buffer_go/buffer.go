package main

import (
	"fmt"
	"bufio"
	"os"
)

func main() {
    reader := bufio.NewReader(os.Stdin)
	var password[]byte
	var user[]byte

    fmt.Println("Username: ")
    user, _ = reader.ReadBytes('\n')
    fmt.Println("Password: ")
	password, _ = reader.ReadBytes('\n')
	
    fmt.Println("Your username is:", string(user), "\nYour password is:", string(password))
}