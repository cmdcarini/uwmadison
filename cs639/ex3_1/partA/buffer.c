#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* This program demonstrates the stack smash attack.
 * If a username > 8 characters is enterted then the value of 'bang'
 * is unintentionally modified. 
 *
 * By default any input over 8 characters is detected as a stack smash.
 *
 * If it is compliled with gcc option -fno-stack-protector, then the 
 * exploit can be observed.
 *
 * Example input:
 * '12345678' - Works as expected
 * '123456789' - Bang character is equal to \0 byte. The character 9 is taking the place of the null byte in the array. The null byte is overflowed to the bang variable ont he stack. 
 * '1234567890' - Bang character is equal to '0' character. The \0 byte is located outside of the bounds of each of these variables
 *
 * Without the -fno-stack-protector error, the program exits with a Stack Smashing identified error. 
 * 	*** stack smashing detected ***: <unknown> terminated
 *	Aborted (core dumped)
 */


int main(void) {
    char array[9];
    char bang = '!';

    printf("Enter 8 char username: ");
    gets(array);
    printf("Array variable is equal to '%s'\n", array);
    printf("Bang variable is equal to '%c'. Should be equal to '!'\n",bang);
    return 0;
}
