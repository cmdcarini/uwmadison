# Marco Carini - University of Wisconsin - Madison Portfolio
Projects and classes completed throughout my time at the University of Wisconsin - Madison.

Portfolio includes range of programs that focus on developing concepts like multithreading, multiprocessing, networking, and security.

Languages include: 
Java, C++, C, Python, HTML, node, Golang
