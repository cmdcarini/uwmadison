////////////////////////////////////////////////////////////////////////////////
// File Name:      a2.cpp
//
// Author:         Marco Carini
// CS email:       carini@cs.wisc.edu
//
// Description:    The source file for a2.
//
// IMPORTANT NOTE: THIS IS THE ONLY FILE THAT YOU SHOULD MODIFY FOR A2.
//                 You SHOULD NOT MODIFY any of the following:
//                   1. Name of the functions/methods.
//                   2. The number and type of parameters of the functions.
//                   3. Return type of the functions.
////////////////////////////////////////////////////////////////////////////////

#include "a2.hpp"
#include "trim.hpp"

#include <algorithm>
#include <cctype>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

using namespace std;
void cleanData(std::ifstream &inFile, std::ofstream &outFile,
  std::unordered_set<std::string> &stopwords) {
    vector<std::string> words, nopuncs, nowhite;
    string curr;
    while(getline(inFile, curr)){
      replaceHyphensWithSpaces(curr);
      splitLine(curr, words);
      removePunctuation(words, nopuncs);
      removeWhiteSpaces(nopuncs);
      removeEmptyWords(nopuncs);
      removeSingleLetterWords(nopuncs);
      removeStopWords(nopuncs, stopwords);
      for(int i = 0; i < nopuncs.size(); i++)
        outFile << nopuncs.at(i) << " ";
      outFile << endl;
      words.clear();
      nopuncs.clear();
    }
}

void fillDictionary(std::ifstream &newInFile,
  std::unordered_map<std::string, std::pair<long, long>> &dict) {
    string sentence, word;
    long rating;
    int pos;
    while(getline(newInFile, sentence)) {
      rating = sentence.c_str()[0] - '0';
      sentence.erase(0, 2);
      while((pos = sentence.find(" ")) != string::npos) {
        word = sentence.substr(0, pos);
        if(dict.find(word) == dict.end()){
          dict.insert({word, {rating, 1}});
        }
        else {
          dict.at(word).second += 1;
          dict.at(word).first += rating;
        }
        sentence.erase(0, pos + 1);
      }
    }
}

void fillStopWords(std::ifstream &inFile,
  std::unordered_set<std::string> &stopwords) {
    string word;
    while(inFile >> word) {
      stopwords.insert(word);
    }
}

void rateReviews(std::ifstream &testFile,
  std::unordered_map<std::string, std::pair<long, long>> &dict,
  std::ofstream &ratingsFile) {
    string sentence;
    string word;
    int index, pos = 0;
    double rating = 0;
    long count = 0;
    while(getline(testFile, sentence)) {
      rating = 0;
      count = 0;
      if(sentence.length() == 0) {
        ratingsFile << "2.00" << endl;
      }
      else {
        while((pos = sentence.find(" ")) != string::npos) {
          word = sentence.substr(0, pos);
          if(dict.find(word) != dict.end()){
            rating += (double)dict.at(word).first/(double)dict.at(word).second;
            count+=1;
          }
          else{
            if(word.length() > 1){
              rating += 2;
              count +=1;
            }
          }
          sentence.erase(0, pos + 1);
        }
        if(count > 0){
          char buffer[5];
          sprintf(buffer, "%.2f", (double)rating/(double)count);
          ratingsFile << buffer << endl;
        }
      }
    }
}

void removeEmptyWords(std::vector<std::string> &tokens) {
  for(int i = 0; i < tokens.size(); i++){
    if(tokens.at(i).length() == 0) {
      tokens.erase(tokens.begin() + (i));
      i--;
    }
  }
}

void removePunctuation(std::vector<std::string> &inTokens,
  std::vector<std::string> &outTokens) {
    string s;
    for(int i = 0; i < inTokens.size(); i++) {
      s = inTokens.at(i);
      for(int j = 0; j < s.length(); j++) {
        if(ispunct(s.at(j)) != 0){
          s.erase(j, 1);
          j--;
        }
      }
      outTokens.push_back(s);
    }
}

void removeSingleLetterWords(std::vector<std::string> &tokens) {
  for(int i = 0; i < tokens.size(); i++){
    if(tokens.at(i).length() == 1 && isdigit(tokens.at(i).c_str()[0]) == 0) {
      tokens.erase(tokens.begin() + (i));
      i--;
    }
  }
}

void removeStopWords(std::vector<std::string> &tokens,
  std::unordered_set<std::string> &stopwords) {
    for(int i = 0; i < tokens.size(); i++){
      if(stopwords.find(tokens.at(i)) != stopwords.end()){
        tokens.erase(tokens.begin() + (i));
        i--;
      }
    }
}

void removeWhiteSpaces(std::vector<std::string> &tokens) {
  for(string &s: tokens) {
    trim(s);
  }
}

void replaceHyphensWithSpaces(std::string &line) {
  string hyphen = "-";
  string space = " ";
  replace(line.begin(), line.end(), hyphen.c_str()[0], space.c_str()[0]);
}

void splitLine(std::string &line, std::vector<std::string> &words) {
  string word;
  int pos = 0;
  while((pos = line.find(" ")) != string::npos ) {
    word = line.substr(0, pos);
    words.push_back(word);
    line.erase(0, pos + 1);
  }
  words.push_back(line);
}
