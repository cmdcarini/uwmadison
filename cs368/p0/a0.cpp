#include <iostream>
#include <string>
/*
* p0 - 2/1/2018
* Author: Marco Carini
* cs username: carini
* This program outputs a custom welcome message to each user, based on the
* input name.
*/
using namespace std;
int main() {
  cout << "Enter your name: ";
  string name;
  getline(cin, name);
  string message [3];

  /*
  * I disagree with Stanford's style guides, brackets ARE NOT necessary for
  * single line if/else statements, but I've begrudgingly included them
  */
  if(name.length() != 0) {
    message[2] = "* Hello " + name + "! Welcome to CS 368 (C++)! :) *\n";
  } else {
    message[2] = "* Hello! Welcome to CS 368 (C++)! :) *\n";
  }

  for(int i = 0; i < message[2].length() - 1; i++) {
    message[0] = message[0] + "*";
  }
  message[0] = message[0] + "\n";

  message[1] = "*";
  for(int j = 0 ; j < message[2].length() - 3; j++) {
    message[1] = message[1] + " ";
  }
  message[1] = message[1] + "*\n";

  cout << "\n" << message[0] << message[1] << message[2]
  << message[1] << message[0] << endl;
  return 0;
}
