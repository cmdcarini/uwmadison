///////////////////////////////////////////////////////////////////////////////
// File Name:      processStudent.cpp
//
// Author:         <Marco Carini>
// CS email:       <carini@cs.wisc.edu>
//
// Description:    Methods to perform some processing on student objects.
///////////////////////////////////////////////////////////////////////////////

#include "processStudent.hpp"

#include <algorithm>
#include <cctype>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <typeinfo>
#include <iomanip>

using namespace std;

/**
* @brief Read students' data from an input file and store them in 2 vectors
*        based on the type of the student.
*
* @param inFile The input file with students' data.
* @param gstudents Vector of graduate students.
* @param ugstudents Vector of undergraduate students.
*/
void fillStudents(std::ifstream &inFile,
  std::vector<GradStudent> &gstudents,
  std::vector<UndergradStudent> &ugstudents) {
    while(inFile) {
      string line;
      if(!getline(inFile, line))
      break;
      stringstream ss(line);
      string infoByte;
      if(!getline(ss, infoByte, ','))
      break;
      if(infoByte == "G") {
        getline(ss, infoByte, ',');
        std::string name = infoByte;
        getline(ss, infoByte, ',');
        int yearOfBirth = stoi(infoByte);
        std::vector<double> assignmentsScore;
        for(int i = 0; i < 7; i++) {
          getline(ss, infoByte, ',');
          assignmentsScore.push_back(stoi(infoByte));
        }
        getline(ss, infoByte, ',');
        double projectScore = stoi(infoByte);
        getline(ss, infoByte, ',');
        std::string researchArea = infoByte;
        getline(ss, infoByte, ',');
        std::string advisor = infoByte;
        GradStudent gs(name, yearOfBirth, assignmentsScore, projectScore,
          researchArea, advisor);
          gstudents.push_back(gs);
        } else if(infoByte == "U") {
          getline(ss, infoByte, ',');
          std::string name = infoByte;
          getline(ss, infoByte, ',');
          int yearOfBirth = stoi(infoByte);
          std::vector<double> assignmentsScore;
          for(int i = 0; i < 7; i++) {
            getline(ss, infoByte, ',');
            assignmentsScore.push_back(stoi(infoByte));
          }
          getline(ss, infoByte, ',');
          double projectScore = stoi(infoByte);
          getline(ss, infoByte, ',');
          std::string residenceHall = infoByte;
          getline(ss, infoByte, ',');
          std::string yearInCollege = infoByte;
          UndergradStudent us(name, yearOfBirth, assignmentsScore, projectScore,
            residenceHall, yearInCollege);
            ugstudents.push_back(us);
          }
        }
      }

      /**
      * @brief Prints the details of a group of students.
      *
      * @param students A vector of student references to be printed.
      */
      void printStudents(const std::vector<std::reference_wrapper<Student>> &students) {
        for(auto i: students)
        i.get().printDetails();

      }

      /**
      * @brief Computes the statistics like number of students, mean of total score, and
      *        the sorted list of students in descending order based on their total.
      *
      * @param students A vector of student references for which the statistics needs to computed.
      */
      void computeStatistics(std::vector<std::reference_wrapper<Student>> &students) {
        // compute the # of students based on the type of students.
        double mean_score = 0;
        std::cout << "Number of students = " << students.size() << std::endl;
        // compute the mean of the total score.
        for(auto i: students)
        mean_score += i.get().getTotal();
        std::stringstream ss;
        ss <<  mean_score/students.size();
        std::cout << "The mean of the total score = " << ss.str() << std::endl;
        // sort and print the students based on their total.
        auto comparefunc = [] (std::reference_wrapper<Student> one, std::reference_wrapper<Student> two)
        { return one.get().getTotal() > two.get().getTotal();} ;
        std::sort(students.begin(), students.end(), comparefunc);
        std::cout << "The sorted list of students (id, name, total, grade) in descending order of total:" << std::endl;
        for(auto i: students)
        std::cout << i.get().getId() << ", " << i.get().getName() << ", " << i.get().getTotal() << ", " << i.get().getGrade() << std::endl;
        std::cout << std::endl;
      }
