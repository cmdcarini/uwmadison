#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <iomanip>
#include "Student.hpp"

int Student::numStudents = 0;
/**
* @brief A parameterised constructor for a Student.
*
* @param name Student's name.
* @param yearOfBirth Student's year of birth.
* @param assignmentsScore Student's assignment scores.
* @param projectScore Student's project score.
*/
Student::Student(std::string name,
  int yearOfBirth,
  const std::vector<double> &assignmentsScore,
  double projectScore) {
    Student::name = name;
    Student::yearOfBirth = yearOfBirth;
    Student::assignmentsScore = assignmentsScore;
    Student::projectScore = projectScore;
    Student::numStudents = numStudents + 1;
    Student::id = numStudents;
  }

  /**
  * @brief Private getter for Assignments in string form
  *
  * @return Assignements in string form
  */
  std::string Student::getAssignmentsStr() {
    std::stringstream ss;
    std::string delimeter = "";
    ss << "[";
    std::vector<double> assigns = Student::getAssignmentsScore();
    for(int i = 0; i < assigns.size(); i++) {
      ss << std::fixed << std::setprecision(0) << delimeter << assigns.at(i);
      delimeter = ", ";
    }
    ss <<  "]";
    return ss.str();
  }

  /**
  * @brief Getter for student's id.
  *
  * @return The id of the student.
  */
  int Student::getId() {
    return Student::id;
  }

  /**
  * @brief Getter for student's name.
  *
  * @return The name of the student.
  */
  std::string Student::getName() {
    return Student::name;
  }

  /**
  * @brief Getter for student's YOB.
  *
  * @return The YOB of the student.
  */
  int Student::getYearOfBirth() {
    return yearOfBirth;
  }

  /**
  * @brief Get the age of a student.
  *
  * @return The student's age.
  */
  int Student::getAge() {
    return current_year - yearOfBirth;
  }

  /**
  * @brief Getter for student's assignment scores.
  *
  * @return A const reference to the vector of student's assignment scores.
  */
  const std::vector<double> &Student::getAssignmentsScore() {
    return assignmentsScore;
  }

  /**
  * @brief Getter for student's project score.
  *
  * @return The project score of the student.
  */
  double Student::getProjectScore() {
    return Student::projectScore;
  }

  /**
  * @brief Get the total number of students.
  *
  * @return The total number of students.
  */
  int Student::getNumStudents() {
    return Student::numStudents;
  }

  /**
  * @brief Prints the details of the student.
  *
  * @example This method prints the following details of a student.
  * Id = 10
  * Name = Rex Fernando
  * Age =  19
  * Assignments = [57, 90, 81, 96, 80, 95, 78]
  * Project = 98
  * Total = 90.2143
  * Grade = CR
  */
  void Student::printDetails() {
    std::cout << "STUDENT DETAILS:" << std::endl;
    std::cout << "Id = " << Student::id << std::endl;
    std::cout << "Name = " << Student::name << std::endl;
    std::cout << "Age = " << Student::getAge() << std::endl;
    std::cout << "Assignments = " << Student::getAssignmentsStr() << std::endl;
    std::cout << "Project = " << Student::getProjectScore() << std::endl;
  }
