#include <string>
#include <vector>
#include <iostream>
#include "GradStudent.hpp"

int GradStudent::numGradStudents = 0;

/**
* @brief Constructor for a graduate student.
*
* @param name Graduate student's name.
* @param yearOfBirth Graduate student's YOB.
* @param assignmentsScore Graduate student's assignment scores.
* @param projectScore Graduate student's project score.
* @param researchArea Graduate student's research area.
* @param advisor Graduate student's advisor.
*/
GradStudent::GradStudent(std::string name,
  int yearOfBirth,
  const std::vector<double> &assignmentsScore,
  double projectScore,
  std::string researchArea,
  std::string advisor) : Student(name, yearOfBirth, assignmentsScore, projectScore)
  {
    GradStudent::researchArea = researchArea;
    GradStudent::advisor = advisor;
  }

  /**
  * @brief Getter for the student's research area.
  *
  * @return The research area of the student.
  */
  std::string GradStudent::getResearchArea() {
    return GradStudent::researchArea;
  }

  /**
  * @brief Getter for the student's advisor.
  *
  * @return The advisor of the student.
  */
  std::string GradStudent::getAdvisor() {
    return GradStudent::advisor;
  }

  /**
  * @brief Get the total number of graduate students.
  *
  * @return The number of undergraduate students.
  */
  int GradStudent::getNumStudents() {
    return GradStudent::numGradStudents;
  }

  /**
  * @brief Prints details of GradStudent class
  *
  * @return Void
  */
  void GradStudent::printDetails() {
    Student::printDetails();
    std::cout << "Total = " << GradStudent::getTotal() << std::endl;
    std::cout << "Grade = " << GradStudent::getGrade() << std::endl;
    std::cout << "Type = Graduate Student" << std::endl;
    std::cout << "Research Area = " << GradStudent::getResearchArea() << std::endl;
    std::cout << "Advisor = " << GradStudent::getAdvisor() << std::endl;
    std::cout << std::endl;
  }

  /**
  * @brief Gets the total grade
  *
  * @return total grade
  */
  double GradStudent::getTotal() {
    double result = 0;
    for(auto i: Student::getAssignmentsScore())
    result += i;
    result = result/(Student::getAssignmentsScore().size());
    return 0.5*result + 0.5*Student::getProjectScore();
  }

  /**
  * @brief Gets the string grade
  *
  * @return Letter grade
  */
  std::string GradStudent::getGrade() {
    return GradStudent::getTotal() >= 80 ? "CR" : "N";
  }
