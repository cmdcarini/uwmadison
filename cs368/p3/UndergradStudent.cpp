#include <string>
#include <vector>
#include <iostream>
#include "UndergradStudent.hpp"

int UndergradStudent::numUndergradStudents = 0;

/**
* @brief Constructor for an undergraduate student.
*
* @param name Undergraduate student's name
* @param yearOfBirth Undergraduate student's YOB
* @param assignmentsScore Undergraduate student's assignment scores.
* @param projectScore Undergraduate student's project score.
* @param residenceHall Undergraduate student's residence hall.
* @param yearInCollege Undergraduate student's year in college.
*/
UndergradStudent::UndergradStudent(std::string name,
  int yearOfBirth,
  const std::vector<double> &assignmentsScore,
  double projectScore,
  std::string residenceHall,
  std::string yearInCollege) : Student(name, yearOfBirth, assignmentsScore, projectScore){
    UndergradStudent::residenceHall = residenceHall;
    UndergradStudent::yearInCollege = yearInCollege;
  }

  /**
  * @brief Getter for a student's residence hall.
  *
  * @return The residence hall in which the student resides.
  */
  std::string UndergradStudent::getResidenceHall() {
    return UndergradStudent::residenceHall;
  }

  /**
  * @brief Getter for a student's year in college.
  *
  * @return The year in college of the student.
  */
  std::string UndergradStudent::getYearInCollege() {
    return UndergradStudent::yearInCollege;
  }

  /**
  * @brief Get the total number of undergraduate students.
  *
  * @return The number of undergraduate students.
  */
  int UndergradStudent::getNumStudents() {
    return UndergradStudent::numUndergradStudents;
  }

  /**
  * @brief Gets the total grade
  *
  * @return Total grade
  */
  double UndergradStudent::getTotal() {
    double result = 0;
    for(auto i: Student::getAssignmentsScore())
    result += i;
    result = result/(Student::getAssignmentsScore().size());
    return 0.7*result + 0.3*Student::getProjectScore();
  }

  /**
  * @brief Gets the string grade
  *
  * @return String, Letter grade
  */
  std::string UndergradStudent::getGrade() {
    return UndergradStudent::getTotal() >= 70 ? "CR" : "N";
  }

  /**
  * @brief Prints the details
  *
  * @return void
  */
  void UndergradStudent::printDetails() {
    Student::printDetails();
    std::cout << "Total = " << UndergradStudent::getTotal() << std::endl;
    std::cout << "Grade = " << UndergradStudent::getGrade() << std::endl;
    std::cout << "Type = Undergraduate Student" << std::endl;
    std::cout << "Residence Hall = " << UndergradStudent::getResidenceHall() << std::endl;
    std::cout << "Year in College = " << UndergradStudent::getYearInCollege() << std::endl;
    std::cout << std::endl;
  }
